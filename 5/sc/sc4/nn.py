# By Gema Pratama Aditya 1706040031

#[x1, x2, x3, target]
data = [                   
    [1, 0, 0, 0],
    [0.6, 0.2, 1, 0],
    [0.5, 0, 0.2, 1],
    [1, 1, 0.8, 1],
    [0.3, 1, 0.5, 1]
]

initialWeight = [-1, -0.5, 0.2]
learningRate = 0.2

def activationFunction(listX, listW):
    currWeight = listW.copy()
    weightedSum = 0
    result = 0
    target = listX[3]

    for i in range(len(listW)):
        summand = listX[i] * listW[i]
        weightedSum += summand

    if weightedSum <= 0:       
        result = 0
    else:      
        result = 1

    for i in range(len(listW)):
        deltaWi = learningRate * (target - result) * listX[i]
        #print("Currweight = {}".format(currWeight))
        #print("listW: {}".format(listW))
        #print("{} * ({} - {}) * {} = {}".format(learningRate, target, result, listX[i], deltaWi))
        #print("w{} baru: {}".format(i + 1, round(currWeight[i] + deltaWi, 2)))
        #print("currWeight[i] = {}, deltaWi = {}".format(currWeight[i], deltaWi))
        initialWeight[i] = currWeight[i] + deltaWi
        #print("AFTER UPDATE: {}".format(initialWeight))

    #print("Updated weight = " + str(initialWeight))
    for i in range(len(initialWeight)):
        print("w{}next: {:.{}f}".format(i + 1, initialWeight[i], 2) + "   ", end="")

    print("Sum: {}, y: {}, Error: {}, Target: {}, ".format(round(weightedSum, 2), round(result, 2), round(target - result, 2), target))     
    print()


print("\nepoch 1\n=============================================================================")
for i in range(len(data)):
    #print("Weight = " + str(initialWeight))
    activationFunction(data[i], initialWeight)

print("Initial weight epoch 2: " + str(initialWeight))
print("\nepoch 2\n=============================================================================")
for i in range(len(data)):
    #print("Data = " + str(data[i]))
    activationFunction(data[i], initialWeight)

print("Initial weight epoch 3: " + str(initialWeight))
print("\nepoch 3\n=============================================================================")
for i in range(len(data)):
    #print("Weight = " + str(initialWeight))
    activationFunction(data[i], initialWeight)