import math 
def log2(b):
        return math.log(b, 2)
    
def entropy(p, n):
    if p == 0 or n == 0:
        return 0
    else:
        total = p + n
        suku1 = -(p / total) * log2(p/total)
        suku2 = n / total * log2(n/total)
        return suku1 - suku2

print(entropy(13,4))