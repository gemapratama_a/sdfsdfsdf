import math

def log(a,b):
    return math.log(b,a)

positive = 4
negative = 4
total = positive + negative
def entropyDataset():
    total = positive + negative

    suku1 = -(positive / total) * log(2, positive/total)
    suku2 = negative / total * log(2, negative/total)
    entropy = suku1 - suku2

    return entropy

print("ENTROPY DATASET: " + str(entropyDataset()))

def entropy(p, n):
    if p == 0 or n == 0:
        return 0
    else:
        total = p + n
        suku1 = -(p / total) * log(2, p/total)
        suku2 = n / total * log(2, n/total)
        return suku1 - suku2

print(f'''
TABEL ENTROPY SKILL

SANGATMAHIR YA 1 TIDAK 2
BISA        YA 2 TIDAK 2
TIDAKBISA   YA 1 TIDAK 0

SKILL       ENTROPY
----------------------
sangatmahir {entropy(1,2)}
tidakbisa   {entropy(2,2)}
bisa        {entropy(1,0)}

REMAINDER SKILL = 1/8 * ((1+2) * {entropy(1,2)} + (2+2) * {entropy(2,2)} + (1+0)* {entropy(1,0)})
                = {1/8 * ((1+2) * entropy(1,2) + (2+2) * entropy(2,2) + (1+0)* entropy(1,0))}

TABEL ENTROPY GENRE

HORROR YA 2 TIDAK 1
ACTION YA 0 TIDAK 2
DRAMA  YA 2 TIDAK 1

GENRE   ENTROPY
-----------------------
horror  {entropy(2,1)}
drama   {entropy(2,1)}
action  {entropy(0,2)}

REMAINDER GENRE = 1/8 * [(2+1) * {entropy(2,1)} + (2+1)*{entropy(2,1)} + (0+2) * 0)]
                = {1/8 * ((2+1) * entropy(2,1) + (2+1)* entropy(2,1) + (0+2) * 0)}
               
TABEL ENTROPY NILAI

A YA 2 TIDAK 1
B YA 1 TIDAK 1
C YA 2 TIDAK 1

NILAI   ENTROPY
-----------------------
a       {entropy(2,1)}
b       {entropy(1,1)}
c       {entropy(2,1)}

REMAINDER NILAI  = 1/8 * [(2+1) * 0.9182958340544896 + (2+1)*0.9182958340544896 + 2)]
                 = {1/8 * ((2+1) * 0.9182958340544896 + (2+1)*0.9182958340544896 + 2)}

GAIN SKILL = {entropyDataset()} - 0.84436       = {entropyDataset() - 0.84436}  
GAIN NILAI = {entropyDataset()} - 0.93872187554 = {entropyDataset() - 0.93872187554}
GAIN GENRE = {entropyDataset()} - 0.68872187554 = {entropyDataset() - 0.68872187554}

ROOT: GENRE

new dataset horror
SANGATMAHIR         C       TIDAK
BISA                C       YA
BISA                C       YA

Entropy(Horror) = {entropy(2,1)}


entropy nilai
C   2 YA 1 TIDAK

REMAINDER = 1/3 * (2 + 1) * {entropy(2,1)}
          = {1/3 * ((2 + 1) * entropy(2,1))}

entropy skill
SANGATMAHIR 0 YA 1 TIDAK
BISA        2 YA 0 TIDAK

REMAINDER = 1/3 * (0) = 0

GAIN NILAI = {entropy(2,1) - (1/3 * ((2 + 1) * entropy(2,1)))}
GAIN SKILL = {entropy(2,1) - 0}
PICK: SKILL

=============================================================================
new dataset action
tidak
=============================================================================
new dataset drama
SANGATMAHIR         A       YA
TIDAKBISA           B       YA
BISA                A       TIDAK

2 ya 1 tidak, entropy = {entropy(2,1)}

SKILL
SANGATMAHIR 1 YA 0 TIDAK 
TIDAKBISA 1 YA 0 TIDAK
BISA 0 YA 1 TIDAK

entropy = 0

NILAI
A 1 YA 1 TIDAK
2 1 YA 0 TIDAK

entropy = 1/3 * (2 * 1)
        = {2/3} 

GAIN SKILL = {entropy(2,1) - 0}
GAIN NILAI = {entropy(2,1) - (2/3)}

PICK: SKILL
''')
