﻿DO language plpgsql $$
DECLARE
  var_activity character varying;
BEGIN
  var_activity := 'Inserting records into table mahasiswa_7 ... ';
  RAISE NOTICE '%',var_activity;
  INSERT INTO activity_log(activity) VALUES(var_activity);
  
  FOR i IN 1 .. 5000000 LOOP
    INSERT INTO mahasiswa_7 VALUES(i, 'nama ' || i, 'alamat ' || i);
  END LOOP;
END;
$$;