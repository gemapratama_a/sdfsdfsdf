﻿DO language plpgsql $$
DECLARE
  var_activity character varying;
BEGIN
  var_activity := 'Inserting records into table mahasiswa_1 ... ';
  RAISE NOTICE '%',var_activity;
  INSERT INTO activity_log(activity) VALUES(var_activity);
  
  FOR i IN 1 .. 10 LOOP
    INSERT INTO mahasiswa_1 VALUES(i, 'nama ' || i, 'alamat ' || i);
  END LOOP;
  
  var_activity := 'Inserting records into table mahasiswa_2 ... ';
  RAISE NOTICE '%',var_activity;
  INSERT INTO activity_log(activity) VALUES(var_activity);
  
  FOR i IN 1 .. 100 LOOP
    INSERT INTO mahasiswa_2 VALUES(i, 'nama ' || i, 'alamat ' || i);
  END LOOP;
  
  var_activity := 'Inserting records into table mahasiswa_3 ... ';
  RAISE NOTICE '%',var_activity;
  INSERT INTO activity_log(activity) VALUES(var_activity);
  
  FOR i IN 1 .. 1000 LOOP
    INSERT INTO mahasiswa_3 VALUES(i, 'nama ' || i, 'alamat ' || i);
  END LOOP;
  
  var_activity := 'Inserting records into table mahasiswa_4 ... ';
  RAISE NOTICE '%',var_activity;
  INSERT INTO activity_log(activity) VALUES(var_activity);
  
  FOR i IN 1 .. 10000 LOOP
    INSERT INTO mahasiswa_4 VALUES(i, 'nama ' || i, 'alamat ' || i);
  END LOOP;
  
  var_activity := 'Inserting records into table mahasiswa_5 ... ';
  RAISE NOTICE '%',var_activity;
  INSERT INTO activity_log(activity) VALUES(var_activity);
  
  FOR i IN 1 .. 100000 LOOP
    INSERT INTO mahasiswa_5 VALUES(i, 'nama ' || i, 'alamat ' || i);
  END LOOP;
  
  var_activity := 'Inserting records into table mahasiswa_6 ... ';
  RAISE NOTICE '%',var_activity;
  INSERT INTO activity_log(activity) VALUES(var_activity);
  
  FOR i IN 1 .. 1000000 LOOP
    INSERT INTO mahasiswa_6 VALUES(i, 'nama ' || i, 'alamat ' || i);
  END LOOP;
END;
$$;