﻿DO language plpgsql $$
DECLARE
  var_activity character varying;
BEGIN
  CREATE TABLE activity_log
  (
    id serial NOT NULL,
    ts timestamp without time zone DEFAULT now(),
    username character varying DEFAULT current_user,
    activity character varying,
    CONSTRAINT log_pk PRIMARY KEY (id)
  )
  WITH (
    OIDS=FALSE
  );

  var_activity := 'Creating table mahasiswa 1 ... ';
  RAISE NOTICE '%',var_activity;
  INSERT INTO activity_log(activity) VALUES(var_activity);

  CREATE TABLE mahasiswa_1
  (
    nomor serial NOT NULL,
    nama character varying,
    alamat character varying,
    CONSTRAINT mahasiswa_1_pk PRIMARY KEY (nomor)
  )
  WITH (
    OIDS=FALSE
  );

  var_activity := 'Creating table mahasiswa 2 ... ';
  RAISE NOTICE '%',var_activity;
  INSERT INTO activity_log(activity) VALUES(var_activity);

  CREATE TABLE mahasiswa_2
  (
    nomor serial NOT NULL,
    nama character varying,
    alamat character varying,
    CONSTRAINT mahasiswa_2_pk PRIMARY KEY (nomor)
  )
  WITH (
    OIDS=FALSE
  );

  var_activity := 'Creating table mahasiswa 3 ... ';
  RAISE NOTICE '%',var_activity;
  INSERT INTO activity_log(activity) VALUES(var_activity);

  CREATE TABLE mahasiswa_3
  (
    nomor serial NOT NULL,
    nama character varying,
    alamat character varying,
    CONSTRAINT mahasiswa_3_pk PRIMARY KEY (nomor)
  )
  WITH (
    OIDS=FALSE
  );

  var_activity := 'Creating table mahasiswa 4 ... ';
  RAISE NOTICE '%',var_activity;
  INSERT INTO activity_log(activity) VALUES(var_activity);

  CREATE TABLE mahasiswa_4
  (
    nomor serial NOT NULL,
    nama character varying,
    alamat character varying,
    CONSTRAINT mahasiswa_4_pk PRIMARY KEY (nomor)
  )
  WITH (
    OIDS=FALSE
  );

  var_activity := 'Creating table mahasiswa 5 ... ';
  RAISE NOTICE '%',var_activity;
  INSERT INTO activity_log(activity) VALUES(var_activity);

  CREATE TABLE mahasiswa_5
  (
    nomor serial NOT NULL,
    nama character varying,
    alamat character varying,
    CONSTRAINT mahasiswa_5_pk PRIMARY KEY (nomor)
  )
  WITH (
    OIDS=FALSE
  );

  var_activity := 'Creating table mahasiswa 6 ... ';
  RAISE NOTICE '%',var_activity;
  INSERT INTO activity_log(activity) VALUES(var_activity);

  CREATE TABLE mahasiswa_6
  (
    nomor serial NOT NULL,
    nama character varying,
    alamat character varying,
    CONSTRAINT mahasiswa_6_pk PRIMARY KEY (nomor)
  )
  WITH (
    OIDS=FALSE
  );

  var_activity := 'Creating table mahasiswa 7 ... ';
  RAISE NOTICE '%',var_activity;
  INSERT INTO activity_log(activity) VALUES(var_activity);

  CREATE TABLE mahasiswa_7
  (
    nomor serial NOT NULL,
    nama character varying,
    alamat character varying,
    CONSTRAINT mahasiswa_7_pk PRIMARY KEY (nomor)
  )
  WITH (
    OIDS=FALSE
  );
END;
$$;
