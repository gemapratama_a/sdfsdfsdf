1. Tampilkan bookid, title, branchName (library), dan jumlah copy yang dimiliki.

SELECT B.BookId, B.Title, LB.BranchName, BC.No_Of_Copies
FROM BOOK B, PUBLISHER P, BOOK_COPIES BC, LIBRARY_BRANCH LB, BOOK_LOANS BL
WHERE BC.BookId = B.BookId
AND BL.BookId = B.BookId
AND BL.BranchId = LB.BranchId;


2. Tampilkan bookid dan title buku yang tidak pernah dipinjam.

SELECT B.BookId, B.Title
FROM BOOK B, BOOK_LOANS BL
WHERE BL.BookId = B.BookId
AND DateOut = NULL;

SELECT B.BookId, B.Title
FROM BOOK B, BOOK_LOANS BL
WHERE B.BookId NOT IN (
    SELECT BL.BookId 
    FROM BOOK_LOANS
)


3. Tampilkan semua buku dan jumlah peminjaman terhadap buku tersebut. Yang ditampilkan bookid, title, dan jumlahnya.

SELECT B.BookId, B.Title, COUNT(*) AS 'Jumlah Peminjaman'
FROM BOOK B, BOOK_LOANS BL
WHERE B.BookId = BL.BookId;


4. Tampilkan nama peminjam (borrower) dan branchName (library) sebagai tempat peminjaman buku dilakukan.

SELECT B.Name, LB.BranchName
FROM BORROWER B, LIBRARY_BRANCH LB, BOOK_LOANS BL,
WHERE BL.BranchId = LB.BranchId
AND B.CardNo = BL.CardNo;


5. Tampilkan authorname (bookauthor) dan title buku dari authornya.

SELECT AuthorName, B.Title
FROM BOOK B, BOOK_AUTHORS BA
WHERE B.BookId = BA.BookId;





















SELECT B.BookId, B.Title
FROM BOOK B, BOOK_LOANS BL
WHERE B.BookId NOT IN (
    SELECT BL.BookId 
    FROM BOOK_LOANS
)

