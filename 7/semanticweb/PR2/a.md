# Exercise 2

-Gema Pratama Aditya
-1706040031


#### 1. The DBPedia SPARQL endpoint hosts more than one named graphs. Find all those named graphs.

Query:
```
SELECT DISTINCT ?g 
WHERE {
  GRAPH ?g { ?s ?p ?o }
}
```
Result:

| g                                                                                     |
| ------------------------------------------------------------------------------------- |
| http://www.openlinksw.com/schemas/virtrdf#                                            |
| http://www.w3.org/ns/ldp#                                                             |
| http://dbpedia.org/sparql                                                             |
| http://dbpedia.org/DAV/                                                               |
| http://www.w3.org/2002/07/owl#                                                        |
| file:wikidata_to_schema_mappings.ttl                                                  |
| http://dbpedia.org/schema/property_rules#                                             |
| dbprdf-label                                                                          |
| http://dbpedia.org/resource/classes#                                                  |
| http://www.ontologyportal.org/SUMO#                                                   |
| http://www.ontologyportal.org/WordNet#                                                |
| b3sonto                                                                               |
| b3sifp                                                                                |
| urn:rules.skos                                                                        |
| http://www.openlinksw.com/schemas/oplweb#                                             |
| virtrdf-label                                                                         |
| facets                                                                                |
| urn:virtuoso:val:acl:schema                                                           |
| http://www.openlinksw.com/virtpivot/icons                                             |
| http://www.openlinksw.com/schemas/virtpivot                                           |
| virtpivot-rules                                                                       |
| http://www.openlinksw.com/schemas/virtcxml#                                           |
| virtpivot-icon-test                                                                   |
| http://pivot_test_data/campsites                                                      |
| http://pivot_test_data/ski_resorts                                                    |
| http://www.wikidata.org                                                               |
| https://github.com/structureddynamics/UMBEL/External%20Ontologies/dbpedia-ontology.n3 |
| http://dbpedia.org                                                                    |
| http://people.aifb.kit.edu/ath/#DBpedia_PageRank                                      |
| http://dbpedia.org/page_links                                                         |
| http://dbpedia.org/void/                                                              |
| http://dbpedia.org/virt_curi                                                          |
| asEquivalent                                                                          |
| urn:activitystreams-owl:map                                                           |


#### 2. DBpedia contains RDFS triples that specify a class hierarchy (those triples whose predicate is rdfs:subClassOf orowl:equivalentClass). Find all classes whose IRI is in the http://dbpedia.org/ontology/ namespace such that these classes appear in at least one triple whose predicate is rdfs:subClassOf or owl:equivalentClass.

Query:
```
SELECT ?class
WHERE {
    {?class rdfs:subClassOf ?o .}
    UNION
    {?class owl:equivalentClass ?equivalentclass .}
} 
LIMIT 20
```

Result:

| class                                           |
| ----------------------------------------------- |
| http://dbpedia.org/ontology/Activity            |
| http://dbpedia.org/ontology/Name                |
| http://dbpedia.org/ontology/Place               |
| http://dbpedia.org/ontology/Language            |
| http://dbpedia.org/ontology/Agent               |
| http://dbpedia.org/ontology/Altitude            |
| http://dbpedia.org/ontology/AnatomicalStructure |
| http://dbpedia.org/ontology/Area                |
| http://dbpedia.org/ontology/Award               |
| http://dbpedia.org/ontology/Biomolecule         |
| http://dbpedia.org/ontology/Blazon              |
| http://dbpedia.org/ontology/ChartsPlacements    |
| http://dbpedia.org/ontology/ChemicalSubstance   |
| http://dbpedia.org/ontology/Colour              |
| http://dbpedia.org/ontology/Currency            |
| http://dbpedia.org/ontology/Demographics        |
| http://dbpedia.org/ontology/Depth               |
| http://dbpedia.org/ontology/Device              |
| http://dbpedia.org/ontology/Diploma             |
| http://dbpedia.org/ontology/Disease             |

### 3. List the name of all airlines which are from Orlando International Airport. If any, also find its destination in English label.

Query:
```
SELECT DISTINCT ?airlines ?destination 
WHERE {
  ?airlines dbo:hubAirport dbr:Orlando_International_Airport .
  OPTIONAL {
      ?airlines dbo:targetAirport ?destination
  }
}
LIMIT 20
```
Result:

| airlines                                        | destination                                                                  |
| ----------------------------------------------- | ---------------------------------------------------------------------------- |
| http://dbpedia.org/resource/Delta_Express       | http://dbpedia.org/resource/John_F._Kennedy_International_Airport            |
| http://dbpedia.org/resource/Delta_Express       | http://dbpedia.org/resource/Logan_International_Airport                      |
| http://dbpedia.org/resource/Delta_Express       | http://dbpedia.org/resource/New_York_City                                    |
| http://dbpedia.org/resource/Delta_Express       | http://dbpedia.org/resource/Boston                                           |
| http://dbpedia.org/resource/Comair              | http://dbpedia.org/resource/LaGuardia_Airport                                |
| http://dbpedia.org/resource/Comair              | http://dbpedia.org/resource/Logan_International_Airport                      |
| http://dbpedia.org/resource/Comair              | http://dbpedia.org/resource/Piedmont_Triad_International_Airport             |
| http://dbpedia.org/resource/Comair              | http://dbpedia.org/resource/Hartsfield-Jackson_Atlanta_International_Airport |
| http://dbpedia.org/resource/Song_(airline)      | http://dbpedia.org/resource/Logan_International_Airport                      |
| http://dbpedia.org/resource/Song_(airline)      | http://dbpedia.org/resource/Fort_Lauderdale-Hollywood_International_Airport  |
| http://dbpedia.org/resource/AirTran_Airways     |
| http://dbpedia.org/resource/Silver_Airways      |
| http://dbpedia.org/resource/ValuJet_Airlines    |
| http://dbpedia.org/resource/Braniff_(1983–1990) |


### 4. List all companies whose field is either in the entertainment or video game industry and their address.
Query:

```
SELECT DISTINCT ?company_name ?company_address
WHERE {
    {?company_name a dbo:Company ; dbo:industry dbr:Entertainment . }
    UNION
    {?company_name a dbo:Company ; dbo:industry dbr:Video_game_industry . }
    ?company_name dbo:location ?company_address . 
}
LIMIT 20
```

Result:

| company_name                                        | company_address                                |
| --------------------------------------------------- | ---------------------------------------------- |
| http://dbpedia.org/resource/MBO_Cinemas             | http://dbpedia.org/resource/Malaysia           |
| http://dbpedia.org/resource/UltraStar_Cinemas       | http://dbpedia.org/resource/Redlands,_CA       |
| http://dbpedia.org/resource/Les'_Copaque_Production | http://dbpedia.org/resource/Shah_Alam          |
| http://dbpedia.org/resource/Les'_Copaque_Production | http://dbpedia.org/resource/Selangor           |
| http://dbpedia.org/resource/Les'_Copaque_Production | http://dbpedia.org/resource/Malaysia           |
| http://dbpedia.org/resource/19_Entertainment        | http://dbpedia.org/resource/Los_Angeles        |
| http://dbpedia.org/resource/Amuse,_Inc.             | http://dbpedia.org/resource/Japan              |
| http://dbpedia.org/resource/Amuse,_Inc.             | http://dbpedia.org/resource/Shibuya,_Tokyo     |
| http://dbpedia.org/resource/Big_Cinemas             | http://dbpedia.org/resource/Maharashtra        |
| http://dbpedia.org/resource/Big_Cinemas             | http://dbpedia.org/resource/Mumbai             |
| http://dbpedia.org/resource/Cine_Animadores         | http://dbpedia.org/resource/Chile              |
| http://dbpedia.org/resource/GMA_New_Media           | http://dbpedia.org/resource/Philippines        |
| http://dbpedia.org/resource/GMA_New_Media           | http://dbpedia.org/resource/GMA_Network_Center |
| http://dbpedia.org/resource/GMA_New_Media           | http://dbpedia.org/resource/Quezon_City        |
| http://dbpedia.org/resource/Apple_Corps             | http://dbpedia.org/resource/UK                 |
| http://dbpedia.org/resource/Kerasotes_Theatres      | http://dbpedia.org/resource/Chicago,_Illinois  |
| http://dbpedia.org/resource/Van_Houtte              | http://dbpedia.org/resource/Montreal           |
| http://dbpedia.org/resource/Van_Houtte              | http://dbpedia.org/resource/Canada             |
| http://dbpedia.org/resource/Van_Houtte              | http://dbpedia.org/resource/Quebec             |
| http://dbpedia.org/resource/Bwin                    | http://dbpedia.org/resource/Vienna             |


--------------------------------------------------------------------------------------------------------------
### 5. List all companies which have subsidiaries in the video gaming industry located in Quebec.

Query:
```
SELECT DISTINCT ?company_name 
WHERE { 
    ?company_name a dbo:Company ; dbo:industry dbr:Video_game_industry .
    ?company_name a dbo:Company ; dbo:locationCity dbr:Quebec
}
LIMIT 20
```
Result:

| company_name                                     |
| ------------------------------------------------ |
| http://dbpedia.org/resource/Eidos_Montreal       |
| http://dbpedia.org/resource/EA_Montreal          |
| http://dbpedia.org/resource/Superbrothers        |
| http://dbpedia.org/resource/Tribute_Games        |
| http://dbpedia.org/resource/Square_Enix_Montreal |


### 6. List all companies which have subsidiary in United State and founded after 1900.

BELOM ADA YG > 1900
```
SELECT DISTINCT ?company_name ?year_founded
WHERE {
    { ?company_name a dbo:Company ; dbo:locationCountry dbr:United_States . }
}
LIMIT 20
```

Result:

### 7. List all products that are developed by the subsidiary of Activision Publishing, Inc. List also the name of product developer companies.

```
SELECT DISTINCT ?product ?product_developer
WHERE {
  dbr:Activision dbo:subsidiary ?subsidiary .
  ?subsidiary dbo:product ?product .
  ?product a dbo:Software ; dbo:developer ?product_developer
}
LIMIT 20
```
Result:

| product                                                     | product_developer                                |
| ----------------------------------------------------------- | ------------------------------------------------ |
| http://dbpedia.org/resource/Transformers:_Fall_of_Cybertron | http://dbpedia.org/resource/High_Moon_Studios    |
| http://dbpedia.org/resource/Deadpool_(video_game)           | http://dbpedia.org/resource/High_Moon_Studios    |
| http://dbpedia.org/resource/DJ_Hero                         | http://dbpedia.org/resource/FreeStyleGames       |
| http://dbpedia.org/resource/DJ_Hero                         | http://dbpedia.org/resource/Exient_Entertainment |
| http://dbpedia.org/resource/Transformers:_War_for_Cybertron | http://dbpedia.org/resource/High_Moon_Studios    |
| http://dbpedia.org/resource/Call_of_Duty:_Advanced_Warfare  | http://dbpedia.org/resource/Sledgehammer_Games   |

### 8. List all multiplayer FPS (first-person shooter) games, developed by any company which is a subsidiary in the United States, that can be played in PS 2 where the games’ names are returned as one literal value (separated by ’ ; ').

```
SELECT DISTINCT ?game
WHERE {
  ?game a dbo:Software ; dbo:genre dbr:First-person_shooter . 
  ?game a dbo:Software ; dbp:modes dbr:Multiplayer_video_game . 
  ?game a dbo:Software ; dbo:computingPlatform dbr:PlayStation_2 . 
  ?game a dbo:Software ; dbo:developer ?developer .
  ?developer dbo:locationCountry dbr:United_States .
}
```

Result:

| game                                                         |
| ------------------------------------------------------------ |
| http://dbpedia.org/resource/Soldier_of_Fortune_(video_game)  |
| http://dbpedia.org/resource/Turok:_Evolution                 |
| http://dbpedia.org/resource/Red_Faction                      |
| http://dbpedia.org/resource/Return_to_Castle_Wolfenstein     |
| http://dbpedia.org/resource/Star_Trek:_Voyager_–_Elite_Force |
| http://dbpedia.org/resource/Call_of_Duty:_Finest_Hour        |
| http://dbpedia.org/resource/Project_Snowblind                |
| http://dbpedia.org/resource/Delta_Force:_Black_Hawk_Down     |
| http://dbpedia.org/resource/007:_Agent_Under_Fire            |
| http://dbpedia.org/resource/Call_of_Duty_2:_Big_Red_One      |
| http://dbpedia.org/resource/Area_51_(2005_video_game)        |

### 9. List the names of all people who won IEEE awards more than once and were born in Southeast Asian countries.
blm beres
Query:
```
SELECT DISTINCT ?person_name ?birth_country
WHERE {

}
LIMIT 20
```



###10. List all directors who directed supernatural horror films that are produced by producers who already produced more than 2 movies.

Query:
Result:
###11. List all the five oldest actresses who co-starred with Tom Cruise in the movie directed by Paul Thomas Anderson where the actresses’ names are returned as one literal value (separated by ’ ; ').
Query:
Result:
###12. Get the actor who co-starred with Tom Cruise in the movie directed by Paul Thomas Anderson and has the biggest age difference with Tom Cruise compared to other actors.
Query:
Result: