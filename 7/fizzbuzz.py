condition = {
    3 : "fizz",
    5 : "buzz",
    7 : "bazz",
    9 : "bezz",
    11 : "fuzz"
}

for i in range(1, 200):
    result = ""
    for key in condition.keys():
        if i % key == 0:
            result += condition[key]
    if result == "":
        result = str(i)
            
    print(result)