
{-# LANGUAGE FlexibleContexts #-}
-- Sudah dikonsultasi dengan asdos (Michael Halim) untuk di-fix menjadi 2 parameter saja
myUncurry f = \(x,y) -> f x y
myCurry f = (\x -> (\y -> f x y))

--Deret bilangan fibonacci adalah serangkaian deret angka sederhana yang susunan
--angkanya merupakan penjumlahan dari dua angka sebelumnya. Buatlah fungsi
--fibonacci yang menerima bilangan bulat positif n dan mengembalikan list yang
--berisi bilangan fibonacci dari 0 sampai n
fibHelper :: Integer -> Integer
fibHelper 0 = 0
fibHelper 1 = 1
fibHelper n = fibHelper (n-1) + fibHelper (n-2)
fibonacci n = map fibHelper [0..n]


--Buatlah fungsi power namun hanya dengan menggunakan operasi penjumlahan (+)
mult :: Int -> Int -> Int
mult x 0 = 0
mult x y = x + mult x (y - 1)

power :: Int -> Int -> Int
power x 0 = 1
power x y = mult x (power x (y-1))


--Buatlah fungsi sumEven yang menerima list dari bilangan asli dan menjumlahkan
--bilangan-bilangan genap saja.
sumEven :: [Integer] -> Integer
sumEven xs = sum (filter even xs)

--Buatlah sebuah kalkulator investasi sederhana berupa fungsi invest yag menerima
--3 buah parameter, yaitu:
--- nominal tiap bulan
--- return investasi tiap bulan (%)
--- durasi (bulan)
investHelper :: Double -> Double -> Double -> Double
investHelper nominal returned duration
  | duration == 0 = nominal
  | otherwise = nominal + investHelper (nominal * (1 + returned / 100)) returned (duration - 1)
invest :: Double -> Double -> Double -> Double
invest nominal returned duration = (investHelper nominal returned duration) - nominal

main = do
  print (fibonacci 4)
  print (power 3 2)
  print (sumEven [1..6])
  print (myUncurry (+) (3,4))
  print (myCurry (+) 2 3)
  print (invest 100000 0.3 2)

-- Referensi:  
-- https://stackoverflow.com/questions/48053330/calculate-power-of-number-with-recursion-and-addition
-- https://www.geeksforgeeks.org/write-you-own-power-without-using-multiplication-and-division/
-- https://stackoverflow.com/questions/1105765/generating-fibonacci-numbers-in-haskell
-- https://dev.to/__namc/what-is-currying--3l2a
-- https://stackoverflow.com/questions/28879421/haskell-is-it-possible-to-create-a-curry-function-that-can-curry-any-number-of
-- 