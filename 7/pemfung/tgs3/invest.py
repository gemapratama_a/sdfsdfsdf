def invest(nominal, returned, duration):
    total = 0
    for i in range(duration + 1):
        if i == 0:
            total = nominal
        elif 0 < i < duration:
            total += nominal * (1 + returned / 100)
        elif i == duration:
            total *= (1 + returned / 100)
            
        print("Bulan ke {} : {}".format(i, total))
        
    return total
print(invest(100000, 0.3, 2))

def invest_recursive(nominal, returned, duration):
    if duration == 0:
        return 0 + nominal
    else: 
        return nominal + invest(nominal * (1 + returned / 100), returned, duration - 1)

print(invest_recursive(100000, 0.3, 2))