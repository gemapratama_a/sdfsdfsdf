--1. Berikut adalah definisi fungsi length
--length :: [a] -> Int
--length [] = 0
--length (x:xs) = 1 + length xs
--Buatlah definisi fungsi length baru menggunakan map dan fold!
length' :: [a] -> Int
length' xs = foldl (+) 0 (map (\x -> 1) xs)

--2. Diberikan fungsi addUp ns = filter greaterOne (map addOne ns) dimana 
--greaterOne n = n > 1 dan addOne n = n + 1,  definisikan ulang fungsi tersebut
--dengan filter sebelum map, misalnya addUp ns = map fun1 (filter fun2 ns)!
--Prelude> addUp [0,1,2,3]
--[2,3,4]
greaterOne n = n > 1
addOne n = n + 1
greaterZero n = n > 0
addUp' ns = map addOne (filter greaterZero ns)


--3. Definisikan fungsi sum of the squares dari 1 sampai n!
--Prelude> sumOfSquares 3
--14
sumOfSquares3a n = foldl (+) 0 (map (\x -> x * x) [1..n])
sumOfSquares3b n = foldl (+) 0 ([x * x | x <- [1..n]])


--4. Definisikan fungsi yang mengembalikan jumlah bilangan kelipatan 5 dalam sebuah list!
--Prelude> multipleOf5 [1,2,3,4,5,6,7,8,9,10]
--2
multipleOf5 :: [Int] -> Int
multipleOf5 xs = length ([x | x <- xs, x `mod` 5 == 0])

--5. Definisikan fungsi total dimana total :: (Int -> Int) -> (Int -> Int) sehingga total
--f adalah fungsi ketika mendapat nilai n memberikan total dari f 0 + f 1 + … + f n!
--Prelude> total (+1) 3
--9
total :: (Int -> Int) -> (Int -> Int)
total f n = foldl (+) 0 ((map (\x -> f x) [0..n]))

--6. Buatlah fungsi reverse dengan menggunakan foldr!
--Prelude> reverse [1,2,3,4,5]
--[5,4,3,2,1]
reverse' xs = foldr (\acc x -> x ++ [acc]) [] xs 


--8. Buatlah definisi infinite list dari triple pythagoras. List tersebut terdiri dari 
--elemen triple bilangan bulat positif yang mengikut persamaan pythagoras x2 + y2 = z2!
--Prelude > take 6 pythaTriple
--[(3,4,5),(6,8,10),(5,12,13),(9,12,15),(8,15,17),(12,16,20)]
--Perhatian urutan penyusun list comprehension nya, coba mulai dari variable z!
pythaTriple = [(x,y,z) | z <- [1..], y <- [2..z-1], x <- [2..y-1], x*x + y*y == z*z]

--10. Buatlah fungsi noUpperAndIdent yang menghapus seluruh karakter kapital dan karakter
--non-alfabet dari argumen String yang diberikan! (Hint: Gunakan library function elem 
--dan isUpper)
--Prelude> noUpperAndIdent "FunPro MOOC"
--"unro"
noUpperAndIdent xs = [x | x <- xs, x `elem` ['a'..'z']]


main = do
  print (length' [3,2,1]) -- 1
  print (addUp' [0,1,2,3]) -- 2
  print (sumOfSquares3a 3) -- 3a
  print (sumOfSquares3b 3) -- 3b
  print (multipleOf5 [1,2,3,4,5,6,7,8,9,10]) -- 4
  print (total (+1) 3) -- 5
  print (reverse' [1,2,3,4,5]) -- 6
  print (take 6 pythaTriple) -- 8
  print (noUpperAndIdent "FunPro MOOC") -- 10


divisor n = [x | x <- [1..n], n `mod` x == 0]
-- Referensi:
-- http://zvon.org/other/haskell/Outputprelude/foldl_f.html
-- https://github.com/edahlgren/100-lengths-in-Haskell
-- https://stackoverflow.com/questions/21220606/pythagorean-triples-in-haskell-using-infinities-lists
-- https://stackoverflow.com/questions/30242668/remove-characters-from-string-in-haskell
-- https://stackoverflow.com/questions/26017352/why-can-you-reverse-list-with-foldl-but-not-with-foldr-in-haskell