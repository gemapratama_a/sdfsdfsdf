import Data.Char (isLetter)
import Data.Char (toLower)

--Buatlah fungsi sumOfSquares yang menerima sebuah list of integer dan mengembalikan sebuah integer 
--yang merupakan penjumlahan dari kuadrat elemen list input!
sumOfSquares :: [Integer] -> Integer
sumOfSquares list = sum squared 
    where squared = map (\x -> x^2) list
    

--Bilangan triangular adalah penjumlahan bilangan positif tersebut dengan seluruh bilangan bulat positif 
--sebelumnya. Contohnya bilangan triangular ke-5 adalah 5+4+3+2+1. Buatlah fungsi  triangular yang menerima
--bilangan bulat positif n dan mengembalikan bilangan triangular yang ke-n!
triangular :: Integer -> Integer
triangular 0 = 0
triangular n = n + triangular (n - 1)


--Buatlah fungsi power tanpa menggunakan fungsi pangkat yang sudah ada di Haskell, input dibatasi hanya 
--untuk bilangan bulat positif
power :: Integer -> Integer -> Integer
power x 0 = 1
power x y = x * power x (y - 1)

--Palindrome adalah kata yang dibaca sama dari depan ataupun belakang. Contohnya “Madam, I’m Adam”, “No lemon, 
--no melon” dan lain-lain. Buatlah sebuah fungsi yang menerima string  dan mengembalikan Boolean untuk 
--mengecek apakah string tersebut palindrome atau tidak.
removeNonLetters = filter isLetter
toLowerStr xs = map toLower xs


isPalindrome :: String -> Bool
isPalindrome [] = True
isPalindrome xs = loweredStripped == reverse loweredStripped
    where {
        loweredStripped = removeNonLetters lowered
        where {
            lowered = toLowerStr xs;
        }
    }

main = do 
   print (sumOfSquares [1,2,3])
   print (triangular 5)
   print (power 3 2)
   print (isPalindrome "Madam, I'm Adam")

   

-- Referensi:
-- https://riptutorial.com/haskell/example/6125/factorial
-- https://wiki.haskell.org/Let_vs._Where
-- http://www.codecodex.com/wiki/Remove_non-letters_from_a_string#Haskell
-- https://stackoverflow.com/questions/29068636/haskell-nested-where-clauses