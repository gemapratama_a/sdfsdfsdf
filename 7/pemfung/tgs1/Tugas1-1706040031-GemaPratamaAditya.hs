-- Buatlah fungsi circleArea yaitu fungsi yang menerima parameter 
-- r Double dan mengembalikan luas dari lingkaran dengan jari-jari r dengan pi 3.14.
circleA :: Double -> Double
circleA r = 3.14 * r * r


--Buatlah fungsi isTriangle yang menerima 3 buah parameter bertipe Integer 
--yang merupakan sisi-sisi segitiga dan mengembalikan Boolean yang menyatakan 
--apakah segitiga dapat dibuat berdasarkan 3 buah sisi tersebut.
--isTriangle :: Integer -> Integer -> Integer -> Bool
--isTriangle x y z = if x + y <= z || y + z <= x || x + z <= y then False else True
isTriangle :: Integer -> Integer -> Integer -> Bool
isTriangle a b c
    | a + b <= c || b + c <= a || a + c <= b = False
    | otherwise = True


--Buatlah fungsi listSum yang menerima parameter List Integer dan mengembalikan 
--total dari elemen didalam list tersebut.
listSum :: [Integer] -> Integer
listSum [] = 0
listSum (x:xs) = x + listSum xs

--Buatlah fungsi listSumArea yang menerima parameter List circleArea dan 
--mengembalikan total dari semua area dididalam list tersebut.
listSumArea :: [Double] -> Double
listSumArea [] = 0.0
listSumArea (x:xs) = x + listSumArea xs


--Buatlah implementasi reverseList.
reverseList :: [Integer] -> [Integer]
reverseList [] = []
reverseList (x:xs) = reverseList xs ++ [x]

--Buatlah implementasi algoritma quicksort descending.
quicksort1 :: (Ord a) => [a] -> [a]
quicksort1 [] = []
quicksort1 (x:xs) =
  let biggerSorted = quicksort1 [a | a <- xs, a >= x]
      smallerSorted = quicksort1 [a | a <- xs, a < x]
  in  biggerSorted ++ [x] ++ smallerSorted




main = do 
   print (circleA 3.2)
   print (isTriangle 2 3 4)
   print (listSum [2,3,4])
   print (listSumArea [circleA 2,circleA 3,circleA 4])
   print (reverseList [1,2,3,4])
   print (quicksort1 [4,1,5,6,1,3])


-- Referensi:
-- https://www.geeksforgeeks.org/check-whether-triangle-valid-not-sides-given/
-- https://stackoverflow.com/questions/26847192/reverse-a-list-in-haskell
-- http://www.newthinktank.com/2015/08/learn-haskell-one-video/
-- https://stackoverflow.com/questions/53287115/taking-sum-of-list-in-haskell
-- https://mmhaskell.com/blog/2019/5/13/quicksort-with-haskell