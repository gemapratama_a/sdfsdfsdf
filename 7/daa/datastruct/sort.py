import time
import datetime
import random
import sys

def bubblesort(array):
	n = len(array)
	for i in range(n - 1):
		for j in range(n - i - 1):
			if array[j] > array[j+1]:
				array[j], array[j+1] = array[j+1], array[j]

def insertionsort(array):
	
	for step in range(1, len(array)):
		key = array[step]
		j = step - 1
		
		# Compare key with each element on the left of it until an element smaller than it is found
		# For descending order, change key<array[j] to key>array[j].        
		while j >= 0 and key < array[j]:
			array[j + 1] = array[j]
			j = j - 1
		
		# Place key at after the element just smaller than it.
		array[j + 1] = key
def selectionsort(array):
	for i in range(len(array)):
		min_index = i
		for j in range(i + 1, len(array)):
			if array[min_index] > array[j]:
				min_index = j
		
		array[i], array[min_index] = array[min_index], array[i]

def mergesort(array):
	if len(array) > 1:
		mid = len(array) // 2
		left = array[:mid]
		right = array[mid:]

		mergesort(left)
		mergesort(right)

		i = 0
		j = 0
		k = 0

		while i < len(left) and j < len(right):
			if left[i] < right[j]:
				array[k] = left[i]
				i += 1
			else:
				array[k] = right[j]
				j += 1
			k += 1

		while i < len(left):
			array[k] = left[i]
			i += 1
			k += 1

		while j < len(right):
			array[k] = right[j]
			j += 1
			k += 1
  

def quicksort(a, p, r): 
	def partition(a, p, r):
		x = a[p]
		i = p
		j = r

		while True:
			while i < r and a[i] < x:
				i += 1
			while j < p and a[j] > x:
				j -= 1
			
			if i < j:
				temp = a[i]
				i += 1
				a[i] = a[j]
				j -= 1
				a[j] = temp
			else:
				return j

	while p < r:
		q = partition(a, p, r)
		if q - p <= r - q - 1:
			quicksort(a, p ,q)
			p = q + 1
		else:
			quicksort(a, q + 1, r)
			r = q
			



def heapify(arr, n, i): 
	largest = i
	l = 2 * i + 1
	r = 2 * i + 2 
 
	if l < n and arr[i] < arr[l]: 
		largest = l 
	if r < n and arr[largest] < arr[r]: 
		largest = r 
	if largest != i: 
		arr[i],arr[largest] = arr[largest],arr[i]
		heapify(arr, n, largest) 

def heapsort(arr): 
	n = len(arr) 
  
	for i in range(n//2 - 1, -1, -1): 
		heapify(arr, n, i) 
   
	for i in range(n-1, 0, -1): 
		arr[i], arr[0] = arr[0], arr[i]
		heapify(arr, i, 0) 

def time(func, array):
	start_time = datetime.datetime.now()
	func(array)
	end_time =  datetime.datetime.now()
	elapsed = (end_time - start_time).total_seconds() * 1000
	print("{}: {} ms".format(func, int(elapsed)))

def main():
	
	number_of_elements = 10000
	random_list = []
	for i in range(0, number_of_elements):
		random_list.append(int(random.randint(0, number_of_elements)))

	funcs = [bubblesort, selectionsort, mergesort, heapsort, insertionsort, radixsort, quicksort]
	print("[NUMBER OF ELEMENTS: {}]".format(number_of_elements))
	for func in funcs:
		if func.__name__ == 'quicksort':
			start_time = datetime.datetime.now()
			func(random_list, 0, len(random_list) - 1)
			end_time =  datetime.datetime.now()
			elapsed = (end_time - start_time).total_seconds() * 1000
			print("{}: {} ms".format(func.__name__, int(elapsed)))
			random.shuffle(random_list)
		else:
			start_time = datetime.datetime.now()
			func(random_list)
			end_time =  datetime.datetime.now()
			elapsed = (end_time - start_time).total_seconds() * 1000
			print("{}: {} ms".format(func.__name__, int(elapsed)))
			random.shuffle(random_list)




def countingsort(arr, exp):
	n = len(arr)
	output = [0] * n

	count = [0] * 10

	for i in range(n):
		index = arr[i] / exp
		count[int(int(index % 10))] += 1
	
	for i in range(1, 10):
		count[i] += count[i - 1]

	i = n - 1
	while i >= 0:
		index = arr[i] / exp
		output[count[int(index % 10)] - 1] = arr[i]
		count[int(index % 10)] -= 1
		i -= 1

def radixsort(arr):
	max1 = max(arr)
	exp = 1

	while max1 / exp > 0:
		countingsort(arr, exp)
		exp *= 10
if __name__ == '__main__':
	main()