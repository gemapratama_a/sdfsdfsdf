import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.Random;

class Sorting {

	public static void insertionsort(int[] input) {
		for (int i = 1; i < input.length; i++) { 
			int key = input[i]; 
			int j = i - 1;
			while (j >= 0 && input[j] > key) {
				input[j + 1] = input[j];
				j = j - 1;
			}
			input[j + 1] = key; 
		}
	}

	public static int lomutoPartition(int[] arr, int low, int high) {
		int pivot = arr[high];
		
		// Index of smaller element
		int i = (low - 1);
	
		for (int j = low; j <= high- 1; j++) {
			// If current element is smaller
			// than or equal to pivot
			if (arr[j] <= pivot) {
				i++; // increment index of
					// smaller element
				Swap(arr, i, j);
			}
		}
		Swap(arr, i + 1, high);
		return i + 1;
	}

	public static void Swap(int[] array, int position1, int position2) {
		int temp = array[position1]; 
		array[position1] = array[position2]; 
		array[position2] = temp; 
	}

	public static int hoarePartition(int[] arr, int low, int high) {
        int pivot = arr[low];
        int i = low - 1, j = high + 1;
 
        while (true) {
            // Find leftmost element greater
            // than or equal to pivot
            do {
                i++;
            } while (arr[i] < pivot);
 
            // Find rightmost element smaller
            // than or equal to pivot
            do {
                j--;
            } while (arr[j] > pivot);
 
            // If two pointers met.
            if (i >= j)
                return j;
            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
            // swap(arr[i], arr[j]);
        }
    }
	
	
	public static void quickSortHoare(int[] arr, int low, int high) {
        if (low < high) {
            /* pi is partitioning index,
            arr[p] is now at right place */
            int pi = hoarePartition(arr, low, high);
 
            // Separately sort elements before
            // partition and after partition
            quickSortHoare(arr, low, pi);
            quickSortHoare(arr, pi + 1, high);
        }
	}
	
	public static void quickSortLomuto(int []arr, int low, int high) {
		if (low < high)
		{
			/* pi is partitioning index, 
			arr[p] is now at right place */
			int pi = lomutoPartition(arr, low, high);
	
			// Separately sort elements before
			// partition and after partition
			quickSortLomuto(arr, low, pi - 1);
			quickSortLomuto(arr, pi + 1, high);
		}
	}
	
	public static void merge(int[] arr, int l, int m, int r) { 

        int n1 = m - l + 1; 
        int n2 = r - m; 
        int L[] = new int[n1]; 
        int R[] = new int[n2]; 
        for (int i = 0; i < n1; ++i) 
            L[i] = arr[l + i]; 
        for (int j = 0; j < n2; ++j) 
            R[j] = arr[m + 1 + j]; 

        int i = 0, j = 0; 
        int k = l; 
        while (i < n1 && j < n2) { 
            if (L[i] <= R[j]) { 
                arr[k] = L[i]; 
                i++; 
            } 
            else { 
                arr[k] = R[j]; 
                j++; 
            } 
            k++; 
        } 
        while (i < n1) { 
            arr[k] = L[i]; 
            i++; 
            k++; 
        } 
        while (j < n2) { 
            arr[k] = R[j]; 
            j++; 
            k++; 
        } 
    } 
  
    public static void mergesort(int[] arr, int l, int r) { 
        if (l < r) { 
            int m = (l + r) / 2; 
  
            mergesort(arr, l, m); 
            mergesort(arr, m + 1, r); 
            merge(arr, l, m, r); 
        } 
	} 
	
	public static void bubbleSort(int[] arr) { 
        int n = arr.length; 
        for (int i = 0; i < n-1; i++) 
            for (int j = 0; j < n-i-1; j++) 
                if (arr[j] > arr[j+1]) { 
                    int temp = arr[j]; 
                    arr[j] = arr[j+1]; 
                    arr[j+1] = temp; 
                } 
	}

	public static void selectionsort(int[] arr) { 
        int n = arr.length; 
  
        for (int i = 0; i < n-1; i++) { 
            int min_idx = i; 
            for (int j = i+1; j < n; j++) 
                if (arr[j] < arr[min_idx]) 
                    min_idx = j; 
  
            int temp = arr[min_idx]; 
            arr[min_idx] = arr[i]; 
            arr[i] = temp; 
        } 
	} 
	
	public static void printArray(int[] arr) {
		for (Integer i : arr) {
			System.out.print(i + " ");
		}
		System.out.println();
	}

	 
	public static void main(String[] args) {
		/*
		Random rd = new Random();
		int NUMBER_OF_ELEMENTS = 10000;
      	int[] arr = new Integer[NUMBER_OF_ELEMENTS];
      	for (int i = 0; i < arr.length; i++) {
        	 arr[i] = rd.nextInt(NUMBER_OF_ELEMENTS);
		}

		System.out.println("[NUMBER OF ELEMENTS: " + NUMBER_OF_ELEMENTS + "]");

		long startTime = System.nanoTime();
		selectionsort(arr);
		long endTime = System.nanoTime();
    	long durationInNano = (endTime - startTime); 
		long durationInMillis = TimeUnit.NANOSECONDS.toMillis(durationInNano);
		System.out.println("Selection sort: " + durationInMillis + " ms");
		List<Integer> intList = Arrays.asList(arr);
		Collections.shuffle(intList);
		intList.toArray(arr);

		startTime = System.nanoTime();
		bubbleSort(arr);
		endTime = System.nanoTime();
    	durationInNano = (endTime - startTime); 
		durationInMillis = TimeUnit.NANOSECONDS.toMillis(durationInNano);
		System.out.println("Bubble sort: " + durationInMillis + " ms");
		//List<Integer> intList = Arrays.asList(arr);
		Collections.shuffle(intList);
		intList.toArray(arr);

		startTime = System.nanoTime();
		mergesort(arr, 0, arr.length - 1);
		endTime = System.nanoTime();
    	durationInNano = (endTime - startTime); 
		durationInMillis = TimeUnit.NANOSECONDS.toMillis(durationInNano);
		System.out.println("Merge sort: " + durationInMillis + " ms");
		//System.out.println(Arrays.asList(arr));
		//List<Integer> intList = Arrays.asList(arr);
		Collections.shuffle(intList);
		intList.toArray(arr);

		startTime = System.nanoTime();
		insertionsort(arr);
		endTime = System.nanoTime();
    	durationInNano = (endTime - startTime); 
		durationInMillis = TimeUnit.NANOSECONDS.toMillis(durationInNano);
		System.out.println("Insertion sort: " + durationInMillis + " ms");
		//List<Integer> intList = Arrays.asList(arr);
		Collections.shuffle(intList);
		intList.toArray(arr);

		startTime = System.nanoTime();
		quicksort(arr, 0, arr.length - 1);
		endTime = System.nanoTime();
    	durationInNano = (endTime - startTime); 
		durationInMillis = TimeUnit.NANOSECONDS.toMillis(durationInNano);
		System.out.println("Quick sort: " + durationInMillis + " ms");
		//List<Integer> intList = Arrays.asList(arr);
		Collections.shuffle(intList);
		intList.toArray(arr);
		*/
		System.out.println("Before: ");
		
		int[] arr = new int[] {9, 7, 5, 11, 12, 2, 14, 3, 10, 6};
		printArray(arr);
		quickSortLomuto(arr, 0, arr.length - 1);
		//quickSortHoare(arr, 0, arr.length - 1);
		System.out.println("After: ");
		printArray(arr);
	}
}

