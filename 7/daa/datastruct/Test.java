import java.util.*;

class Test {

	public static void main(String[] args) {

		Random rd = new Random();
		Integer[] arr = new Integer[10];
		for (int i = 0; i < arr.length; i++) {
		   arr[i] = rd.nextInt();
		}
		System.out.println("BEFORE");
		for (Integer i : arr) {
			System.out.print(i + " ");
		}
		System.out.println();
		
		List<Integer> intList = Arrays.asList(arr);
		Collections.shuffle(intList);
		intList.toArray(arr);



		System.out.println("AFTER");
		for (Integer i : arr) {
			System.out.print(i + " ");
		}
		System.out.println();

	}
}