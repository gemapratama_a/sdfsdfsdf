import java.util.ArrayList;

class BinarySearchTree {
	Node root; 
   
    public BinarySearchTree() {  
        root = null;  
	} 
	
	public void insert(int key) { 
		root = insertRecursive(root, key); 
	}
	public Node insertRecursive(Node root, int key) { 
  
        /* If the tree is empty, return a new node */
        if (root == null) { 
            root = new Node(key); 
            return root; 
        } 
  
        /* Otherwise, recur down the tree */
        if (key < root.key) 
            root.left = insertRecursive(root.left, key); 
        else if (key > root.key) 
            root.right = insertRecursive(root.right, key); 
  
        /* return the (unchanged) node pointer */
        return root; 
	} 

	public Node deleteIterative(Node root, int key) {
		Node current = root;
		Node previous = null;

		while (current != null && current.key != key) {
			previous = current;
			if (current.key < key) {
				current = current.right;
			} else {
				current = current.left;
			}
		}

		if (current == null) {
			System.out.println("Key not found: " + key);
			return root;
		}

		if (current.right == null || current.left == null) {
			Node newCurrent = null;

			if (current.left == null) {
				newCurrent = current.right;
			} else {
				newCurrent = current.left;
			}

			if (previous == null) {
				return newCurrent;
			}

			if (current == previous.left) {
				previous.left = newCurrent;
			} else {
				previous.right = newCurrent;
			}

			current = null;
		} else {
			Node p = null;
			Node temp = null;

			temp = current.right;
			while (temp.left != null) {
				p = temp;
				temp = temp.left;
			}

			if (p != null) {
				p.left = temp.right;
			} else {
				current.right = temp.right;
			}

			current.key = temp.key;
			temp = null;
		}
		return root;
	}

	
	public void inorder()  { 
		inorderRecursive(root); 
		System.out.println();
	}
	
	public void inorderRecursive(Node root) { 
		ArrayList<Integer> temp = new ArrayList<Integer>();
        if (root != null) { 
			inorderRecursive(root.left); 
			temp.add(root.key);
			//System.out.print(root.key + " ");
			//System.out.println();
            inorderRecursive(root.right); 
		} 
		for (Integer i : temp) {
			System.out.print(i + " ");
		}
		//System.out.println();
	} 
	
	public static void main(String[] args) { 
        BinarySearchTree tree = new BinarySearchTree(); 
  
		// int[] values = {50, 30, 20, 40, 70, 60, 80};
		int[] values = {10, 7, 5, 8 , 15, 11, 18};
		for (Integer v : values) {
			tree.insert(v);
		} 
		tree.inorder();
		
		Node newNode = tree.deleteIterative(tree.root, 11);

		tree.inorder();
	
    } 
}


class Node { 
	int key; 
	Node left, right; 

	public Node(int item) { 
		key = item; 
		left = right = null; 
	} 
} 