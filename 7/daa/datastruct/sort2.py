import time
import datetime
import random
import sys

def bubblesort(array):
	n = len(array)
	for i in range(n - 1):
		for j in range(n - i - 1):
			if array[j] > array[j+1]:
				array[j], array[j+1] = array[j+1], array[j]

def insertionsort(arr):   
	for i in range(1, len(arr)): 
		key = arr[i]
		j = i-1
		while j >= 0 and key < arr[j] : 
				arr[j + 1] = arr[j] 
				j -= 1
		arr[j + 1] = key 
  
def selectionsort(array):
	for i in range(len(array)):
		min_index = i
		for j in range(i + 1, len(array)):
			if array[min_index] > array[j]:
				min_index = j
		
		array[i], array[min_index] = array[min_index], array[i]

def mergesort(array):
	if len(array) > 1:
		mid = len(array) // 2
		left = array[:mid]
		right = array[mid:]

		mergesort(left)
		mergesort(right)

		i = 0
		j = 0
		k = 0

		while i < len(left) and j < len(right):
			if left[i] < right[j]:
				array[k] = left[i]
				i += 1
			else:
				array[k] = right[j]
				j += 1
			k += 1

		while i < len(left):
			array[k] = left[i]
			i += 1
			k += 1

		while j < len(right):
			array[k] = right[j]
			j += 1
			k += 1
  
def quicksort(arr, low, high): 
	def partition(arr, low, high): 
		i = low - 1 
		pivot = arr[high]
	
		for j in range(low, high): 
			if arr[j] < pivot: 
			
				i += 1 
				arr[i], arr[j] = arr[j], arr[i] 
	
		arr[i+1], arr[high] = arr[high], arr[i+1] 
		return i + 1 
  
	if low < high: 
		pi = partition(arr, low, high) 
		quicksort(arr, low, pi - 1) 
		quicksort(arr, pi + 1, high) 



def heapify(arr, n, i): 
	largest = i
	l = 2 * i + 1
	r = 2 * i + 2 
 
	if l < n and arr[i] < arr[l]: 
		largest = l 
	if r < n and arr[largest] < arr[r]: 
		largest = r 
	if largest != i: 
		arr[i],arr[largest] = arr[largest],arr[i]
		heapify(arr, n, largest) 

def heapsort(arr): 
	n = len(arr) 
  
	for i in range(n//2 - 1, -1, -1): 
		heapify(arr, n, i) 
   
	for i in range(n-1, 0, -1): 
		arr[i], arr[0] = arr[0], arr[i]
		heapify(arr, i, 0) 

def main():
	random_list = []
	arg = input("Method: (bubble, insertion, selection, merge, heap, quick):\n>>> ")
	number_of_elements = 1000

	for i in range(0, number_of_elements):
		random_list.append(int(random.randint(0, number_of_elements)))

	start_time = datetime.datetime.now()

	if arg == 'quick':
		command = arg + "sort(random_list, {}, {})".format(0, len(random_list) - 1)	
	else:
		command = arg + "sort(random_list)"
	
	print("Before: ", random_list)
	eval(command)
	print("After: ", random_list)

	end_time =  datetime.datetime.now()
	elapsed = (end_time - start_time).total_seconds() * 1000
	
	print("Time: {} ms".format(int(elapsed)))

def countingsort(arr, exp):
	n = len(arr)
	output = [0] * n

	count = [0] * 10

	for i in range(n):
		index = arr[i] / exp
		count[int(int(index % 10))] += 1
	
	for i in range(1, 10):
		count[i] += count[i - 1]

	i = n - 1
	while i >= 0:
		index = arr[i] / exp
		output[count[int(index % 10)] - 1] = arr[i]
		count[int(index % 10)] -= 1
		i -= 1

def radixsort(arr):
	max1 = max(arr)
	exp = 1

	while max1 / exp > 0:
		countingsort(arr, exp)
		exp *= 10
if __name__ == '__main__':
	main()