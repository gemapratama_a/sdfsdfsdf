class Node:
	
    def __init__(self, value):
        self.left = None
        self.right = None
        self.value = value

class BinarySearchTree:
	def __init__(self):
		self.root = None

	def insert(self, value: int):
		root = insert_recursive(root, value)

	def insert_recursive(self, root, value):
		if root == None:
			root = Node(value)
			return root

		if value < root.value:
			root.left = insert_recursive(root.left, value)
		elif value > root.value:
			root.right = insert_recursive(root.right, value)
		
		return root

	def inorder(self, root):
		inorder_recursive(root)

	def inorder_recursive(self, root):
		if root != None:
			inorder_recursive(root.left)
			print(root.value)
			inorder_recursive(root.right)

def main():
	bst = BinarySearchTree()
	values = [50, 30, 20, 40, 70, 60, 80]
	for value in values:
		bst.insert(value)

	bst.inorder()

if __name__ == '__main__':
	main()