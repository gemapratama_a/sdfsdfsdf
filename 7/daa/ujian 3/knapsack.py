def knapSack(MAX_WEIGHT, weights, values, n): 
    K = [[0 for x in range(MAX_WEIGHT + 1)] for x in range(n + 1)] 
  
    # Build table K[][] in bottom up manner 
    for i in range(n + 1): 
        for w in range(MAX_WEIGHT + 1): 
            if i == 0 or w == 0: 
                K[i][w] = 0
            elif weights[i-1] <= w: 
                K[i][w] = max(values[i-1] + K[i-1][w-weights[i-1]], K[i-1][w]) 
            else: 
                K[i][w] = K[i-1][w] 
  
    answer = K[n][MAX_WEIGHT] 
    w = MAX_WEIGHT 
    
    print("Weights: ", end=" ")
    for i in range(n, 0, -1): 
        if answer <= 0: 
            break
        # either the result comes from the 
        # top (K[i-1][w]) or from (values[i-1] 
        # + K[i-1] [w-weights[i-1]]) as in Knapsack 
        # table. If it comes from the latter 
        # one/ it means the item is included. 
        if answer == K[i - 1][w]: 
            continue
        else: 
  
            # This item is included. 
            print(weights[i - 1], end=" ") 
              
            # Since this weight is included 
            # its value is deducted 
            answer = answer - values[i - 1] 
            w = w - weights[i - 1] 
    print("")
    return K[n][MAX_WEIGHT] 

if __name__ == '__main__':
    values = input("Values/Profit: ").split(" ")
    weights =input("Weights:       ").split(" ")
    for i in range(len(values)):
        values[i] = int(values[i])
        weights[i] = int(weights[i])
    MAX_WEIGHT = int(input("Max weight:    "))
    n = len(values) 
    print("=========ANS=========")
    print("Max profit: ", knapSack(MAX_WEIGHT, weights, values, n)) 
    
    # This code is contributed by Bhavya Jain 