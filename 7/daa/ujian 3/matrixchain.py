def MatrixChainOrder(p, n): 
    # For simplicity of the program,  
    # one extra row and one 
    # extra column are allocated in m[][].   
    # 0th row and 0th 
    # column of m[][] are not used 
    m = [[0 for x in range(n)] for x in range(n)] 
    s = [[0 for x in range(n)] for x in range(n)] 
  
    # m[i, j] = Minimum number of scalar  
    # multiplications needed 
    # to compute the matrix A[i]A[i + 1]...A[j] =  
    # A[i..j] where 
    # dimension of A[i] is p[i-1] x p[i] 
  
    # cost is zero when multiplying one matrix. 
    for i in range(1, n): 
        m[i][i] = 0
  
    # L is chain length. 
    for L in range(2, n): 
        for i in range(1, n - L + 1): 
            j = i + L - 1
            m[i][j] = float('inf')
            for k in range(i, j): 
  
                # q = cost / scalar multiplications 
                #print("i={}, j={}, k={}")
               
                q = m[i][k] + m[k + 1][j] + p[i-1]*p[k]*p[j] 
                #if L == 3:
                    #print("q = m[{}][{}] + m[{} + 1][{}] + p[{} - 1]*p[{}]*p[{}]".format(i,k,k,j,i,k,j))
                    #print("q = ", q)
                if q < m[i][j]: 
                    m[i][j] = q 
                    #if L == 3:
                        #print("[masuk if] M[{}][{}]={}".format(i, j, q))
                    s[i][j] = k
                    if L == 2:
                        print("s[{}][{}]={}".format(i,j,k))
  
    print("M")
    for row in m:
        print(row)
        
    print("S")
    for row in s:
        print(row)
   # print(m)
    #print(s)
    return m[1][n-1] 
  
if __name__ == '__main__':
    # Driver code 
    arr = [3, 5, 4, 10, 8, 2, 6] 
    size = len(arr) 
    
    print("Minimum number of multiplications is: ", MatrixChainOrder(arr, size))
    # This Code is contributed by Bhavya Jain 