def findDuplicate(nums):
    tortoise = nums[0]
    hare = nums[0]
    while True:
        tortoise = nums[tortoise]
        hare = nums[nums[hare]]
        if tortoise == hare:
            break
    
    tortoise = nums[0]  # Meletakkan tortoise ke awal list
    
    while tortoise != hare:             # Melakukan while loop, sampai mereka berdua bertemu,
        tortoise = nums[tortoise]       # namun kali ini mereka berdua hanya maju satu node per iterasi.
        hare = nums[hare]
    
    return tortoise    
     

#print(findDuplicate([3,1,3,4,2]))
#print(findDuplicate([1,2,3,4,5,6]))
#print(findDuplicate([5,15,2,2,1,2]))
print(findDuplicate([1, 4, 3, 5, 6, 1, 2]))