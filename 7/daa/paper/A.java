
class Node {
    int value;
    Node next;
}

class TurtleAndHare {
 
    public static Node newNode(int value) {
        Node temp = new Node();
        temp.value = value;
        temp.next = null;
        return temp;
    }
 
    public static void printList(Node head) {
        while (head != null) {
            System.out.print(head.value + " ");
            head = head.next;
        }
        System.out.println();
    }
 

    public static Node detectCycle(Node head) {

        // Jika linked list hanya memiliki <= 1 buah node
        if (head == null || head.next == null) {
            return null;
        }
    
        // Menjalankan algoritma tortoise and hare
        Node tortoise = head;
        Node hare = head;

        tortoise = tortoise.next;
        hare = hare.next.next;
 
        while (hare != null && hare.next != null) {
            if (tortoise == hare) {
                break;
            }
      
            tortoise = tortoise.next;
            hare = hare.next.next;
        }
 
        // Jika tidak ditemukan cycle
        if (tortoise != hare) {
            return null;
        }

        // Jika ditemukan cycle, cari titik awal cycle tesebut
 

        // Meletakkan tortoise pada awal list
        tortoise = head;            

        // Sekarang tortoise dan hare bersama-sama berjalan hanya satu node tiap iterasi
        // sampai bertemu
        while (tortoise != hare) {
            tortoise = tortoise.next;
            hare = hare.next;
        }
 
        return tortoise;
    }   
 
    public static void main(String[] args) {

        Node head = newNode(50);
        head.next = newNode(20);
        head.next.next = newNode(15);
        head.next.next.next = newNode(4);
        head.next.next.next.next = newNode(10);
 
        // Membuat suatu cycle. 10 akan menunjuk ke 15. Dari sini, 
        // node dengan value 15 adalah titik awal dari cycle.
        head.next.next.next.next.next = head.next.next;
 
        Node result = detectCycle(head);
        if (result == null) {
            System.out.print("Tidak ditemukan cycle");
        } else {
            System.out.print("Ditemukan cycle. Titik awal cycle ada pada node bervalue " + result.value);
        }
    
    }
}