class Node: 
    def __init__(self, data): 
        self.data = data 
        self.next = None
  
class LinkedList: 
    def __init__(self): 
        self.head = None

    def insert(self, new_data): 
        new_node = Node(new_data) 
        new_node.next = self.head 
        self.head = new_node 
        
    
    def append(self, new_data): 
        new_node = Node(new_data) 
        
        if self.head is None: 
            self.head = new_node 
            return
        
        last = self.head 
        while last.next: 
            last = last.next
        
        last.next =  new_node 

    
    def printList(self): 
        temp = self.head 
        while temp: 
            print(temp.data, end=" ")
            temp = temp.next
        print()


    def turtleAndHare(self): 
        tortoise = self.head 
        hare = self.head 
        
        tortoise = tortoise.next
        hare = hare.next.next
        
        while hare != None and tortoise.next != None: 
            if tortoise == hare: 
                #print("Ditemukan loop")
                break
            
            tortoise = tortoise.next
            hare = hare.next.next
            
        if tortoise != hare:
            return None
        
        tortoise = self.head # Meletakkan tortoise ke awal list
    
        while tortoise != hare:             # Melakukan while loop, sampai mereka berdua bertemu,
            tortoise = tortoise.next      # namun kali ini mereka berdua hanya maju satu node per iterasi.
            hare = tortoise.next
    
        print("Titik awal cycle: {}".format(tortoise.data))
    
if __name__ == '__main__':
    linked_list = LinkedList() 
    
    insert_values = [50, 20, 15, 4, 10]
    #insert_values = [20, 4, 15, 10, 5]
    for v in insert_values:
        linked_list.insert(v)
    linked_list.printList()

    # Buat suatu loop
                #head.next.next.next.next.next = head.next.next;
    linked_list.head.next.next.next.next.next = linked_list.head.next.next
    linked_list.turtleAndHare()
