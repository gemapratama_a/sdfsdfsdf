import java.util.Arrays;

public class QuickSort {

    public static void sort(int[] array, int l, int r) {
        if (l < r) {
            int pivot = array[l];
            int i = l;
            int j = r;
            while (i < j) {
                i++;
                while (i <= r && array[i] < pivot) {
                    i++;
                }
                while (j >= l && array[j] > pivot) {
                    j--;
                }
                if (i <= r && i < j) {
                    swap(array, i, j);
                }
            }

            swap(array, l, j);
            printArray(array);
            sort(array, l, j - 1);
            sort(array, j + 1, r);
        }
    }

    public static void swap(int[] array, int i, int j) {
        if (i >= 0 && j >= 0 && i < array.length && j < array.length) {
            int tmp = array[i];
            array[i] = array[j];
            array[j] = tmp;
        }
    }

    public static void printArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + ", ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[] array = new int[] {7, 11, 14, 6, 9, 4, 3, 12};

        sort(array, 0, array.length - 1);
        System.out.println("Sorted: " + Arrays.toString(array));
    }

}