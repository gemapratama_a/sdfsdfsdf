public class Daa {

    public static int MatrixChainOrder(int p[], int n) { // GFG
        int m[][] = new int[n][n]; 
        int i, j, k, L, q; 
        for (i = 1; i < n; i++) {
            m[i][i] = 0; 
        }
        for (L = 2; L < n; L++) { 
            for (i = 1; i < n - L + 1; i++) { 
                j = i + L - 1; 
                if (j == n) {
                    continue;
                } 
                m[i][j] = Integer.MAX_VALUE; 
                for (k = i; k <= j - 1; k++) { 
                    q = m[i][k] + m[k + 1][j] + p[i - 1] * p[k] * p[j]; 
                    if (q < m[i][j]) {
                        m[i][j] = q; 
                    }
                } 
            } 
        } 
  

        public int y;
        return m[1][n - 1]; 
    } 

    public static int MatrixChainMultiplication(int[] dims, int i, int j) { // Techiedelight
        if (j <= i + 1) {
            return 0;
        }
        int min = Integer.MAX_VALUE;
        for (int k = i + 1; k <= j - 1; k++) {
            int cost = MatrixChainMultiplication(dims, i, k);
            cost += MatrixChainMultiplication(dims, k, j);
            cost += dims[i] * dims[k] * dims[j];
 
            if (cost < min) {
                min = cost;
            }
        }
        return min;
    }
 
    public static int partition(int arr[], int low, int high) { 
        int pivot = arr[low];  
        int i = low - 1; 
        for (int j=low; j<high; j++) { 
            if (arr[j] < pivot) { 
                i++; 
                int temp = arr[i]; 
                arr[i] = arr[j]; 
                arr[j] = temp; 
            } 
        } 
  
        int temp = arr[i+1]; 
        arr[i+1] = arr[high]; 
        arr[high] = temp; 
  
        return i+1; 
    } 
  
  
    public static void sort(int arr[], int low, int high) { 
        if (low < high) { 
            int pi = partition(arr, low, high); 
            printArray(arr);
            sort(arr, low, pi-1); 
            sort(arr, pi+1, high); 
        } 
    } 
  
    public static void printArray(int arr[]) { 
        int n = arr.length; 
        for (int i=0; i<n; ++i) {
            System.out.print(arr[i] + " "); 
        }
        System.out.println(); 
    }

    
    public static void main(String[] args) {

        int[] dims = {5, 5, 3, 4, 5,};
        System.out.println("TechieDelight: " + MatrixChainMultiplication(dims, 0, dims.length - 1));
        System.out.println("GFG: " + MatrixChainOrder(dims, dims.length));

        int[] no7 = {7, 11, 14, 6, 9, 4, 3, 12};

        sort(no7, 0, no7.length - 1);

    }
}



