import java.util.HashMap;

public class FizzBuzz {
    
    public static void main(String[] args) {
        HashMap<Integer, String> conditions = new HashMap<Integer, String>();

        conditions.put(3, "fizz");
        conditions.put(5, "buzz");
        //conditions.put(3, "fizz");
        //conditions.put(3, "fizz");
        for (int i = 1; i <= 100; i++) {
            String result = "";
            for (Integer x : conditions.keySet()) {
                if (i % x == 0) {
                    result += conditions.get(x);
                }
            }
            if (result.equals("")) {
                result = String.valueOf(i);
            }

            System.out.println(result);
        }


    }
}