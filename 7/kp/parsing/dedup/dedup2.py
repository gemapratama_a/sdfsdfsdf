csv_name = input("Masukkan nama csv: (contoh: duplicate.csv)\n>>> ")
output = input("Masukkan nama keluaran csv yang mengandung duplikasi (jika terdeteksi): (contoh: detected.csv)\n>>> ")

entries = []
duplicate_entries = []
with open(csv_name, 'r') as my_file:
    for line in my_file:
        columns = line.strip().split(',')
        if columns[2] not in entries:
            entries.append(columns[2])
        else:
            duplicate_entries.append(columns[2]) 

if len(duplicate_entries) > 0:
    with open(output, 'w') as out_file:
        with open(csv_name, 'r') as my_file:
            for line in my_file:
                columns = line.split(',')
                if columns[2] in duplicate_entries:
                    print(line)
                    out_file.write(line)
else:
    print("No repetitions")
