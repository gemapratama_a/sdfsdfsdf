import os

entries = []
duplicate_entries = []

csv_name = input("Masukkan nama csv: (contoh: duplicate.csv)\n>>> ")
output = input("Masukkan nama keluaran csv yang mengandung duplikasi (jika terdeteksi): (contoh: detected.csv)\n>>> ")

input_header = None
with open(csv_name, 'r',encoding='utf8', errors='ignore') as my_file:
    input_header = next(my_file)
    for line in my_file:
        columns = line.strip().split(',')
        
        if columns[2] not in entries:
            entries.append(columns[2])
        else:
            duplicate_entries.append(columns[2]) 

if len(duplicate_entries) > 0:
    print("Terdeteksi duplikasi: ")
    with open('temp.csv', 'w', encoding='utf8', errors='ignore') as out_file:
        out_file.write(input_header)
        with open(csv_name, 'r', encoding='utf8', errors='ignore') as my_file:
            for line in my_file:
                columns = line.split(',')
                if columns[2] in duplicate_entries:
                    print(line)
                    out_file.write(line)
    print("\n\nDuplikasi yang terdeteksi dapat dilihat di", output)

    with open('temp.csv', encoding='utf8', errors='ignore') as input, open(output, 'w', encoding='utf8', errors='ignore') as output:
        non_blank = (line for line in input if line.strip())
        output.writelines(non_blank)
    os.remove("temp.csv")
else:
    print("Tidak terdeteksi adanya duplikasi")
    
    

    
