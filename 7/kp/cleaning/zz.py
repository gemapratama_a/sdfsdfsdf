import pip
def install(package):
	if hasattr(pip, 'main'):
		pip.main(['install', package])
	else:
		pip._internal.main(['install', package])

def main():
	try:
		install('pandas')
	except:
		print('boo')

if __name__ == '__main__':
	main()