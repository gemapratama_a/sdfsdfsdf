1. Tampilkan nama penjaga kantin beserta nama kantin/counter yang dijaganya.

SELECT DISTINCT P.nama AS nama_penjaga, C.nama AS nama_counter
FROM PENJAGA_KANTIN PK, PENGGUNA P, COUNTER C, TRANSAKSI_PEMBELIAN TP
WHERE TP.id_penjaga_kantin = PK.no_ktp
AND TP.id_counter = C.id
AND PK.no_ktp = P.no_ktp;

     nama_penjaga      |  nama_counter
-----------------------+-----------------
 Mila Karmila          | Ayam Mas Fu'ad
 Aisyahrani            | Ayam Mas Jep
 Dimas Aditya Ramadhan | Baso
 Syifa Nur Aini        | Sate Sop
 Dimas Aditya Ramadhan | Rawonx
 Adelia Oktavia        | Minuman
 Aisyahrani            | Ayam Mas Fu'ad
 Adelia Oktavia        | Soto
 Adelia Oktavia        | Sate Sop
 Dimas Aditya Ramadhan | Lotek n Karedok
 Mila Karmila          | Sate Sop
(11 rows)



2. Tampilkan semua informasi counter yang pernah mempunyai denda lebih dari 40.000
beserta nama ownernya

SELECT C.id,COUNT(denda),SUM(denda)--, C.nama, C.tanggal_perubahan_status, C.tanggal_pengajuan, C.proposal, C.saldo_e_wallet, 
--C.status_penerimaan, C.status_kemitraan, C.no_ktp_owner
FROM COUNTER C, TRANSAKSI_PEMBAYARAN_TAGIHAN TPT, TAGIHAN_OPERASIONAL T, OWNER O, PENGGUNA P
WHERE TPT.id_counter = T.id_counter
AND T.id_counter = C.id
AND C.no_ktp_owner = O.no_ktp 
AND O.no_ktp = P.no_ktp
GROUP BY C.id, C.nama, C.tanggal_perubahan_status, C.tanggal_pengajuan, C.proposal, C.saldo_e_wallet, 
C.status_penerimaan, C.status_kemitraan, C.no_ktp_owner
HAVING sum(denda) > 40000;


3. Tampilkan nama counter beserta nama menu yang dijual oleh counter tersebut jika
pemiliknya adalah perempuan.

SELECT P.nama, nama_menu
FROM MENU M, COUNTER C, OWNER O, PENGGUNA P
WHERE C.id = M.id_counter	
AND C.no_ktp_owner = O.no_ktp
AND O.no_ktp = P.no_ktp
AND P.jenis_kelamin = 'P';

     nama      |    nama_menu
---------------+-----------------
 Tri amanda    | Ayam Bakar
 Tri amanda    | Ayam Krispi
 Tri amanda    | Ayam Lada Hitam
 Tri amanda    | Tuna Lada Hitam
 Mytea Hugandi | Mie Baso
 Mytea Hugandi | Jus Alpukat
 Mytea Hugandi | Jus Apel
 Mytea Hugandi | Jus Campur
 Mytea Hugandi | Jus Jambu
 Mytea Hugandi | Jus Jeruk
(10 rows)




4. Tampilkan nama mahasiswa beserta rata-rata harga pembelian mahasiswa tersebut

	SELECT nama, AVG(total_harga)
	FROM TRANSAKSI_PEMBELIAN TP, MAHASISWA M, PENGGUNA P
	WHERE TP.npm_mhs = M.npm
	AND M.no_ktp = P.no_ktp
	GROUP BY nama;
        nama        |  npm_mhs   |          avg
--------------------+------------+------------------------
 Hana Tiara         | 1403967000 | 14000.0000000000000000
 M. Kodir           | 1403601147 |     15000.000000000000
 Dilan Dashusa      | 1403723098 | 15000.0000000000000000
 Achmad Terry       | 1402991392 | 10000.0000000000000000
 Farizki Ahmad      | 1402137735 | 14000.0000000000000000
 Rewianza           | 1702625539 | 13000.0000000000000000
 Geofanny Sarah     | 1502381637 | 15000.0000000000000000
 Fahri Setiawan     | 1602503588 |     32000.000000000000
 Christopher Albert | 1403235294 |     20000.000000000000
 Widatie Cahaya     | 1502259686 | 10000.0000000000000000
 Dodo Sardodo       | 1402747490 |     30000.000000000000
 Siti Fadhilia      | 1804088951 |     23000.000000000000
(12 rows)
		Rho nama, average_price (
			(
				(pengguna theta no_ktp = no_ktp mahasiswa) theta npm_mhs = npm MAHASISWA) nama ℑ average total_harga ))

5. Tampilkan nama owner, jumlah counter yang dimiliki nya, dan rata-rata saldo yang
dipunyai counter-counter owner tersebut.

SELECT P.nama, COUNT(no_ktp_owner) as jumlah_counter, AVG(saldo_e_wallet) as rata_rata_saldo
FROM PENGGUNA P, OWNER O, COUNTER C
WHERE C.no_ktp_owner = O.no_ktp
AND O.no_ktp = P.no_ktp
GROUP BY P.nama;

     nama      | jumlah_counter |  rata_rata_saldo
---------------+----------------+--------------------
 Rakana Adian  |              1 | 40000.000000000000
 Ani Hidayat   |              1 | 50000.000000000000
 Budi Santoso  |              3 | 20000.000000000000
 Mytea Hugandi |              2 | 50000.000000000000
 Fikri Munawar |              2 | 45000.000000000000
 Tri amanda    |              2 | 20000.000000000000
(6 rows)


6. Tampilkan total biaya yang harus dikeluarkan untuk membeli semua menu yang ada di
counter Soto

SELECT sum(harga) AS total_biaya
FROM MENU M, COUNTER C 
WHERE M.id_counter = C.id
AND C.nama = 'Soto';





7. Tampilkan semua nama penjaga kantin yang melayani transaksi pembelian pada 
tanggal 11 Maret 2019

SELECT DISTINCT nama --, TP.timestamp
FROM PENGGUNA P, TRANSAKSI_PEMBELIAN TP, PENJAGA_KANTIN PK
WHERE TP.id_penjaga_kantin = PK.no_ktp
AND PK.no_ktp = P.no_ktp
AND TP.timestamp BETWEEN '2019-03-11 00:00:00' AND '2019-03-12 00:00:00';

     nama
--------------
 Aisyahrani
 Mila Karmila
(2 rows)


8. Tampilkan semua informasi transaksi pembelian pada counter soto dan counter
minuman (gabungan)

	SELECT id_transaksi_pembelian, TP.timestamp, id_counter, npm_mhs, id_penjaga_kantin, total_harga
	FROM TRANSAKSI_PEMBELIAN TP, COUNTER C
	WHERE TP.id_counter = C.id
	AND (C.nama = 'Minuman' OR C.nama = 'Soto');



9. Tampilkan nama-nama mahasiswa yang pernah membeli semua menu yang pernah
dibeli mahasiswa bernama ‘Achmad Terry’



10. Tampilkan semua nama mahasiswa beserta nama semua menu yang pernah dibeli
mahasiswa tersebut dari counter bernama ‘Ayam Mas Fu'ad