1. Jalankan perintah EXPLAIN ANALYZE dengan statement berupa keempat query
di atas. Simpan hasil yang ditampilkan pada laporan Anda.

2. Buatlah index berikut:
a. nama_mitra_idx pada tabel MITRA kolom nama_mitra
b. jenis_mitra_idx pada tabel MITRA kolom jenis_mitra
c. nama_kota_idx pada tabel KOTA kolom nama_kota
d. nomor_resi_idx pada tabel TRACKING kolom nomor_resi
Tampilkan index untuk setiap tabel dalam laporan Anda.



3. Jalankan kembali keempat query di atas menggunakan perintah EXPLAIN
ANALYZE. Simpan hasil yang ditampilkan pada laporan Anda.

4. Bandingkan cost time jika tidak menggunakan index dan jika menggunakan
index. Manakah yang lebih baik, menggunakan index atau tidak menggunakan
index? Berikan penjelasan.



============================================================================
1. Buat atribut baru jumlah_shipping bertipe INT ke tabel CUSTOMER. Lalu,
buatlah function untuk mengisi atribut tersebut.
 Atribut ini akan dipakai
untuk menghitung berapa banyak shipping yang dimiliki seorang CUSTOMER.
Setelah itu, jalankan function tersebut dan lihat perubahan yang terjadi
pada tabel CUSTOMER.


ALTER TABLE CUSTOMER ADD COLUMN jumlah_shipping INT DEFAULT 0;

CREATE OR REPLACE FUNCTION 
hitung_jumlah_shipping (customer_id INTEGER)
RETURNS INTEGER AS 
$$
DECLARE
jml_shipping INTEGER;
BEGIN
	SELECT COUNT(S.id_customer) INTO jml_shipping
	FROM SHIPPING S JOIN CUSTOMER C
	ON S.id_customer = C.id_customer
	WHERE C.customer_id = customer_id;

	UPDATE CUSTOMER C
	SET jumlah_shipping = jml_shipping
	WHERE C.customer_id = customer_id;
	RETURN jml_shipping;
END;
$$
LANGUAGE plpgsql;
=======================================

CREATE OR REPLACE FUNCTION
hitung_jumlah_tracking (masukan_id INTEGER)
RETURNS INTEGER AS
$$
DECLARE
jml_tracking INTEGER;
BEGIN
	SELECT COUNT(T.*) INTO jml_tracking
	FROM TRACKING T JOIN KOTA K
	ON K.id_kota = T.id_kota
	WHERE K.id_kota = masukan_id;

	UPDATE KOTA K
	SET jumlah_tracking = jml_tracking
	WHERE K.id_kota = masukan_id;
	RETURN jml_tracking;
END;
$$
LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION
hitung_jumlah_shipping_semua_customer()
RETURNS void AS
$$
DECLARE 
temp_row RECORD;
BEGIN
FOR temp_row IN
SELECT
C.id_customer AS id_customer,
COUNT(C.*) AS jml_shipping
FROM CUSTOMER C
JOIN SHIPPING S
ON C.id_customer = S.id_customer
GROUP BY C.id_customer
LOOP
UPDATE CUSTOMER C
SET jumlah_shipping = temp_row.jml_shipping
WHERE C.id_customer = temp_row.id_customer;
END LOOP;
END;
$$
LANGUAGE plpgsql;










CREATE OR REPLACE FUNCTION
hitung_jumlah_shipping_semua_customer()
RETURNS void AS
$$
DECLARE 
	temp_row RECORD;
BEGIN
	FOR temp_row IN
		SELECT
			C.id_customer AS id_customer,
			COUNT(C.*) AS jml_shipping
		FROM CUSTOMER C
			JOIN SHIPPING S
			ON C.id_customer = S.id_customer
		GROUP BY C.id_customer
	LOOP
		UPDATE CUSTOMER C
		SET jumlah_shipping = temp_row.jml_shipping
		WHERE C.id_customer = temp_row.id_customer;
	END LOOP;
END;
$$
LANGUAGE plpgsql;









CREATE OR REPLACE FUNCTION reset_jumlah_shipping()
RETURNS VOID AS $$
DECLARE
temp_row RECORD;
BEGIN
FOR temp_row IN
SELECT * 
FROM CUSTOMER C
LOOP
UPDATE CUSTOMER C
SET jumlah_shipping = 0;
END LOOP;
END;
$$
LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION update_jumlah_shipping()
RETURNS TRIGGER AS $$
DECLARE
temp_row RECORD;
BEGIN
PERFORM reset_jumlah_shipping();
FOR temp_row IN
SELECT C.id_customer as id_customer,
COUNT(S.*) as jml_shipping
FROM SHIPPING S JOIN CUSTOMER C
ON C.id_customer = S.id_customer
GROUP BY C.id_customer
LOOP
UPDATE CUSTOMER C
SET jumlah_shipping = temp_row.jml_shipping
WHERE C.id_customer = temp_row.id_customer;
END LOOP;
RETURN NULL;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER update_jumlah_shipping_trigger
BEFORE INSERT or UPDATE or DELETE ON SHIPPING 
FOR EACH ROW
EXECUTE PROCEDURE update_jumlah_shipping();






CREATE OR REPLACE FUNCTION
hitung_jumlah_tracking_semua_kota ()
RETURNS void AS
$$
DECLARE
	temp_row RECORD;
BEGIN
	FOR temp_row IN
		SELECT
			K.id_kota AS id_kota,
			COUNT(T.*) AS jml_tracking
		FROM TRACKING T
			JOIN KOTA K ON K.id_kota = T.id_kota
		GROUP BY K.id_kota
	LOOP
		UPDATE KOTA K
			SET jumlah_tracking = temp_row.jml_tracking
			WHERE K.id_kota = temp_row.id_kota;
	END LOOP;
END;
$$
LANGUAGE plpgsql;




2. Buatlah trigger untuk setiap kejadian INSERT, UPDATE, dan DELETE pada
tabel SHIPPING yang akan memperbaharui kolom jumlah_shipping di
tabel CUSTOMER.

Sebagai contoh, jika dilakukan INSERT, jumlah_shipping otomatis
bertambah.

Lakukan uji coba dengan perintah-perintah berikut dan lihat perubahan
dan perbedaan sebelum dan sesudah perintah dijalankan. Misalnya,
tampilkan terlebih dahulu jumlah_shipping awal. Kemudian, lakukan
operasi INSERT. Setelah itu, tampilkan jumlah_shipping setelah operasi
INSERT tersebut. Lakukan pula untuk operasi UPDATE dan DELETE.


CREATE OR REPLACE FUNCTION update_jumlah_shipping()
RETURNS trigger AS
$$
	DECLARE
		temp_row RECORD;
	BEGIN
		FOR temp_row IN
			SELECT S.id_customer,
			C.jumlah_shipping
		FROM SHIPPING S
			JOIN CUSTOMER C ON S.id_customer = C.id_customer
		GROUP BY S.id_customer, C.id_customer --;
		LOOP
			IF (TG_OP = 'INSERT') THEN
			UPDATE CUSTOMER
				SET C.jumlah_shipping = C.jumlah_shipping + 1
			--FROM CUSTOMER C
			WHERE C.id_customer = NEW.id_customer;

			ELSIF (TG_OP = 'DELETE') THEN
			UPDATE CUSTOMER
				SET C.jumlah_shipping = C.jumlah_shipping - 1
			WHERE C.id_customer = OLD.id_customer;
			END IF;
		END LOOP;
	END;
$$
LANGUAGE plpgsql;

















CREATE OR REPLACE FUNCTION update_jumlah_shipping()
RETURNS TRIGGER
AS $$

BEGIN
	PERFORM 






























INSERT INTO SHIPPING
	(nomor_resi, id_kota_asal, id_kota_tujuan,
	id_jenis_barang, nilai_barang, berat,
	id_mitra, id_layanan, id_customer,
	nama_penerima, alamat_penerima, telp_penerima)
VALUES
	('TEST123', 1, 1,
	'B3', 1000, 10,
	'12400', 'L2', '16914',
	'Pak Eko', 'Jagakarsa', '081082083084');

DELETE FROM SHIPPING WHERE id_customer = '16910';

UPDATE SHIPPING
SET id_customer = '16909'
WHERE id_customer = '16914';

3. Buatlah trigger untuk setiap event INSERT pada tabel SHIPPING yang
akan mencegah pemasukan shipping jika identitas pelanggan (yaitu nama,
nomor telepon, alamat) sama dengan identitas penerima dan kota asal
juga sama dengan kota tujuan. Definisi identitas yang sama di sini adalah
sama secara case-insensitive.

Lakukan uji coba dengan perintah-perintah berikut

INSERT INTO SHIPPING
	(nomor_resi, id_kota_asal, id_kota_tujuan,
	id_jenis_barang, nilai_barang, berat,
	id_mitra, id_layanan, id_customer,
	nama_penerima, alamat_penerima, telp_penerima)
VALUES
	('TEST321', 1, 1,
	'B3', 1000, 10,
	'12400', 'L2', '16914',
	'James Gonzales', 'Blitar', '082123456789');






CREATE OR REPLACE FUNCTION insert_check()
RETURNS TRIGGER AS $$
DECLARE
id_kota_sama BOOLEAN;
kota_sama BOOLEAN;
BEGIN
IF(TG_OP = 'INSERT') THEN
SELECT EXISTS(
SELECT id_customer FROM CUSTOMER C
WHERE UPPER(C.nama_customer) = UPPER(NEW.nama_penerima)
AND UPPER(C.alamat_customer) = UPPER(NEW.alamat_penerima)
AND C.id_customer = NEW.id_customer)
INTO id_kota_sama;
kota_sama := (NEW.id_kota_tujuan = NEW.id_kota_asal);
END IF;
IF id_kota_sama OR kota_sama THEN
RAISE EXCEPTION 'Data yang diinsert tidak valid';
END IF;
RETURN NEW;
END;
$$
LANGUAGE plpgsql;


CREATE TRIGGER insert_check_trigger
BEFORE INSERT ON SHIPPING
FOR EACH ROW
EXECUTE PROCEDURE insert_check();