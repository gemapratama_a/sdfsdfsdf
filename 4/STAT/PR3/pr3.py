import math
def fact(n):
	result = 1
	if n < 0:
		return 1
	for i in range(1, n+1):
		result *= i
	return result

def C(n, r):
	atas = fact(n)
	bawah = fact(r) * fact(n-r)
	return atas // bawah

def hyperP(ambilBrp, specialBrp, totalSize, target):
	gaSpecialBrp = totalSize - specialBrp
	gaSpecialKeambilBrp = ambilBrp - target
	atas = C(specialBrp, target) * C(gaSpecialBrp, gaSpecialKeambilBrp)
	bawah = C(totalSize, ambilBrp)

	return atas / bawah


def geoP(p, x):
	q = 1 - p
	return q**(x-1) * p
"""
a = hyperP(20, 4000, 5000, 3)
b = hyperP(20, 4000, 5000, 2)
c = hyperP(20, 4000, 5000, 1)
d = hyperP(20, 4000, 5000, 0)
"""
#print(a+b+c+d)


b = C(4000,2) * C(1000,18) / C(5000,20)
c = C(4000,1) * C(1000,19) / C(5000,20)
d = C(4000,0) * C(1000,20) / C(5000,20)
print(b+c+d)
print(c)
print(d)
print("=============")
e = 0.95**2 * 0.05
f = 0.95**1 * 0.05
g = 0.95**0 * 0.05
print(e+f+g)
print("=============")
#print(hyperP(20, 4000, 5000, 17))
"""
print(C(12,3))

print(geoP(0.05, 5))

print(geoP(0.05, 3) + geoP(0.05, 2) + geoP(0.05, 1))

print(geoP(0.05, 4))

print(hyperP(5, 16, 20, 2))


#1000C3 * 4000C17 / 5000C20
print(C(1000,3) * C(4000,17) / C(5000,20))
"""

#print(C(4,3) * C(16,2) / C(20,5))
"""
print()
a = C(4,0) * C(16,5) / C(20,5)
b = C(4,1) * C(16,4) / C(20,5)
print(1-a-b) 
"""

#=====================================
# NO 3b
"""
a = C(4,3) * C(16,2) / C(20,5)
b = C(4,2) * C(16,3) / C(20,5)
c = C(4,1) * C(16,4) / C(20,5)
d = C(4,0) * C(16,5) / C(20,5)
print(a)
print(b)
print(c)
print(d)
print(a+b+c+d)
"""
#=====================================

#print(C(1000,3) * C(4000,17) / C(5000,20))


#20C3 * p^3 * q^17
#print(C(20, 3) * 0.2**3 * 0.8**17)

#print( 1750**1000 / ((math.e)**1750 * fact(1000)))
"""
sum = 0
for i in range(150, 200 + 1):
	summand = 1750**i / (math.e)**1750 * fact(i)
	sum += summand

print(sum)
"""

#px10 = math.exp(-15) * y**10 / fact(10)
#print(px10)
print("===================")



y = 15
sum = 0
for i in range(11, 16):
	summand = math.exp(-15) * y**i / fact(i) # e^-15 * lambda^i / i!
	sum += summand

print("SUM:" + str(sum))

"""
h = C(15,7) * 0.44962516407952874**7 * (1-0.44962516407952874)**8
print(h)

j = (1-0.44962516407952874)**3
print(j)
"""

print(0.55**100)