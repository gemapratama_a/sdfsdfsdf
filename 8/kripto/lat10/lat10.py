# Gema Pratama Aditya
# 1706040031

from functools import reduce

def prob_birthday_success(r, N=365):
    if r > N: 
        return 1.
    factorial = reduce(lambda x, y: x*y, range(N-r+1, N+1))
    power = N**r
    return (1 - factorial/power)

if __name__ == '__main__':
    i = 1
    HARI = 1000
    KEMUNGKINAN = 0.5
    while prob_birthday_success(i, HARI) <= KEMUNGKINAN:
        i += 1
    
    print(i) # Jawaban: 38