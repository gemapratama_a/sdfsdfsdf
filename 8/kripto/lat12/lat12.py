# Gema Pratama Aditya
# 1706040031

def no1() -> None:
    p, q, g = 1279, 71, 1157

    # 1a
    x = 10       # kunci privat
    y = (g ** x) % p # 316
    k = 4
    HM = 3
    

    print("1a: y =", y)

    # 1b
    r = ((g ** k) % p) % q
    kinverse = 18
    s = (kinverse * (HM + x*r)) % q 
    
    print("1b: r = {}, s = {}".format(r,s))


def no2():
    e, n = 199, 3869 # public
    d = 2935
    M = "DATANG"
    S = 2642

    if hash(M) == (S**e) % n: 
        print("valid")
    else:
        print("invalid")

    print("H(M) = {}, S^e mod n = {}".format(hash(M), (S**e) % n))

def hash(message: str) -> int:
    result = 1
    for char in message:
        print("{} -> {}".format(char, charToInt(char)))
        result *= charToInt(char)

    return result % 3869

def charToInt(c: str) -> int:
    c = c.upper()
    return ord(c) - 64

if __name__ == "__main__":
    no1()
    no2()