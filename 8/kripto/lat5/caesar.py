def decrypt(text, K):
    return encrypt(text, 26 - K)

def encrypt(text, K):
    result = ""

    for i in range(len(text)):
        char = text[i]
        if char == " ":
            result += " "
        elif (char.isupper()):
            result += chr((ord(char) + K - 65) % 26 + 65)
        else:
            result += chr((ord(char) + K - 97) % 26 + 97)
  
    return result
  
if __name__ == '__main__':
    abc = "abcdefghijklmnopqrstuvwxyz"
    ABC = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    plaintext = "VBIAXKMXQM" #input("Plain text: ")
    K = 1 #int(input("K: "))

    print("Plain abc: " + abc)
    print("Plain ABC: " + ABC)

    print("Encrypted abc: " + encrypt(abc, K))
    print("Encrypted ABC: " + encrypt(ABC, K))

    print("Text  : " + plaintext)
    print("Shift : " + str(K))
    print("Cipher: " + encrypt(plaintext, K))

    for k in [8, 19, 23]:
        print(decrypt(plaintext, k))
    #for k in range(0, 27):
    #3    print(k+1, encrypt(plaintext, k))