# Gema Pratama Aditya
# 1706040031
frequency_ciphertext = {
    "A" : 0.1,
    "B" : 0.1,
    "I" : 0.1,
    "K" : 0.1,
    "M" : 0.2,
    "Q" : 0.1,
    "V" : 0.1,
    "X" : 0.2
}

def get_frequency_word(word: str) -> dict:
    length = len(word)
    all_freq = {}
  
    for i in word:
        if i in all_freq:
            all_freq[i] += 1
        else:
            all_freq[i] = 1

    for word in all_freq:
        all_freq[word] = all_freq[word] / length 

    return all_freq
frequency_slide = {
    "G" : 0.1,
    "H" : 0.1,
    "K" : 0.1,
    "O" : 0.3,
    "R" : 0.2,
    "U" : 0.1,
    "Z" : 0.1,
}

frequency_english = {
    "A" : 0.08,
    "B" : 0.015,
    "C" : 0.03,
    "D" : 0.04,
    "E" : 0.13,
    "F" : 0.02,
    "G" : 0.015,
    "H" : 0.06,
    "I" : 0.065,
    "J" : 0.005,
    "K" : 0.005,
    "L" : 0.035,
    "M" : 0.03,
    "N" : 0.07,
    "O" : 0.08,
    "P" : 0.02,
    "Q" : 0.002,
    "R" : 0.065,
    "S" : 0.06,
    "T" : 0.09,
    "U" : 0.03,
    "V" : 0.01,
    "W" : 0.015,
    "X" : 0.005,
    "Y" : 0.02,
    "Z" : 0.002
}

I = [x for x in range(0, 27)]

def number_to_letter(number):
    if number < 0:
        return chr(26 - number + 65)
    else:
        return chr(number + 65)

def letter_to_number(letter):
    return ord(letter) - 65

def phi(i):
    total = 0
    for c in frequency_ciphertext:
        total += frequency_ciphertext[c] * frequency_english[number_to_letter((letter_to_number(c) - i) % 26)]

    return total

if __name__ == '__main__':
    ciphertext = "VBIAXKMXQM"
    sortedCipherText = "".join(sorted(list(ciphertext)))
    print(sortedCipherText)

    ans = []
    for i in range(0, 26):
        ans.append(phi(i))
        print(i, phi(i))

    ans.sort(reverse=True)
    print(ans)