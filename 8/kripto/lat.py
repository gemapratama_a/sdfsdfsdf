import random
def gcd(a, b): 
  
    if (a == 0): 
        return b 
    return gcd(b % a, a) 
  

def phi1(n): 
  
    result = 1
    for i in range(2, n): 
        if gcd(i, n) == 1: 
            result += 1 
    return result 
  


def phi2(n) : 
  
    result = n   # Initialize result as n 

    p = 2
    while p * p <= n : 

        if n % p == 0 : 

            while n % p == 0 : 
                n = n // p 
            result = result * (1.0 - (1.0 / float(p))) 
        p = p + 1
          
          
    if n > 1 : 
        result = result * (1.0 - (1.0 / float(n))) 
   
    return int(result) 

def isPrime(number):
    if number < 2:
        return False
    
    if number % 2 == 0:
        return False
    
    root = int(number ** 0.5)
    for i in range(3, root + 1, 2):
        if number % i == 0:
            return False
    return True

def mr(n, k, q, a):
    #a = random.randint(2, n - 2)
    if a**q % n == 1:
        print("{}^{} % {} == 1".format(a, q, n))
        return "INCONCLUSIVE"
        #return
    for j in range(0, k):
        if a**((2**j)*q) % n == n - 1:
            print("{}^(2^{}*{}) mod {} = {} - 1".format(a, j, q, n, n))
            return "INCONCLUSIVE"

    print("[a = {}]".format(a))
    return "COMPOSITE"
n = 61
k = 3
q = 7
for a in range(2, n - 1):
    print(a, mr(n,k,q, a))