# Gema Pratama Aditya
# 1706040031
from textwrap import wrap

def hex2bin(s):
    mp = {'0' : "0000", 
          '1' : "0001",
          '2' : "0010", 
          '3' : "0011",
          '4' : "0100",
          '5' : "0101", 
          '6' : "0110",
          '7' : "0111", 
          '8' : "1000",
          '9' : "1001", 
          'A' : "1010",
          'B' : "1011", 
          'C' : "1100",
          'D' : "1101", 
          'E' : "1110",
          'F' : "1111" }
    bin = ""
    for i in range(len(s)):
        bin = bin + mp[s[i]]
    return bin

def bin2hex(s):
    mp = {
        "0000" : '0', 
        "0001" : '1',
        "0010" : '2', 
        "0011" : '3',
        "0100" : '4',
        "0101" : '5', 
        "0110" : '6',
        "0111" : '7', 
        "1000" : '8',
        "1001" : '9', 
        "1010" : 'A',
        "1011" : 'B', 
        "1100" : 'C',
        "1101" : 'D', 
        "1110" : 'E',
        "1111" : 'F' 
    }
    hex = ""
    for i in range(0, len(s), 4):
        ch = ""
        ch = ch + s[i]
        ch = ch + s[i + 1] 
        ch = ch + s[i + 2] 
        ch = ch + s[i + 3] 
        hex = hex + mp[ch]
          
    return hex


IP = [
    58, 50, 42, 34, 26, 18, 10, 2,
    60, 52, 44, 36, 28, 20, 12, 4,
    62, 54, 46, 38, 30, 22, 14, 6,
    64, 56, 48, 40, 32, 24, 16, 8,
    57, 49, 41, 33, 25, 17, 9, 1,
    59, 51, 43, 35, 27, 19, 11, 3,
    61, 53, 45, 37, 29, 21, 13, 5,
    63, 55, 47, 39, 31, 23, 15, 7
]

IPinverse = [
        40, 8, 48, 16, 56, 24, 64, 32,
        39, 7, 47, 15, 55, 23, 63, 31,
        38, 6, 46, 14, 54, 22, 62, 30,
        37, 5, 45, 13, 53, 21, 61, 29,
        36, 4, 44, 12, 52, 20, 60, 28,
        35, 3, 43, 11, 51, 19, 59, 27,
        34, 2, 42, 10, 50, 18, 58, 26,
        33, 1, 41, 9, 49, 17, 57, 25
    ]

PC1 = [57, 49, 41, 33, 25, 17, 9,
        1, 58, 50, 42, 34, 26, 18,
        10, 2, 59, 51, 43, 35, 27,
        19, 11, 3, 60, 52, 44, 36,
        63, 55, 47, 39, 31, 23, 15,
        7, 62, 54, 46, 38, 30, 22,
        14, 6, 61, 53, 45, 37, 29,
        21, 13, 5, 28, 20, 12, 4]

#Permut applied on shifted key to get Ki+1
PC2 = [ 14, 17, 11, 24, 1, 5, 
        3, 28, 15, 6, 21, 10, 
        23, 19, 12, 4, 26, 8, 
        16, 7, 27, 20, 13, 2, 
        41, 52, 31, 37, 47, 55, 
        30, 40, 51, 45, 33, 48, 
        44, 49, 39, 56, 34, 53, 
        46, 42, 50, 36, 29, 32
        ]

E = [   
    32, 1, 2,   3, 4, 5,
    4, 5, 6,   7, 8, 9,
    8, 9, 10, 11, 12,13,
    12, 13, 14, 15, 16, 17,
    16, 17, 18, 19, 20, 21,
    20, 21, 22, 23, 24, 25,
    24, 25, 26, 27, 28, 29,
    28, 29, 30, 31, 32, 1
    ]
def permute(bitstring: str, table: list) -> str:
    result = ""
    for index in table:
        result += bitstring[index - 1]
    return result

def left_shift(bitstring: str, n: int) -> str: #Shift a list of the given value
        return bitstring[n:] + bitstring[:n]

def split(bitstring: str, n: int):#Split a list into sublists of size "n"
    return [bitstring[k:k+n] for k in range(0, len(bitstring), n)]

def XOR(a: str, b: str) -> str:
    y = int(a, 2) ^ int(b,2)
    return bin(y)[2:].zfill(len(a))

def substitute(d_e):#Substitute bytes using SBOX
    subblocks = nsplit(d_e, 6)#Split bit array into sublist of 6 bits
    result = list()
    for i in range(len(subblocks)): #For all the sublists
        block = subblocks[i]
        row = int(str(block[0])+str(block[5]),2)#Get the row with the first and last bit
        column = int(''.join([str(x) for x in block[1:][:-1]]),2) #Column is the 2,3,4,5th bits
        val = S_BOX[i][row][column] #Take the value in the SBOX appropriated for the round (i)
        bin = binvalue(val, 4)#Convert the value to binary
        result += [int(x) for x in bin]#And append it to the resulting list
    return result

def nsplit(s, n):#Split a list into sublists of size "n"
    return wrap(s, n)

def binvalue(val, bitsize): #Return the binary value as a string of the given size 
    binval = bin(val)[2:] if isinstance(val, int) else bin(ord(val))[2:]
    if len(binval) > bitsize:
        raise "binary value larger than the expected size"
    while len(binval) < bitsize:
        binval = "0"+binval #Add as many 0 as needed to get the wanted size
    return binval

S_BOX = [
         
[[14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7],
 [0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8],
 [4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0],
 [15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13],
],

[[15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10],
 [3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5],
 [0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15],
 [13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9],
],

[[10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8],
 [13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1],
 [13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7],
 [1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12],
],

[[7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15],
 [13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9],
 [10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4],
 [3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14],
],  

[[2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9],
 [14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6],
 [4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14],
 [11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3],
], 

[[12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11],
 [10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8],
 [9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6],
 [4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13],
], 

[[4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1],
 [13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6],
 [1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2],
 [6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12],
],
   
[[13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7],
 [1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2],
 [7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8],
 [2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11],
]
]

P = [16, 7, 20, 21, 29, 12, 28, 17,
     1, 15, 23, 26, 5, 18, 31, 10,
     2, 8, 24, 14, 32, 27, 3, 9,
     19, 13, 30, 6, 22, 11, 4, 25]


if __name__ == '__main__':

    # No 1
    print("=================NO1================")
    pesan = "AE018D295C63"
    padded = "AE018D295C630202"
    padded2bin = hex2bin(padded)
    print("Padded:", padded)
    print("Padded to bin:", padded2bin)
    #print("Length padded to bin:", len(padded2bin))
    permutated = permute(padded2bin, IP)
    print("Permutated:", wrap(permutated, 4))
    print("Permutated bin:", bin2hex(permutated))

    # No 2
    print("\n\n=================NO2================")
    #hasilno1 = "3010152E05291DE1"
    no2 = "13190FE4050E19FC"
    #testno2 = bin2hex(inversePermutation(hex2bin(hasilno1)))

    ansno2 = bin2hex(permute(hex2bin(no2), IPinverse))
    #print("TESTno2", testno2)
    print("ansno2", ansno2)


    # No 4
    print("\n\n=================NO4================")
    K = "1234567890ABCDEF"
    K_bin = hex2bin(K)
    print(wrap(K_bin, 4))
    K_PC1 = permute(K_bin, PC1)

    splitted = split(K_PC1, 28)
    C0, D0 = splitted[0], splitted[1]

    C1 = left_shift(C0, 1)
    D1 = left_shift(D0, 1)
    print(wrap(K_PC1, 4))

    print("C0: ", wrap(C0, 4))
    print("C1: ", wrap(C1, 4))
    print("\n\nD0: ", wrap(D0, 4))
    print("D1: ", wrap(D1, 4))

    X = C1 + D1
    K1 = permute(X, PC2)
    print("K1:", wrap(K1, 4))
    print("k1 hex: ", bin2hex(K1))

    C2 = left_shift(C1, 1)
    D2 = left_shift(D1, 1)
    print("C2: ",wrap(C2, 4))
    print("D2: ",wrap(D2, 4))
    Y = C2 + D2
    K2 = permute(Y, PC2)
    print("K2:", wrap(K2, 4))
    print("k2 hex: ", bin2hex(K2))


    # No 5
    print("\n\n=================NO5================")
    plaintext = "BA5EBA11F005BA11"

    Key       = "AB89135E267CF40D"

    plaintext_bin = hex2bin(plaintext)
    print("Plaintext bin:", wrap(plaintext_bin, 4))

    plaintext_IP = permute(plaintext_bin, IP)
    #print("Plaintext abis IP: ", wrap(plaintext_IP, 4))
    print("Plaintext abis IP: ", bin2hex(plaintext_IP))

    Key_bin = hex2bin(Key)
    LR = split(plaintext_IP, 32)
    L0 = LR[0]
    R0 = LR[1]
    print("L0:", wrap(L0 ,4), "=", bin2hex(L0))
    print("R0:", wrap(R0, 4), "=", bin2hex(R0))
    E_R0 = permute(R0, E)
    print("E(R0): ", wrap(E_R0, 4))

    K_plus = permute(Key_bin, PC1)
    print("K binary:", wrap(Key_bin, 4))
    print("K+ :", wrap(K_plus, 4))

    C0D0 = split(K_plus, 28)
    C0 = C0D0[0]
    D0 = C0D0[1]
    print("C0:", C0, "\nD0:", D0)
    C1 = left_shift(C0, 1)
    D1 = left_shift(D0, 1)
    print("C1:", wrap(C1, 4), "\nD1:", wrap(D1, 4))

    C1D1 = C1 + D1

    K1 = permute(C1D1, PC2)
    print("K1:", wrap(K1, 4), "=", bin2hex(K1))

    ER0xorK1 = XOR(E_R0, K1)
    print("R0 XOR K1:", wrap(ER0xorK1, 4))

    S_output = substitute(ER0xorK1)
    S_output = "".join([str(x) for x in S_output])
    print("S output: ", wrap(S_output, 4))

    fR0K1 = permute(S_output, P)
    print("f(R0, K1):", wrap(fR0K1, 4))

    L0XORfR0K1 = XOR(L0, fR0K1)
    print("L0 XOR f(R0, K1) =", wrap(L0XORfR0K1, 4), "=", bin2hex(L0XORfR0K1))