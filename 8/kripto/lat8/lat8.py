from textwrap import wrap

# Gema Pratama Aditya
# 1706040031

# NO 1
initial_input = "ABCDEF1234567890B01DFACECA55E77E"
after_sbox    = "62BDDFC918B1BC60E7A42D8B74FC94F3"

"""
62 18 E7 74
BD B1 A4 FC
DF BC 2D 94
C9 60 8B F3
"""
after_shift   = "62B12DF318A494C9E7FCDF6074BDBC8B"




# https://stackoverflow.com/questions/66115739/aes-mixcolumns-with-python
def mixColumns(a, b, c, d):
    printHex(gmul(a, 2) ^ gmul(b, 3) ^ gmul(c, 1) ^ gmul(d, 1))
    printHex(gmul(a, 1) ^ gmul(b, 2) ^ gmul(c, 3) ^ gmul(d, 1))
    printHex(gmul(a, 1) ^ gmul(b, 1) ^ gmul(c, 2) ^ gmul(d, 3))
    printHex(gmul(a, 3) ^ gmul(b, 1) ^ gmul(c, 1) ^ gmul(d, 2))
    print()

def gmul(a, b):
    if b == 1:
        return a
    tmp = (a << 1) & 0xff
    if b == 2:
        return tmp if a < 128 else tmp ^ 0x1b
    if b == 3:
        return gmul(a, 2) ^ a




def printHex(val):
    print('{:02x}'.format(val), end=' ')

# CONTOH DI SLIDE
#mixColumns(0x87, 0x6e, 0x46, 0xa6) 
#mixColumns(0xf2, 0x4c, 0xe7, 0x8c) 
#mixColumns(0x4d, 0x90, 0x4a, 0xd8) 
#mixColumns(0x97, 0xec, 0xc3, 0x95) 

# SOAL
mixColumns(0x62, 0xb1, 0x2d, 0xf3) # column 1
mixColumns(0x18, 0xa4, 0x94, 0xc9) # column 2
mixColumns(0xe7, 0xfc, 0xdf, 0x60) # column 3
mixColumns(0x74, 0xbd, 0xbc, 0x8b) # column 4


# NO 2
kunci_utama = "B01DFACECA55E77EABCDEF1234567890"
def hex2bin(s):
    mp = {'0' : "0000", 
          '1' : "0001",
          '2' : "0010", 
          '3' : "0011",
          '4' : "0100",
          '5' : "0101", 
          '6' : "0110",
          '7' : "0111", 
          '8' : "1000",
          '9' : "1001", 
          'A' : "1010",
          'B' : "1011", 
          'C' : "1100",
          'D' : "1101", 
          'E' : "1110",
          'F' : "1111" }
    bin = ""
    for i in range(len(s)):
        bin = bin + mp[s[i]]
    return bin

def bin2hex(s):
    mp = {
        "0000" : '0', 
        "0001" : '1',
        "0010" : '2', 
        "0011" : '3',
        "0100" : '4',
        "0101" : '5', 
        "0110" : '6',
        "0111" : '7', 
        "1000" : '8',
        "1001" : '9', 
        "1010" : 'A',
        "1011" : 'B', 
        "1100" : 'C',
        "1101" : 'D', 
        "1110" : 'E',
        "1111" : 'F' 
    }
    hex = ""
    for i in range(0, len(s), 4):
        ch = ""
        ch = ch + s[i]
        ch = ch + s[i + 1] 
        ch = ch + s[i + 2] 
        ch = ch + s[i + 3] 
        hex = hex + mp[ch]
          
    return hex

def XOR(a: str, b: str) -> str:
    y = int(a, 2) ^ int(b,2)
    return bin(y)[2:].zfill(len(a))

w_accent = "B0BC6018"
w0 = "B01DFACE"
w1 = "CA55E77E"
w2 = "ABCDEF12"
w3 = "34567890"

w0_bin = hex2bin(w0)
w1_bin = hex2bin(w1)
w2_bin = hex2bin(w2)
w3_bin = hex2bin(w3)
w_accent_bin = hex2bin(w_accent)

w4 = XOR(w0_bin, w_accent_bin)
print("W4 = {} = {}".format(wrap(w4, 4), bin2hex(w4)))

w5 = XOR(w4, w1_bin)
print("W5 = {} = {}".format(wrap(w5, 4), bin2hex(w5)))

w6 = XOR(w5, w2_bin)
print("W6 = {} = {}".format(wrap(w6, 4), bin2hex(w6)))

w7 = XOR(w6, w3_bin)
print("W7 = {} = {}".format(wrap(w7, 4), bin2hex(w7)))

answer = w4+w5+w6+w7
print("jawaban akhir:", bin2hex(answer))