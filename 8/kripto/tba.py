L1 = []
L2 = []
L3 = []
L4 = []
L5 = [] 
L6 = []
L7 = []
L8 = []
L9 = []
kleenestarAB = []
kleenestar01 = []
#========================================================
# GENERATE KLEENE STAR AB & 01
#========================================================
def generateKleeneStar01(s, digitsLeft):
    if digitsLeft == 0:
        kleenestar01.append(s)
        kleenestarAB.append(s)
    else:
        generateKleeneStar01(s + '0', digitsLeft - 1)
        generateKleeneStar01(s + '1', digitsLeft - 1)

for i in range (0, 5):
    generateKleeneStar01("", i)

for i in range (0, len(kleenestarAB)):
    kleenestarAB[i] = kleenestarAB[i].replace('0','a')
    kleenestarAB[i] = kleenestarAB[i].replace('1','b')
    

for w in kleenestarAB:
    if w == w[::-1]:
        print(w)

