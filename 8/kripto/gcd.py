def gcdExtended(a, b):  
    # Base Case  
    if a == 0 :   
        return b,0,1
             
    gcd,x1,y1 = gcdExtended(b%a, a)  
     
    # Update x and y using results of recursive  
    # call  
    x = y1 - (b//a) * x1  
    y = x1  
     
    return gcd,x,y 

def inverseMod(a, m):
    for x in range(1, m):
        if (((a%m) * (x%m)) % m == 1):
            return x
    return -1
 

m = 1759
b = 550


def EXTENDED_EUCLID(m, b):
    A1, A2, A3 = 1, 0, m
    B1, B2, B3 = 0, 1, b

    if B3 == 0:
        A3 = gcdExtended(m, b)
        return A3 # no inverse
    if B3 == 1:
        B3 = gcdExtended(m, b)
        return B3

    Q = A3 // B3
    T1, T2, T3 = A1 - Q*B1, A2 - Q*B2, A3 - Q*B3 
    A2, A2, A3 = B1, B2, B3
    B1, B2, B3 = T1, T2, T3


print(EXTENDED_EUCLID(1759, 550))
print(inverseMod(550, 1759))