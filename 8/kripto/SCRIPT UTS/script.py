import math 
def gcd_euclidean(a: int, b: int) -> int:
    if a == 0:
        #print("GCD: ", b)
        return b
    
    #print("{} = ({} x {}) + {}".format(b, b // a, a, b % a))
    return gcd_euclidean(b % a, a)


# Euler totient funciton 
def phi(n: int) -> int:
    result = 1
    for i in range(2, n):
        if gcd_euclidean(i, n) == 1:
            result += 1
    return result

def get_prime_factors(number):
    prime_factors = []

    while number % 2 == 0:
        prime_factors.append(2)
        number = number / 2

    for i in range(3, int(math.sqrt(number)) + 1, 2):
        while number % i == 0:
            prime_factors.append(int(i))
            number = number / i

    if number > 2:
        prime_factors.append(int(number))

    return prime_factors
"""
Untuk n=641, hitung k dan q sehingga n-1=2
k q. Jelaskan tahapan yang dilakukan.
Jawab:
"""
def hitungKQsehingga(n: int):
    primeFactors = get_prime_factors(n - 1)
    k = primeFactors.count(2)
    q = 1
    for prime in primeFactors:
        if prime != 2:
            q *= prime 
    print("k = {}, q = {}".format(k, q))
    return k, q
    #print(primeFactors)

class Point(object):
    # Ep(a, b), bisa ganti2
    #a = 1
    #b = 1
    #prime = 19
    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y

    def __str__(self):
        return "Point({}, {})".format(self.x, self.y)
    @staticmethod
    def add(P, Q, p: int):
        n = 0
        if P.x == Q.x and P.y == Q.y:
            print("P == Q")
            print("3 * {}^2 + {} / (2 * {}) mod {}".format(P.x, a, P.y, p))
            n = modFraction(3*P.x*P.x + a, 2*P.y, p)
            print("lambda =", n)
        else:
            print("P != Q")
            print("({} - {}) / ({} - {}) mod {}".format(Q.y, P.y, Q.x, P.x, p))
            n = modFraction(Q.y - P.y, Q.x - P.x, p)
            print("lambda =", n)
        
        xR = (n**2 - P.x - Q.x) % p  
        print("xR = ({} - {} - {}) mod {} = {}".format(n*n, P.x, Q.x, p, xR))
        yR = (n * (P.x - xR) - P.y) % p
        print("yR = ({}({} - {}) - {}) mod {} = {}".format(n, P.x, xR, P.y, p, yR))

        print("P + Q = ({}, {})".format(xR, yR))
        return Point(xR, yR)
    
    def negative(self):
        return Point(self.x, -self.y)

    """
    @staticmethod
    def scalarMult(P, num: int):
        if num == 0:
            return 0
        elif num == 1:
            return P 
        elif num % 2 == 1:
            return Point.add(P, Point.scalarMult(P, num - 1), Point.prime)
        else:
            return Point.scalarMult(Point.add(P, P, Point.prime), num // 2)
    """
def modFraction(a, b, mod):
    while a % b != 0:
        a += mod

    return int(a / b) % mod

def decrypt_caesar(text: str, K: int) -> str:
    return encrypt_caesar(text, 26 - K)

def encrypt_caesar(text: str, K: int) -> str:
    result = ""
    for i in range(len(text)):
        char = text[i]
        if char == " ":
            result += " "
        elif char.isupper():
            result += chr((ord(char) + K - 65) % 26 + 65)
        else:
            result += chr((ord(char) + K - 97) % 26 + 97)
    return result









def generateKey(string: str, key: str) -> str:
    key = list(key)
    if len(string) == len(key):
        return(key)
    else:
        for i in range(len(string) - len(key)):
            key.append(key[i % len(key)])
    return "".join(key)

def encrypt_vigenere(string: str, key: str) -> str:
    cipher_text = []
    for i in range(len(string)):
        x = (ord(string[i]) + ord(key[i])) % 26
        x += ord('A')
        cipher_text.append(chr(x))
    return "".join(cipher_text)

def decrypt_vigenere(cipher_text: str, key: str) -> str:
    orig_text = []
    for i in range(len(cipher_text)):
        x = (ord(cipher_text[i]) - ord(key[i]) + 26) % 26
        x += ord('A')
        orig_text.append(chr(x))
    return "".join(orig_text)







def get_frequency_word(word: str) -> dict:
    word = "".join(sorted(list(word)))
    length = len(word)
    all_freq = {}
    for i in word:
        if i in all_freq:
            all_freq[i] += 1
        else:
            all_freq[i] = 1
    for word in all_freq:
        all_freq[word] = all_freq[word] / length 
    return all_freq



# MILLER RABIN PRIMALITY TEST
def mr(n, k, q, a):
    #a = random.randint(2, n - 2)
    if a**q % n == 1:
        print("{}^{} % {} == 1".format(a, q, n))
        return "INCONCLUSIVE"
        #return
    for j in range(0, k):
        if a**((2**j)*q) % n == n - 1:
            print("{}^(2^{}*{}) mod {} = {} - 1".format(a, j, q, n, n))
            return "INCONCLUSIVE"

    #print("[a = {}]".format(a))
    return "COMPOSITE"

def XOR(a: str, b: str) -> str:
    y = int(a, 2) ^ int(b,2)
    return bin(y)[2:].zfill(len(a))

if __name__ == '__main__':
    print(get_frequency_word("VBIAXKMXQM"))
    #gcd_euclidean(2240, 386)
    #print(phi(123))
    #print(hitungKQsehingga(641))
    
    # Buat itung tambah 2 point P + Q
    a = 1
    b = 1 
    prime = 13
    P = Point(10, 6)
    Q = Point(10, 6)
    R = Point.add(P, Q, prime)
    print(R)
    
    n = 62
    k, q = hitungKQsehingga(n)
    
    #mr(61, k, q, a)

    for a in range(2, n - 1):
        print(a, mr(n,k,q, a))

    for i in range(0, 27):
        print(encrypt_caesar("ATTACK", i))
    #print(decrypt_caesar("COVKWKDWOXQOBTKUKX", 10))
    
    print(encrypt_vigenere("LIFEISLIKERIDINGABICYLE", generateKey("LIFEISLIKERIDINGABICYLE", "INDON")))
    print("TVISVAYLYRZVGWAONEWPGYH")
    