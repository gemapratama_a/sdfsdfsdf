
from typing import Type  

# Gema Pratama Aditya - 1706040031

# y^2 mod p
def kiri(y, p):
    return (y*y) % p

# (x^3 + ax + b) mod p
def kanan(x, a, b, p):
    return (x**3 + a*x + b) % p

class Point(object):
    # Ep(a, b), bisa ganti2
    #a = 1
    #b = 1
    #prime = 19
    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y

    @staticmethod
    def add(P, Q, p: int):
        n = 0
        if P.x == Q.x and P.y == Q.y:
            print("P == Q")
            print("3 * {}^2 + {} / (2 * {}) mod {}".format(P.x, a, P.y, p))
            n = modFraction(3*P.x*P.x + a, 2*P.y, p)
            print("lambda =", n)
        else:
            print("P != Q")
            print("({} - {}) / ({} - {}) mod {}".format(Q.y, P.y, Q.x, P.x, p))
            n = modFraction(Q.y - P.y, Q.x - P.x, p)
            print("lambda =", n)
        
        xR = (n**2 - P.x - Q.x) % p  
        print("xR = ({} - {} - {}) mod {} = {}".format(n*n, P.x, Q.x, p, xR))
        yR = (n * (P.x - xR) - P.y) % p
        print("yR = ({}({} - {}) - {}) mod {} = {}".format(n, P.x, xR, P.y, p, yR))

        print("P + Q = ({}, {})".format(xR, yR))
        return Point(xR, yR)
    
    def negative(self):
        return Point(self.x, -self.y)

    """
    @staticmethod
    def scalarMult(P, num: int):
        if num == 0:
            return 0
        elif num == 1:
            return P 
        elif num % 2 == 1:
            return Point.add(P, Point.scalarMult(P, num - 1), Point.prime)
        else:
            return Point.scalarMult(Point.add(P, P, Point.prime), num // 2)
    """
def modFraction(a, b, mod):
    #if b == 0:
    #    return 0
    
    while a % b != 0:
        a += mod

    return int(a / b) % mod


if __name__ == '__main__':
    """
    P = Point(13, 8)
    Q = Point(7, 16)
    prime = 19
    R = Point.add(P, Q, prime)
    
    """
    # KONSTANTA
    #a = int(input("a = "))
    #b = int(input("b = "))
    #prime = int(input("prime = "))

    a = 0
    b = -4 
    prime = 11
    #Pxy = [int(x) for x in input("P = ").split()]
    #Qxy = [int(x) for x in input("Q = ").split()]
    #P = Point(Pxy[0], Pxy[1])
    #Q = Point(Qxy[0], Qxy[1])
    
    #R = Point.add(P, Q, prime)
    
    

    G = Point(4, 7)

    G2 = Point.add(G, G, prime)

    print("===============G3=====================")
    G3 = Point.add(G2, G, prime)

    print("===============G4=====================")
    G4 = Point.add(G2, G2, prime)

    print("===============G5=====================")
    G5 = Point.add(G2, G3, prime)

    print("===============G6=====================")
    G6 = Point.add(G3, G3, prime)

    print("===============G7=====================")
    G7 = Point.add(G4, G3, prime)

    print("===============G8=====================")
    G8 = Point.add(G4, G4, prime)

    print("===============G9=====================")
    G9 = Point.add(G5, G4, prime)

    print("===============G10=====================") 
    G10 = Point.add(G5, G5, prime)

    print("===============G11=====================")
    G11 = Point.add(G6, G5, prime)

    #print("===============G12=====================")
    #G12 = Point.add(G10, G2, prime)

    print("===============G13=====================")
    G13 = Point.add(G7, G6, prime)
    
    print("===============G11=====================")
    G14 = Point.add(G7, G7, prime)
    
    #print("===============G11=====================")
    #G15 = Point.add(G7, G8, prime)




    #print(G2.x, ",", G2.y)
    """
    while True:
        ab = input().split()
        p = 23
        a = int(ab[0])
        b = int(ab[1])
        print("KIRI: ", kiri(b, p), "KANAN: " , kanan(a, 1, 1, p))
    """


