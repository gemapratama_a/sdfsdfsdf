from pytube import YouTube
 
with open('qt.txt', 'r') as file:
	#lines = file.readlines()

	full = file.readlines()
	f = open("updated.txt", "w")
	for info in full:
		video_url = info.split(' ')[0]
		timestamps = info.split(' ')[1:]
		for i in range(len(timestamps)):
			timestamps[i] = timestamps[i].replace("\n", "")

		yt = YouTube(video_url)

		yt.streams.get_highest_resolution().download()
		info = "{} - {} - {}".format(yt.video_id, yt.title, timestamps)
		
		f.write(info + "\n")
		print(info)
	f.close()
		