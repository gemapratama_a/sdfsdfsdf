#Safa Fathya Azzizah - 1806147161 - StatProb C
#Data: Height

import pandas as pd, matplotlib.pyplot as plt, statistics as st, numpy as np

data = pd.read_csv('500_Person_Gender_Height_Weight_Index.csv')
height = data['Height']

mean = st.mean(height)
print("Sample mean: " + str(mean))

median = st.median(height)
print("Sample median: " +  str(median))

modus = st.mode(height)
print("Sample mode: " +  str(modus))

jangkauan = (max(height) - min(height)) + 1
print("Range: " + str(jangkauan))

variance = st.variance(height)
print("Sample variance: " +  str(variance))

deviation = st.stdev(height)
print("Sample standard deviation: " +  str(deviation))

percentile15 = np.percentile(height, 15)
print("Percentile-15: " +  str(percentile15))

percentile90 = np.percentile(height, 90)
print("Percentile-90: " +  str(percentile90))

Q1 = np.percentile(height, 25)
Q3 = np.percentile(height, 75)
IQR = Q3 - Q1
print("Interquartile Range (IQR): " + str(IQR))

RLB = Q1 - 1.5*IQR
print("​Reasonable Lower Boundary (RLB): " + str(RLB))

RUB = Q3 + 1.5*IQR
print("​Reasonable Upper Boundary (RUB): " + str(RUB))

boxplot = data.boxplot(column=['Height'], showfliers=True)
plt.show()

histogram = data['Height'].hist(bins=10, color='pink', edgecolor='black')
histogram.set_title("Height Histogram")
histogram.set_xlabel("Height")
histogram.set_ylabel("Frequency")
plt.show()

