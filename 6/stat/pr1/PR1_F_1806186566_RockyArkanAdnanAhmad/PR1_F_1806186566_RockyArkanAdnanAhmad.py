import statistics
import pandas
import csv
import matplotlib.pyplot as plt

def main():
    filename = input("Filename: ")
    csvreader = None
    try:
        with open(filename) as csvfile:
            csvreader = csv.reader(csvfile)
            #NPM = 1806186566 -> genap -> weight
            lst_weight = []
            counter = 0
            for row in csvreader:
                #col1 = gender, col2 = height, col3 = weight, col4 = ibm
                if counter > 0: #line pertama adalah string "weight", jadi harus diskip
                    lst_weight.append(int(row[2]))
                counter += 1
    except:
        print("File doesn't exist or have different format from the original data.")
        return
        
    lst_weight.sort() #Sort list untuk menghitung range (karena method range tidak ada di module statistic)

    #No.1
    mean = statistics.mean(lst_weight) #Menghitung sample mean dari list weight
    median = statistics.median(lst_weight) #Menghitung sample median dari list weight
    #mode = statistics.mode(lst_weight) #Menghitung sample mode dari list weight
    print("Mean = {} \nMedian = {} \n".format(mean, median))#, mode))

    #No.2
    rng = lst_weight[-1] - lst_weight[0] #Menghitung range dari list weight
    variance = statistics.variance(lst_weight) #Menghitung sample variance dari list weight
    std_variation = statistics.stdev(lst_weight) #Menghitung sample standard deviation dari list weight
    print("Range = {} \nVariance = {} \nStandard Variation = {}".format(rng, variance, std_variation))
    
    #No.4
    quantile  = statistics.quantiles(lst_weight, n=100) #list percentile dari 1 - 100
    percentile_90 = quantile[89]
    percentile_15 = quantile[14]
    print("P-90 = {} \nP-15 = {}".format(percentile_90, percentile_15))

    #No.5
    iqr = quantile[74] - quantile[24]  #iqr = p75 - p25
    rub = quantile[74] + iqr*1.5 #rub = p75 + iqr*1.5
    rlb = quantile[24] - iqr*1.5 #rlb = p25 - iqr*1.5
    print("IQR = {} \nRUB = {} \nRLB = {}".format(iqr, rub, rlb))

    #No.6
    outlier_lower = []
    outlier_upper = []

    for data in lst_weight:
        if data < rlb:
            outlier_lower.append(data)
        elif data > rub:
            outlier_upper.append(data)

    print("Outlier lower: {}".format(outlier_lower))
    print("Outlier upper: {}".format(outlier_upper))

    #No.7
    df = pandas.DataFrame(lst_weight, columns=[''])
    box_graph = df.boxplot(vert=False)
    box_graph.set_xlabel("Weight")

    plt.title("Weight Box Graph")


    #No.8
    hist_graph = df.plot.hist(bins=11, title='Weight Histogram Graph')
    hist_graph.set_xlabel("Weight")
    hist_graph.set_ylabel("Frequency")

    #show graph
    plt.show()


if __name__ == "__main__":
    main()