import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('gender_height_weight.csv', delimiter =',')

def hitung_mean():
    mean = df['Weight'].mean()
    print('Sample mean : ' + str(mean))
    
def hitung_median():
    median = df['Weight'].median()
    print('Sample median : ' + str(median)) 

def hitung_mode():
    modus = df['Weight'].mode()
    modus_print = ''

    for i in range(len(modus)):
        modus_print += str(modus[i]) + ' '
    print('Sample mode : ' + modus_print)

def hitung_range():
    range = (df['Weight'].max() - df['Weight'].min()) +1
    print('Range : ' + str(range))

def hitung_sample_variance(): 
    varr = df['Weight'].var()
    print('Sample Variance : ' + str(varr))

def hitung_standard_deviation():
    std_dev = df['Weight'].std()
    print('Sample Standard Deviation : ' + str(std_dev))

def hitung_percentile_15():
    perc_15 = df['Weight'].quantile(0.15)
    print('Percentile 15 : ' + str(perc_15))

def hitung_percentile_90():
    perc_90 = df['Weight'].quantile(0.9)
    print('Percentile 90 : ' + str(perc_90))

def hitung_IQR():
    q3 = df['Weight'].quantile(0.75)
    q1 = df['Weight'].quantile(0.25)
    print('IQR : ' + str(q3-q1))
    return q3-q1

def hitung_RLB(iqr):
    q1 = df['Weight'].quantile(0.25)
    print('RLB : ' + str(q1-(1.5 * iqr)))

def hitung_RUB(iqr):
    q3 = df['Weight'].quantile(0.75)
    print('RUB : ' + str(q3+(1.5 * iqr)))

def hitung_boxplot():
    boxplot = df.boxplot(column=['Weight'], showfliers=True)
    plt.show()
    
def hitung_histogram():
    histogram = df['Weight'].hist(bins=[50,60,70,80,90,100,110,120,130,140,150,160], edgecolor='black')
    histogram.set_title("Weight Histogram")
    histogram.set_xlabel("Weight Value")
    histogram.set_ylabel("Weight Frequency")
    plt.show()
    
hitung_mean()
hitung_median()
hitung_mode()
hitung_range()
hitung_sample_variance()
hitung_standard_deviation()
hitung_percentile_15()
hitung_percentile_90()
iqr = hitung_IQR()
hitung_RLB(iqr)
hitung_RUB(iqr)
hitung_boxplot()
hitung_histogram()








