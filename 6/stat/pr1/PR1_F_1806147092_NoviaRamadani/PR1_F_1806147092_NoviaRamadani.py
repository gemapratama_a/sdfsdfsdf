import statistics as st
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#membaca file
df = pd.read_csv("500_Person_Gender_Height_Weight_Index.csv")

#menentukan dataframe
dataset = df['Weight']

#1 menghitung mean, median dan mode
#source http://www.boxer.or.id/statistik-deskriptif-dasar-dengan-python/
mean = dataset.mean()
median = dataset.median()
mode = dataset.mode()

print('Sample mean : ', mean, '\nSample median : ', median,'\nSample Mode :',mode[0])

#2 menghitung range, sample variance dan sample standard deviation
#source https://docs.python.org/3/library/statistics.html#averages-and-measures-of-central-location
maks = dataset.max()
minm = dataset.min()
jarak = maks- minm + 1
var = st.variance(dataset,mean)
standarDeviation = st.stdev(dataset)

print('Range : ', jarak, '\nSample variance : ', var, '\nSample standard deviation : ', standarDeviation)

#4 menghitung percentile-15 dan percentile-90
#source https://www.geeksforgeeks.org/numpy-percentile-in-python/
p15 = np.percentile(dataset,15)
p90 = np.percentile(dataset, 90)

print('Percentile-15 : ', p15, '\nPercentile-90 : ', p90)

#5 menghitung  Interquartile Range (IQR), Reasonable Lower Boundary (RLB),
# dan Reasonable Upper Boundary (RUB)
q1 = np.percentile(dataset,25)
q3 = np.percentile(dataset,75)

iqr = q3-q1
rlb = q1 - (3/2)*iqr
rub = q3 + (3/2)*iqr

print('IQR : ', iqr,'\nRLB : ',rlb,'\nRUB : ',rub)

#6 mencari outlier dari data
if (minm<rlb) :
    print ('Didata ini ada outlier')
elif (maks>rub) :
    print ('Didata ini ada outlier')
else :
    print('Tidak ada outlier')

#7 menggambar box plot
f = plt.figure(1)
plt.boxplot(dataset,patch_artist=True,labels=['data'])
f.show()

#8 menggambarkan histogram
g = plt.figure(2)
plt.hist(dataset,bins=10, color = '#000000')
plt.ylabel('Banyak Data')
plt.xlabel('Berat')
g.show()
