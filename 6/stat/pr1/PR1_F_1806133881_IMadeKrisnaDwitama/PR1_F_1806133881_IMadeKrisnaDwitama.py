import statistics, numpy, pandas, matplotlib
import matplotlib.pyplot as plt

with open("500_Person_Gender_Height_Weight_Index.csv") as data:
	raw_data = data.read().split()
data.close()

list_data = []

for i in range(1, len(raw_data)):
	data = raw_data[i].split(',')
	list_data.append(int(data[1]))

print()

print("Mean:", statistics.mean(list_data))
print("Median:", statistics.median(list_data))
print("Mode:", statistics.mode(list_data))

print("Range:", max(list_data) - min(list_data) + 1)
print("Sample Variance:", statistics.variance(list_data))
print("Sample Standard Deviation:", statistics.stdev(list_data))

print("Percentile-15:", numpy.percentile(list_data, 15))
print("Percentile-90:", numpy.percentile(list_data, 90))

Q3 = numpy.percentile(list_data, 75)
Q1 = numpy.percentile(list_data, 25)

IQR = Q3 - Q1
RUB = Q3 + 1.5 * IQR
RLB = Q1 - 1.5 * IQR

print("Interquartile Range (IQR):", IQR)
print("Reasonable Lower Boundary (RLB):", RLB)
print("Reasonable Upper Boundary (RUB):", RUB)

list_outlier = []

for data in list_data:
	if data > RUB or data < RLB:
		outlier.append(data)

print("Outlier: ")
for outlier in list_outlier:
	print(outlier, end = ' ')
print()

df = pandas.DataFrame(list_data, columns=['Height'])
boxplot = df.boxplot(column=['Height'])
plt.show()

bins = [140, 150, 160, 170, 180, 190, 200]

plt.hist(list_data, bins, linewidth=0.5, alpha=0.9, edgecolor='black')
plt.xlabel("Height")
plt.ylabel("Frequency")
plt.show()