import matplotlib.pyplot as mpl
import pandas as pd
import numpy as np
import csv

# Nomor 1
def numberOne():
	print("--------- Nomor 1 ---------")
	sampleMean = heightData.mean()
	sampleMedian = heightData.median()
	sampleMode = heightData.mode()[0]
	print("Sample Mean: ", sampleMean)
	print("Sample Median: ", sampleMedian)
	print("Sample Mode: ", sampleMode)
	print("---------------------------")

# Nomor 2
def numberTwo():
	print("--------- Nomor 2 ---------")
	maxValue = heightData.max()
	minValue = heightData.min()
	rangeValue = maxValue - minValue + 1
	sampleVariance = heightData.var()
	sampleStandardDeviation = heightData.std()
	print("Range : ", rangeValue)
	print("Sample Variance : ", sampleVariance)
	print("Sample Standard Deviation : ", sampleStandardDeviation)
	print("---------------------------")

# Nomor 4
def numberFour():
	print("--------- Nomor 4 ---------")
	percentile15 = np.percentile(heightData, 15)
	percentile90 = np.percentile(heightData, 90)
	print("Percentile 15 : ", percentile15)
	print("Percentile 90 : ", percentile90)
	print("---------------------------")

# Nomor 5
def numberFive():
	print("--------- Nomor 5 ---------")
	percentile75 = np.percentile(heightData, 75)
	percentile25 = np.percentile(heightData, 25)
	interquartileRange = percentile75 - percentile25
	reasonableLowerBoundary = percentile25 - (1.5 * interquartileRange)
	reasonableUpperBoundary = percentile75 + (1.5 * interquartileRange)
	print("Interquartile Range : ", interquartileRange)
	print("Reasonable Lower Boundary : ", reasonableLowerBoundary)
	print("Reasonable Upper Boundary : ", reasonableUpperBoundary)
	print("---------------------------")

# Nomor 6
def numberSix():
	print("--------- Nomor 6 ---------")
	minValue = heightData.min()
	maxValue = heightData.max()
	percentile75 = np.percentile(heightData, 75)
	percentile25 = np.percentile(heightData, 25)
	interquartileRange = percentile75 - percentile25
	reasonableLowerBoundary = percentile25 - (1.5 * interquartileRange)
	reasonableUpperBoundary = percentile75 + (1.5 * interquartileRange)
	if (minValue < reasonableLowerBoundary):
		print("Terdapat Outlier Bawah")
	else:
		print("Tidak Ada Outlier Bawah")

	if (maxValue > reasonableUpperBoundary):
		print("Terdapat Outlier Atas")
	else:
		print("Tidak Ada Outlier Atas")
	print("---------------------------")

# Nomor 7
def numberSeven():
	print("--------- Nomor 7 ---------")
	mainData.boxplot(column=["Height"])
	mpl.show()
	print("---------------------------")

# Nomor 8
def numberEight():
	print("--------- Nomor 8 ---------")
	histo = heightData.hist(bins = 6)
	mpl.show()
	print("---------------------------")

# Fungsi Grouping
def grup(tinggi):
    if(tinggi >= 140 and tinggi < 150):
        return 0
    elif(tinggi >= 150 and tinggi < 160):
        return 1
    elif(tinggi >= 160 and tinggi < 170):
        return 2
    elif(tinggi >= 170 and tinggi < 180):
        return 3
    elif(tinggi >= 180 and tinggi < 190):
        return 4
    elif(tinggi >= 190 and tinggi < 200):
        return 5

# Main Program
def main():
	numberOne()
	print()
	numberTwo()
	print()
	numberFour()
	print()
	numberFive()
	print()
	numberSix()
	print()
	numberSeven()
	print()
	numberEight()

mainData = pd.read_csv("data.csv")
heightData = mainData["Height"]

if __name__== "__main__":
  main()