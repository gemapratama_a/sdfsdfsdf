import pandas
import math
import matplotlib.pyplot as p
filename="data.csv"
f = open(filename, "r")
lines=f.readlines()
lines=lines[1:]
datas=[]
for i in range(len(lines)):
    datas.append(int(lines[i].split(',')[2]))
pData = pandas.DataFrame(datas)
print("Mean               : ")
print(pandas.DataFrame.mean(pData)[0])
print("")
print("Median             : ")
print(pandas.DataFrame.median(pData)[0])
print("")
print("Modus              : ")
mode=str(pandas.DataFrame.mode(pData))
print(mode[mode.find('0')+2:])
print("")
print("Range              :")
print(max(datas)-min(datas))
print("")
print("Variance           :")
print(pData.var()[0])
print("")
print("Standard Deviation :")
print(math.sqrt(float(pData.var()[0])))
print("")
print("Percentile-15      :")
print(pData.quantile(.15)[0])
print("")
print("Percentile-90      :")
print(pData.quantile(.9)[0])
print("")
print("Interquartile range:")
iqr=float(pData.quantile(.75)[0])-float(pData.quantile(.25)[0])
print(iqr)
print("")
print("R Upper Boundary   :")
rub=iqr*1.5+float(pData.quantile(.75)[0])
print(rub)
print("R Lower Boundary   :")
rlb=iqr*-1.5+float(pData.quantile(.25)[0])
print(rlb)
print("Outlier            :")
count=0
flag=False
for i in range(len(datas)):    
    if (datas[i]<rlb or datas[i]>rub):
        count+=1
        flag=true
if(flag):
    print(count)
else:
    print("None")
pData.boxplot()
pData.hist()
p.show()
