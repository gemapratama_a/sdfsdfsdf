import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import matplotlib as mpl # for build graph etc
import seaborn as sns
import matplotlib.pyplot as plt
# %matplotlib inline

# Input data files are available in the "../input/" directory.
df = pd.read_csv('data.csv')
    

# Get the Weight data
weight = df['Weight']

# get the data
weight_mean = weight.mean()
weight_median = weight.median()
weight_mode = weight.mode()

#print the result
print(" Mean = " + str(weight_mean))
print(" Median = " + str(weight_median))
print(" Mode = \n" + str(weight_mode))

# get the rnage, varian dan deviation
rangeData = weight.max() - weight.min()
varian = weight.var()
deviation = weight.std()

print("Range = " + str(rangeData))
print("Varian = " + str(varian))
print("Deviation = " + str(deviation))

# persentile
persentile_90 = weight.quantile(0.9)
persentile_15 = weight.quantile(0.15)

print("Persentile-90 = " + str(persentile_90))
print("Persentile-15 = " + str(persentile_15))

# find the IQR, RLB and RUB
q3 = weight.quantile(0.75)
q1 = weight.quantile(0.25)

IQR = q3 - q1
RLB = q1 - 1.5 * (IQR)
RUB = q3 + 1.5 * (IQR)

print(" IQR = " + str(IQR))
print(" RLB = " + str(RLB))
print(" RUB = " + str(RUB))

# count the outliers
outliers_total = 0
for data in weight :
    if ((data < RLB) or (data > RUB)):
        outliers_total = outliers_total + 1
        
print(" " + str(outliers_total))

# define style of plot
sns.set_style('whitegrid')

# plot the data
f, ax = plt.subplots(figsize = (10, 8))
ax = sns.boxplot(data = df, y = df['Weight'], x = df['Gender'], palette = 'Set2')
ax.set_title(label = 'Weight by Gender', size = 20)
plt.ylabel('Weight', size = 15)
plt.xlabel('Gender', size = 15)
plt.show()

f, ax = plt.subplots(figsize = (10, 8))
ax = sns.boxplot(data = df, y = df['Weight'], x = df['Index'], hue = 'Gender', palette = 'Set2')
ax.set_title('Weight by Index', size = 20)
plt.ylabel('Weight', size = 15)
plt.xlabel('Index', size = 15)
plt.show()

# find the class range
total_group = (max(weight) - min(weight)) // 10

# plot data to histogram
f, ax = plt.subplots(1, 1, figsize=(10, 5))
ax = sns.distplot(df['Weight'], bins = total_group, color = "#3498db", kde = False)
plt.ylabel('Frequency', size = 16)
plt.xlabel('Weight', size = 16)
plt.show()