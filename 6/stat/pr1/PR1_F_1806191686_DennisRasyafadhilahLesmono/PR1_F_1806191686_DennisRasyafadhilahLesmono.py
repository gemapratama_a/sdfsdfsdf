# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import pandas
import matplotlib.pyplot as plot
import numpy
import seaborn
import statistics

path = pandas.read_csv("500_Person_Gender_Height_Weight_Index.csv")
data_weight = path["Weight"]

#Soal Nomor 1
sample_mean = data_weight.mean()
sample_median = data_weight.median()
sample_mode = data_weight.mode()

#Soal Nomor 2
sample_range = data_weight.max() - data_weight.min() + 1
sample_variance = data_weight.var()
sample_std_deviation = data_weight.std()

#Soal Nomor 4
percentile_15_of_data = data_weight.quantile(0.15)
percentile_90_of_data = data_weight.quantile(0.9)

#Soal Nomor 5
IQR_of_data = data_weight.quantile(0.75) - data_weight.quantile(0.25)
RUB_of_data = data_weight.quantile(0.75) + (1.5 * IQR_of_data)
RLB_of_data = data_weight.quantile(0.25) - (1.5 * IQR_of_data)

#Soal Nomor 6
data_outlier =  data_weight.max() > RUB_of_data or data_weight.min() < RLB_of_data

print("sample mean: ", sample_mean)
print("sample median: ", sample_median)
print("sample_mode: ", sample_mode.to_numpy())
print ("sample range: ", sample_range)
print ("sample variance: ", sample_variance)
print ("sample standard deviation: ", sample_std_deviation)
print("Percentile 15 of data: ", percentile_15_of_data)
print("Percentile 90 of data: ", percentile_90_of_data)
print("IQR : ", IQR_of_data)
print("RLB : ", RLB_of_data)
print("RUB : ", RUB_of_data)
print("is there an outlier?: ", data_outlier)

#Soal Nomor 7
seaborn.set(style="whitegrid")
fig = plot.figure(figsize=(10,8))
box_plot = seaborn.boxplot(data = data_weight, orient="h")
fig.savefig('boxplot.png', box_inches='thight')

#Soal Nomor 8
fig = plot.figure(2, figsize=(15,10))
seaborn.distplot(data_weight, kde=False, bins=11)
plot.grid(True)
fig.savefig('histogram.png', box_inches='thight')