#!/usr/bin/env python
# coding: utf-8

# # PR1_F_1606917973_MohammadRaffiAkbar

# ## Importing the modules and dataset

# In[ ]:


import pandas as pd


# In[ ]:


df = pd.read_csv('500_Person_Gender_Height_Weight_Index.csv')
df.head()


# ## Selecting only the Height column because odd NPM number and sorted the data

# In[ ]:


df_height = df.loc[:, ['Height']]
df_height = df_height.sort_values(by=['Height'])
df_height.head()


# ## Calculating sample Mean, Median, and Mode of Height data

# In[ ]:


mean = df_height.mean()
mean


# In[ ]:


median = df_height.median()
median


# In[ ]:


mode = df_height.mode()
mode


# ## Calculating sample Range, Variance, and Standard Deviation of Height data

# In[ ]:


range = df_height.max() - df_height.min() + 1
range


# In[ ]:


var = df_height.var()
var


# In[ ]:


stdev = df_height.std()
stdev


# ## Calculating Percentile-15 and Percentile-90 of Height data

# In[ ]:


df_height.describe([.15, .9])


# ## Calculating Q1 and Q3 of Height data

# In[ ]:


df_height.describe()


# ## Creating Box Plot of Height data

# In[ ]:


boxplot = df_height.boxplot()


# ## Creating Histogram of Height data

# In[ ]:


histogram = df_height.hist()


# In[ ]:




