#NPM Even. Weight Datas Calculated

#Libraries Imported
import numpy as np
import math
import pandas as pd
import statistics as statistics
import matplotlib.pyplot as plt

#Read the downloaded File
read_file = pd.read_csv("500_Person_Gender_Height_Weight_Index.csv")['Weight']

#Making Title
print("=======================================")
print("[DATAS OF WEIGHT FROM 500_PERSON_INDEX]")
print("=======================================")

#Calculate and Print Values from downloaded file
mean                = round(read_file.mean(),2)
print("Weight Mean Value: ", mean)
median              = read_file.median()
print("Weight Median Value: ",median)
modus               = read_file.mode()[0]
print("Weight Mode Value: ",modus)
data_range          = (read_file.max() - read_file.min()) + 1
print("Weight Data Range : ",data_range)
sample_variance     = round(statistics.variance(read_file),2)
print("Sample Variance: ",sample_variance)
standard_deviation  = round(math.sqrt(sample_variance),2)
print("Standard Deviation: ",standard_deviation)
percentile_15       = (np.percentile(read_file,15))
print("Percentile 15 of Weight Datas: ",percentile_15)
percentile_90       = (np.percentile(read_file,90))
print("Percentile 90 of Weight Datas : ",percentile_90)
IQR                 = (np.percentile(read_file,75) - np.percentile(read_file,25))
print("IQR Value: ",IQR)
RLB                 = (np.percentile(read_file,25) - (3/2*IQR))
print("RLB Value: ",RLB)
RUB                 = (np.percentile(read_file,75) + (3/2*IQR))
print("RUB Value: ",RUB)
min_value           = read_file.min()
print("Min Value : ", min_value)
max_value           = read_file.max()
print("Max Value : ",max_value)

#Outlier Finder
print("=======================================")
print("[OUTLIER FINDER]")
print("=======================================")

#Finding Min. Outlier
if RLB>min_value:
    print('Minimum Outlier founded: ',min_value)
else:
    print('No minimum outlier founded')

#Finding Max. Outlier
if RUB<max_value:
    print('Maximum Outlier founded: ',max_value)
else:
    print('No Maximum Outlier founded')

#Making boxplot
bp     =  plt.boxplot(read_file)
plt.xticks([1],["Weight"])
plt.title("Data of Weights Boxplot")
plt.show()

#Making histogram
histogram = plt.hist(read_file, bins =[49.5+i for i in range(0,70,10)] ,range=(50,max_value))
plt.title("Data of Weights Histogram")
plt.show()

#Ending Print
print("=======================================")
print("By Amilah Zahiroh, 1806185462")
print("=======================================")



