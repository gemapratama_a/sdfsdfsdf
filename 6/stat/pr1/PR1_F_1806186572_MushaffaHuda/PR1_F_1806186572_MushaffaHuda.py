#### Mushaffa Huda 1806186572 ####
#### StatProb F Assignment 1 ####

import pandas as pd
import matplotlib.pyplot as plt
import statistics as st
from collections import Counter
import math


def main():
    data = pd.read_csv(r'500_Person_Gender_Height_Weight_Index.csv')
    dh = data['Weight']
    data = [int(i) for i in dh[0:]]
    data.sort()

    # count mean on sample data
    mean = st.mean(data)

    # count median on sample data
    median = st.median(data)

    # count mode on sample data
    mode = find_mode(data)

    print("Mean : ", str(mean))
    print("Median : ", str(median))
    print("Mode : ", str(mode))

    # count range on sample data
    range_data = (max(data) - min(data))

    # count variance on sample data
    variance = st.variance(data)

    # count standard deviation on sample data
    std_deviation = math.sqrt(variance)

    print("Range : ", str(range_data))
    print("Variance : ", str(variance))
    print("Standard Deviation : ", str(std_deviation))

    # count percentile 15 & 90
    print("Percentile-15 : ", str(count_percentile(data, 15)))
    print("Percentile-90 : ", str(count_percentile(data, 90)))

    # Calculate interquartile range of sample data
    Q1 = count_percentile(data, 25)
    Q3 = count_percentile(data, 75)
    IQR = Q3 - Q1

    # Calculate reasonable lower boundaries (RLB)
    RLB = Q1 - (1.5 * IQR)

    # Calculate reasonable upper boundaries (RUB)
    RUB = Q3 + (1.5 * IQR)

    print('Interquartile Range(IQR) : ', str(IQR))
    print('Reasonable Lower Boundaries (RLB) :', str(RLB))
    print('Reasonable Upper Boundaries (RUB) :', str(RUB))

    # creating boxplot on sample data
    bplot = plt.figure(num="Boxplot")
    plt.boxplot(data, vert=False)
    bplot.show()

    # creating histogram on sample data
    histogram = plt.figure(num='Histogram')
    plt.hist(data, bins=6, edgecolor='black', linewidth=1)
    histogram.show()

    plt.show()

######## functions ##########


def find_mean(arr):
    count = 0
    total = 0
    for i in arr:
        count += 1
        total += i
    return total / count


def find_mode(arr):
    res = Counter(arr)
    return res.most_common(1)[0][0]


def count_percentile(arr, percentile):
    total_data = len(arr)
    p = percentile/100
    np = int(p*total_data)
    return (arr[np-1] + arr[np]) / 2


if __name__ == "__main__":
    main()
