import csv, statistics, numpy, pandas
import matplotlib.pyplot as plt

with open('500_Person_Gender_Height_Weight_Index.csv','r') as file:
    reader = csv.reader(file)
    data = list(reader)

heightData = [int(col[1]) for col in data if col[1].isdigit()]

mean = statistics.mean(heightData)
median = statistics.median(heightData)
mode = statistics.mode(heightData)

range = (max(heightData) - min(heightData)) + 1
variance = statistics.variance(heightData)
stdev = statistics.stdev(heightData)

percentile15 = numpy.percentile(heightData, 15)
percentile90 = numpy.percentile(heightData, 90)

iqr = numpy.percentile(heightData, 75) - numpy.percentile(heightData, 25)
rub = numpy.percentile(heightData, 75) + (1.5*iqr)
rlb = numpy.percentile(heightData, 25) - (1.5*iqr)

outlier = [col for col in heightData if (col>rub) or (col<rlb)]

print ("mean, median, mode:", mean, median, mode)
print("range, sample variance, sample standard deviance:", range, variance, stdev)
print("percentile-15, percentile-90:", percentile15, percentile90)
print("iqr, rub, rlb:", iqr, rub, rlb)
print("outlier:", ', '.join(outlier) if len(outlier)>0 else 'none')

pandas.DataFrame(heightData, columns=['height']).plot.box()
plt.show()

plt.hist(heightData, bins=10)
plt.show()