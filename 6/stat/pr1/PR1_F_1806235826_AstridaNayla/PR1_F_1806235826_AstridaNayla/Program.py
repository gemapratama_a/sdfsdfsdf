import pandas as pd
import matplotlib.pyplot as plt

# Reading csv file
data_file = pd.read_csv('500_Person_Gender_Height_Weight_Index.csv')

# Separating scores in the Weight column
data = data_file['Weight']

# MEAN, MEDIAN, MODE
total_person = data.count()
mean = data.mean()
median = data.median()
mode = data.mode()

# RANGE, VARIANCE, STANDARD DEVIATION
max_value = data.max()
min_value = data.min()
data_range = (max_value - min_value) + 1
variance = data.var()
standard_deviation = data.std()

# PERCENTILE 
percentile_15 = data.quantile(0.15)
percentile_90 = data.quantile(0.9)

# IQR, RLB, RUB
third_quartile = data.quantile(0.75)
first_quartile = data.quantile(0.25)
iqr = third_quartile - first_quartile
rub = third_quartile + (1.5 * iqr)
rlb = first_quartile - (1.5 * iqr)

# BOX PLOT
boxplot = data_file.boxplot(column = 'Weight')
boxplot.set_title("Weight Box Plot")

plt.show()

# HISTOGRAM
histogram = data.hist(bins = [50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160], edgecolor="k")
histogram.set_title("Weight Histogram")
histogram.set_xlabel("Weight Value")
histogram.set_ylabel("Frequency")

plt.show()

# OUTLIER
outlier = False
if(max_value > rub or min_value < rlb):
    outlier = True

print("RESULTS")
print("Mean :", mean)
print("Median :", median)
print("Mode :", mode[0], mode[1], mode[2])
print("Range :", data_range)
print("Sample Variance :", variance)
print("Standard Deviation :", standard_deviation)
print("Percentile-15 :", percentile_15)
print("Percentile-90 :", percentile_90)
print("Interquartile Range :", iqr)
print("Reasonable Upper Boundary :", rub)
print("Reasonable Lower Boundary :", rlb)
print("Outlier :", outlier)