import statistics
import pandas
import numpy
import matplotlib.pyplot as plt

def main():
	with open("500_Person_Gender_Height_Weight_Index.csv", "r") as file:
		csv_file = pandas.read_csv(file)
		data = csv_file["Height"]
	print("Body height measurements of 500 person")

	print()
	print("1.")
	print("Mean:", data.mean())
	print("Median:", data.median())
	print("Mode:", data.mode()[0])

	print()
	print("2.")
	maximum = data.max()
	minimum = data.min()
	print("Range:", maximum - minimum + 1)
	print("Variance:", data.var())
	print("Standard deviation:", data.std())

	print()
	print("4.")
	print("Percentile-15:", data.quantile(0.15))
	print("Percentile-90:", data.quantile(0.90))

	print()
	print("5.")
	q1 = data.quantile(0.25)
	q3 = data.quantile(0.75)
	iqr = q3 - q1
	rlb = q1 - 1.5*(iqr)
	rub = q3 + 1.5*(iqr)
	print("Interquartile range:", iqr)
	print("Reasonable lower boundary:", rlb)
	print("Reasonable upper boundary:", rub)

	print()
	print("6.")
	print("Max:", maximum)
	print("Min:", minimum)
	print("Numbers above RUB:", data[data > rub].count())
	print("Numbers above RLB:", data[data < rlb].count())

	print()
	print("7.")
	print("Quartile 1:", q1)
	print("Quartile 3:", q3)
	data.plot.box()
	plt.show()

	ranges = numpy.arange(140, maximum + 10, 10)
	data.plot.hist(data, bins=ranges)
	plt.show()

if __name__ == '__main__':
	main()