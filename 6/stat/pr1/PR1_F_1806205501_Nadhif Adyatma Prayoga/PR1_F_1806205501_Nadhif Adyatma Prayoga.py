import pandas as pd
import matplotlib.pyplot as plt

def main():
    try:
        csv_file = pd.read_csv(r'data.csv')
    except:
        print("Error")
        print("Please put the dataset in the same folder as 'data.csv'")
        return

    table = csv_file['Height']

    print(csv_file)
    print()

    print("Mean : " + str(table.mean()))
    print("Median : " + str(table.median()))
    print("Mode : " + str(table.mode()[0]))
    
    print()
    print("Range : " + str(table.max() - table.min() + 1))
    print("Variance : " + str(table.var()))
    print("Standard Deviation : " + str(table.std()))

    print()
    print("Percentile-15 : " + str(table.quantile(0.15)))
    print("Percentile-90 : " + str(table.quantile(0.9)))

    q1 = table.quantile(0.25)
    q3 = table.quantile(0.75)
    iqr = q3 - q1
    rlb = q1 - 1.5 * iqr
    rub = q3 + 1.5 * iqr

    print()
    print("Interquartile Range : " + str(iqr))
    print("Reasonable Lower Boundary : " + str(rlb))
    print("Reasonable Upper Boundary : " + str(rub))
    print("Number of value below RLB : " + str(table[table < rlb].count()))
    print("Number of value above RUB : " + str(table[table > rub].count()))

    fig = plt.figure()
    fig.suptitle('Height Box Plot', fontsize=14, fontweight='bold')

    box_plot = fig.add_subplot(111)
    box_plot.boxplot(table)
    box_plot.set_ylabel("Height")
    plt.show()
    
    fig = plt.figure()
    fig.suptitle('Height Histogram', fontsize=14, fontweight='bold')

    histogram = fig.add_subplot(111)
    histogram.hist(table, bins=[140, 150, 160, 170, 180, 190, 200])
    histogram.set_xlabel("Height")
    histogram.set_ylabel("Frequency")
    plt.show()


if __name__ == "__main__":
    main()