import pandas
import matplotlib.pyplot as plt

data = pandas.read_csv("500_Person_Gender_Height_Weight_Index.csv")
data_weight = data['Weight']

#====================== Nomor 1 ========================#
print("Mean :", data_weight.mean())
print("Median :", data_weight.median())
print("Mode :", end=" ")
print(*data_weight.mode(), sep=', ')

#=========== Nomor 2 =============#
print("Range :", (data_weight.max() - data_weight.min() + 1))
print("Variance :", data_weight.var())
print("Standard Deviation :", data_weight.std())

#============ Nomor 4 ==============#
#  nilai percentile-15 dan percentile-90
print("Percentile-15 :", data_weight.quantile(0.15))
print("Percentile-90 :", data_weight.quantile(0.9))

#============ Nomor 5 ==============#
q3 = data_weight.quantile(0.75)
q1 = data_weight.quantile(0.25)
iqr = q3 - q1
rlb = q1 - 1.5*iqr
rub = q3 + 1.5*iqr

print(q3)
print("Interquartile Range (IQR) :", iqr)
print("Reasonable Lower Boundary (RLB) :", rlb)
print("Reasonable Upper Boundary (RUB) :", rub)

#============ Nomor 6 ===============#
outlier = (data_weight.max() > rub) | (data_weight.min() < rlb)
print("Outlier: ",  outlier)

#============ Nomor 7 ===============#
# box plot
data.boxplot(column=['Weight'])
plt.show()
data.boxplot(by='Gender', column=['Weight'], grid=False)
plt.show()

#============ Nomor 8 ===============#
data.hist(column=['Weight'],
          bins=[50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160],
          color='#e3acbe', edgecolor='#bd627f', grid=False)
plt.show()

#============ Nomor 9 ===============#

#============ Nomor 10 ===============#
