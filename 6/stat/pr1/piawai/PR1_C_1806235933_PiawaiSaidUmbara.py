import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def find_iqr(data):
    return data.quantile(0.75) - data.quantile(0.25)

def find_rlb(data):
    return data.quantile(0.25) - find_iqr(data) * 1.5

def find_rub(data):
    return data.quantile(0.75) + find_iqr(data) * 1.5

def main():
	# Read data from the 'Height' column in the specified file
	path_to_file = "500_Person_Gender_Height_Weight_Index.csv"
	data = pd.read_csv(path_to_file)["Height"]
	
    # Computations for Problem No. 1
	print("Sample mean: {}".format(data.mean()))
	print("Sample median: {}".format(data.median()))
	print("Sample mode: {}".format(data.mode()[0]))
	
	print()
	
	# Computations for Problem No. 2
	print("Range: {}".format(data.max() - data.min()))
	print("Sample variance: {}".format(data.var()))
	print("Sample standard deviation: {}".format(data.std()))
	
	print()
	
	# Computations for Problem No. 4
	print("Percentile-15: {}".format(data.quantile(0.15)))
	print("Percentile-90: {}".format(data.quantile(0.9)))
	
	print()
	
	# Computations for Problem No. 5
	print("Interquartile Range (IQR): {}".format(find_iqr(data)))
	print("Reasonable Lower Boundary (RLB): {}".format(find_rlb(data)))
	print("Reasonable Upper Boundary (RUB): {}".format(find_rub(data)))
	
	print()
	
	# for Problem No. 7 (Boxplot)
	data.plot(kind="box")
	plt.show()
	
	# for Problem No. 8 (Histogram)
	data.plot(kind="hist")
	plt.show()
	
if __name__ == "__main__":
	main()
