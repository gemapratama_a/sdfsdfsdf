#!/usr/bin/env python
# coding: utf-8

# This py was exported from Keggle Notebook


# This Python 3 environment comes with many helpful analytics libraries installed
# It is defined by the kaggle/python docker image: https://github.com/kaggle/docker-python
# For example, here's several helpful packages to load in 

import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
pd.plotting.register_matplotlib_converters()
import matplotlib.pyplot as plt
import seaborn as sns

# Input data files are available in the "../input/" directory.
# For example, running this (by clicking run or pressing Shift+Enter) will list all files under the input directory

# import os
# for dirname, _, filenames in os.walk('/kaggle/input'):
#     for filename in filenames:
#         print(os.path.join(dirname, filename))

# Any results you write to the current directory are saved as output.
file_data = "./500_Person_Gender_Height_Weight_Index.csv"
data = pd.read_csv(file_data)


# In[2]:
sample = data.drop(['Height', 'Index', 'Gender'], axis = 1)
print(sample.describe())
print()
print(sample.mode())
print()

mean = sample.mean().Weight
std = sample.std().Weight
median = sample.quantile(0.5).Weight
var = sample.var().Weight
rnge = sample.max().Weight - sample.min().Weight
mode = list(sample.mode().Weight)

print("Sample mean: {:.2f}\nSample Median: {:.2f}\nSample Mode: {}\nRange: {:.2f}\nSample Variance: {:.2f}\nSample Standard Deviation: {:.2f}".format(
		mean, median, mode, rnge, var, std))

# Percentile
print("Percentile-15: {:.2f}".format(sample.quantile(0.15).Weight))
print("Percentile-90: {:.2f}".format(sample.quantile(0.9).Weight))

# IQR, RUB, RLB
q1 = sample.quantile(0.25)
q3 = sample.quantile(0.75)
IQR = q3 - q1
print("IQR: {:.2f}".format(IQR.Weight))

RLB = q1 - (1.5 * IQR)
print("RLB: {:.2f}".format(RLB.Weight))

RUB = q3 + (1.5 * IQR)
print("RUB: {:.2f}".format(RUB.Weight))

# BoxPlot
plt.figure(figsize = (5, 3))
plt.title("Weight BoxPlot")
sns.boxplot(y = "Weight", data = sample, whis=3)
plt.show()

# Histogram
plt.figure(figsize = (5, 3))
plt.title("Weight Histogram")
sns.distplot(a=sample, bins= 10,kde=False)
plt.show()