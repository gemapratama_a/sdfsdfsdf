
# Gema Pratama Aditya
# 1706040031
def distancefromab(x, y, a, b):
	return ((x - a)**2 + (y - b)**2)**0.5
	
class Point:
	def __init__(self, x, y, label):
		self.x = x
		self.y = y
		self.label = label

	def __str__(self):
		return "Point ({}, {}) [{}]".format(self.x, self.y, self.label)
		
points = [
	Point(0,2,"+"),
	Point(0,-1,"+"),
	Point(0,-2,"+"),
	Point(-3,0,"+"),
	Point(-1,0,"-"),
	Point(1,0,"-"),
	Point(4,0,"-"),
	#Point(0,3,"-")
]

features = []
labels = []

k_values = [1, 3, 5, 7]

distances = []

for point in points:
	x = point.x
	y = point.y
	a, b = 0,3 # ganti"
	distances.append(distancefromab(x,y,a,b))
	features.append([x, y])
	labels.append(point.label)
	print("Distance from {} to ({}, {}) = {}".format(point, a, b, distancefromab(x,y,a,b)))

distances.sort()
print(distances)



