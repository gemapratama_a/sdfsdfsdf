# Gema Pratama Aditya - 1706040031
combis2 = []
combis3 = []

# 2-combination
for i in range(1, 101):
	for j in range(1, 101):
		combi = (i, j)
		combis2.append(combi)

# 3-combination
for i in range(1, 101):
	for j in range(1, 101):
		for k in range(1, 101):
			combi = (i, j, k)
			combis3.append(combi)

unique2 = set()
unique3 = set()

for c in combis2:
	urut = tuple(sorted(c))
	unique2.add(urut)

for c in combis3:
	urut = tuple(sorted(c))
	unique3.add(urut)


print(len(unique2)) #5050
print(len(unique3)) #171700

print(len(unique2) + len(unique3) + 100) #176850