from matplotlib import pyplot as plt
import numpy as np
import pandas as pd

dataset = pd.read_csv("pasien_liver.csv", header=None) #load the dataset
dataset.head(10)

# PREPROCESSING DATA

classification_data = dataset.drop([10], axis=1).values
classification_label = dataset[10].values
from sklearn import preprocessing




class DecisionTreeClassifier:
    def __init__(self, max_depth=None, min_samples_split=2):
        self.max_depth = max_depth
        self.min_samples_split = min_samples_split
        
    # YOUR CODE HERE
    
    def fit(self, X, y):
        '''Build a decision tree classifier from the training set (X, y)'''
        # YOUR CODE HERE
        raise NotImplementedError()

    def predict_proba(self, X):
        '''Predict class probabilities of the input samples X'''
        # YOUR CODE HERE
        raise NotImplementedError()

    def predict(self, X):
        '''Predict class value for X'''
        # YOUR CODE HERE
        raise NotImplementedError()

"""### Test terhadap Decision Tree Class yang telah dibangun"""

# Train all data until pure
dt = DecisionTreeClassifier()
dt.fit(classification_data, classification_label)
np.testing.assert_almost_equal(dt.predict(classification_data), classification_label)

"""### Implementasi Decision Tree Model dalam training dan testing data"""

from random import seed
from random import randrange
from numpy import empty

# Split a dataset into a train and test set
def train_test_split(data, label, split=0.60):
    train_data = []
    train_label = []
    train_size = split * len(data)
    data_copy = data.tolist()
    label_copy = label.tolist()
    while len(train_data) < train_size:
        index = randrange(len(data_copy))
        train_data.append(data_copy.pop(index))
        train_label.append(label_copy.pop(index))
    
    train_data = np.array(train_data)
    train_label = np.array(train_label)
    data_copy = np.array(data_copy)
    label_copy = np.array(label_copy)
    return train_data, train_label, data_copy, label_copy

"""#### Silahkan melakukan pembagian data untuk training dan testing (default 60:40)"""

# test train/test split
seed(5)
x_train_clf, y_train_clf, x_val_clf, y_val_clf = train_test_split(classification_data,classification_label)

# PLAYGROUND
# you can do anything here as long as not adding any new import

dt_default = DecisionTreeClassifier()
dt_default.fit(x_train_clf, y_train_clf)
dt1 = DecisionTreeClassifier(max_depth=2)
dt1.fit(x_train_clf, y_train_clf)
dt2 = DecisionTreeClassifier(max_depth=3)
dt2.fit(x_train_clf, y_train_clf)
dt3 = DecisionTreeClassifier(max_depth=5)
dt3.fit(x_train_clf, y_train_clf)

"""### Evaluasi"""

from sklearn.metrics import accuracy_score 
from sklearn.metrics import confusion_matrix 
from sklearn.metrics import classification_report 

# Function to calculate accuracy 
def cal_accuracy(y_test, y_pred): 
      
    print("* Confusion Matrix") 
    print(pd.DataFrame(confusion_matrix(y_test, y_pred), \
            index = ['Actual Pasien Liver', 'Actual Pasien Non-Liver'], \
            columns = ['Pred Pasien Liver', 'Pred Pasien Non-Liver'])) 
      
    print ("\n* Accuracy : ", 
    accuracy_score(y_test,y_pred)*100) 
      
    print("\n* Classification Report :\n", 
    classification_report(y_test, y_pred))

dt_y_predict = (dt_default.predict(x_val_clf))
dt1_y_predict = (dt1.predict(x_val_clf))
dt2_y_predict = (dt2.predict(x_val_clf))
dt3_y_predict = (dt3.predict(x_val_clf))

cal_accuracy(y_val_clf, dt_y_predict)

cal_accuracy(y_val_clf, dt1_y_predict)

cal_accuracy(y_val_clf, dt2_y_predict)

cal_accuracy(y_val_clf, dt3_y_predict)

"""## SOAL 2
Gambarkan hasil tree yang telah Anda bangun dalam bentuk <b>representasi teks</b>!

Contoh hasil Tree (ex_dt.PNG terlampir di scele): 

<img src="ex_dt.PNG">

Tree dalam bentuk <b>Representasi Teks</b>:

  - WINDY 
      - true
          - OUTLOOK
              - sunny
                  - HUMIDITY
                      - #<83
                          - yes
                      - #>83
                          - no
              - overcast
                  - yes
              - rainy
                  - no
      - false
          - TEMPERATURE
              - #<83
                  - yes
              - #>83
                  - no

## JAWABAN

## SOAL 3
Berikan analisis Anda berdasarkan hasil evaluasi dan issue yang terdapat dalam Decision Tree Class yang dibangun!

## JAWABAN
"""