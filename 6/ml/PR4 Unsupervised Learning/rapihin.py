import numpy as np

# mengembalikan datapoints dengan label l
# hint: gunakan fungsi ini untuk menemukan titik-titik data yang satu klaster
# atau beda dengan klaster dengan suatu titik data
def get_points(X, labels, l):
	indices = [i for i, x in enumerate(labels) if x == l]
	points = []
	for i in indices:
		points.append(X[i])
	return points

# https://stackoverflow.com/questions/3877491/deleting-rows-in-numpy-array
# https://en.wikipedia.org/wiki/Silhouette_(clustering)

def silhouette_score(X, labels):
	s_arr = []

	a = 0
	b = float('inf')
	# Hitung a
	for i in range(len(X)):
		# Hitung a
		selected_point_a = X[i]
		X_without_selected_point_a = np.delete(X, (i), axis=0)
		total_distance = 0
		for point in X_without_selected_point_a:
			total_distance += euclidean_distance(point, selected_point_a)

		a = total_distance / (len(X) - 1)

		#for i in range(len(points)):
		selected = X[i]
		label_of_selected = labels[i]
		index_same_cluster = np.where(labels == label_of_selected)
		#print("index of same cluster:", index_same_cluster)
		points_in_different_cluster = np.delete(points, list(index_same_cluster), 0)
		separated = []
		mean_values = []
		for i in range(len(set(labels)) - 1):
		  separated.append([])
		for i in range(X):
		  separated[labels[i]].append(X[i])

		for cluster in separated:
			total = np.array([])
			for point in cluster:
				distance = euclidean_distance(point, selected)
				total = np.append(total, distance)

		mean_cluster = np.mean(total)
		mean_values.append(mean_cluster)

		b = min(mean_values)

		# menghitung skor silhouette
		s = (b - a) / max(a, b)
		s_arr.append(s)

	print('Skor silhouette: ', np.mean(s_arr))
	return np.mean(s_arr)