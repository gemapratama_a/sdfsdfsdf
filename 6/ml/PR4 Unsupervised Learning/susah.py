import numpy as np
p = []
for i in range(10):
	p.append([i, i])

p = np.array(p)
l = np.array([0,0,0,1,1,1,2,2,2,2])


for i in range(len(p)):
	selected = p[i]
	label_of_selected= l[i]
	index_same_cluster = list(np.where(l == label_of_selected))
	print("index of same cluster:", index_same_cluster)
	points_in_different_cluster = np.delete(p, index_same_cluster, 0)
	print("point: {}, label: {}, different cluster: {}".format(p[i], l[i], points_in_different_cluster))