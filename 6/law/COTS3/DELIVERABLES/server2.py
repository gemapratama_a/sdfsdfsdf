# Mohon menginstall semua dependensi dulu sebelum menjalankan
# Bekerja di Windows, namun tidak diketahui di macOS dan Linux
from flask import Response, Flask, request, redirect
from werkzeug.middleware.shared_data import SharedDataMiddleware
from flask.templating import render_template
from flask_executor import Executor
from flask.json import jsonify

import os
import pika
import socket
import json
import threading
import gzip
import wget
import requests

DOWNLOAD_FOLDER = './downloads'  # localhost/windows
#UPLOAD_FOLDER = 'uploads' # ubuntu/infralabs

app = Flask(__name__)
executor = Executor(app)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['DOWNLOAD_FOLDER'] = DOWNLOAD_FOLDER
app.add_url_rule('/uploads/<filename>', 'uploaded_file', build_only=True)
app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
	'/uploads':  app.config['DOWNLOAD_FOLDER']
})

app.config['SECRET_KEY'] = os.environ.get(
	'SECRET_KEY') or 'you-will-never-guess'

credentials = pika.PlainCredentials(
	username='0806444524', password='0806444524')
parameters = pika.ConnectionParameters(
	host='152.118.148.95', port=5672, virtual_host='/0806444524', credentials=credentials)

connection = pika.BlockingConnection(parameters=parameters) 
channel = connection.channel()
channel.queue_declare(queue='cots3')

# [B] Spesifikasi exchange
channel.exchange_declare(exchange='1706040031', exchange_type='direct')

# [5] Object untuk menjalankan background process
class BackgroundProcess(object):
	def __init__(self, link, interval=1):
		self.interval = interval
		self.link = link

		# Kalau ada TypeError: download() takes XX positional arguments but ZZ were given,
		# biarkan saja, download tetap berjalan
		thread = threading.Thread(target=self.download, args=(self.link)) 
		
		thread.daemon = True
		thread.start()
	
	def bar_custom(self, current, total, width=80):
		channel.queue_bind(exchange='1706040031', queue='cots3', routing_key="gema1706040031")
		message = "Downloading: %d%% [%d / %d] bytes" % (current / total * 100, current, total)
						
		channel.basic_publish(
			exchange='1706040031',
			routing_key="gema1706040031",
			body=message
		)
	# https://medium.com/@petehouston/download-files-with-progress-in-python-96f14f6417a2
	# @app.route('/download', methods=['POST'])
	def download(self, url):
		filename = wget.download(url, bar=self.bar_custom)
		requests.post(url="http://localhost:5000/download", json={'filename' : filename})



@app.route('/', methods=['POST'])
def get_file():
	response = None
	if request.method == 'POST':
		routingKey = 'gema1706040031'
		link = request.json.get('link')

		try:
			bg = BackgroundProcess(link)
			executor.submit(bg.download, link)
			jsonResponse = jsonify(
				status='berhasil',
				routing_key=routingKey
			)
			return jsonResponse
		except Exception as e:
			print(e)
			jsonResponse = jsonify(
				status='gagal',
				routing_key=routingKey
			)
			return jsonResponse

	return jsonResponse

if __name__ == '__main__':
	app.run(host='localhost', port=5001, threaded=True) # localhost
	#app.run(host='0.0.0.0', port=20155, threaded=True) # infralabs
