# Mohon menginstall semua dependensi dulu sebelum menjalankan

from flask import Response, Flask, request, redirect
from werkzeug.middleware.shared_data import SharedDataMiddleware
from flask.templating import render_template
from flask_executor import Executor

import random
import string
import os
import requests
import pika

UPLOAD_FOLDER = './uploads'  # localhost/windows
#UPLOAD_FOLDER = 'uploads' # ubuntu/infralabs

app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.add_url_rule('/uploads/<filename>', 'uploaded_file', build_only=True)
app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
    '/uploads':  app.config['UPLOAD_FOLDER']
})
app.config['SECRET_KEY'] = os.environ.get(
    'SECRET_KEY') or 'you-will-never-guess'

credentials = pika.PlainCredentials(
    username='0806444524', password='0806444524')
parameters = pika.ConnectionParameters(
    host='152.118.148.95',
    port=5672,
    virtual_host='/0806444524',
    credentials=credentials
)

@app.route('/', methods=['GET'])
def upload_page():
    return render_template('upload.html')


@app.route('/upload', methods=['POST'])
def post_file():
    if request.method == 'POST':
        #header = request.headers
        file = request.files['file']  
        filename = file.filename
        basedir = os.path.abspath(os.path.dirname(__file__))
        
        # [4] UNIQ ID yang digenerate secara random
        UNIQID = ''.join(random.choices(
            string.ascii_uppercase + string.digits, k=8))

        # [4] Header untuk dikirim ke server 2 yang berisi UNIQ ID
        header = {
            'X-ROUTING-KEY': UNIQID,
        }

        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        
        # [4] File untuk dikirim ke server 2
        files = {
            'file': open(os.path.join(
                app.config['UPLOAD_FOLDER'], filename), 'rb'
            )
        }

        # [4] URL server 2
        url = "http://localhost:5001" # localhost
        #url = "http://0.0.0.0:20155" # infralabs
        
        # [4] Mengirim file ke server 2
        requests.post(url=url, files=files, headers=header)

        # Meredirect ke halaman yang menunjukkan persentase kompresi file
        return render_template('result.html', key=UNIQID)

if __name__ == '__main__':
    app.run(host='localhost', port=5000, threaded=True) # localhost
    #app.run(host='0.0.0.0', port=20154, threaded=True) # infralabs
