# Mohon menginstall semua dependensi dulu sebelum menjalankan

from flask import Response, Flask, request, redirect
from werkzeug.middleware.shared_data import SharedDataMiddleware
from flask.templating import render_template
from flask_executor import Executor
from flask.json import jsonify

import os
import pika
import socket
import json
import threading
import gzip

UPLOAD_FOLDER = './uploads'  # localhost/windows
#UPLOAD_FOLDER = 'uploads' # ubuntu/infralabs

app = Flask(__name__)
executor = Executor(app)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.add_url_rule('/uploads/<filename>', 'uploaded_file', build_only=True)
app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
    '/uploads':  app.config['UPLOAD_FOLDER']
})

app.config['SECRET_KEY'] = os.environ.get(
    'SECRET_KEY') or 'you-will-never-guess'

credentials = pika.PlainCredentials(
    username='0806444524', password='0806444524')
parameters = pika.ConnectionParameters(
    host='152.118.148.95', port=5672, virtual_host='/0806444524', credentials=credentials)

connection = pika.BlockingConnection(parameters=parameters) 
channel = connection.channel()
channel.queue_declare(queue='law3')

# [B] Spesifikasi exchange
channel.exchange_declare(exchange='1706040031', exchange_type='direct')

# [5] Object untuk menjalankan background process
class BackgroundProcess(object):
    def __init__(self, interval=1):
        self.interval = interval
        thread = threading.Thread(target=self.compress, args=())
        thread.daemon = True
        thread.start()

    def compress(self, filename, key):
        while True:
            fileSize = os.path.getsize(os.path.join(
                app.config['UPLOAD_FOLDER'], filename))
            chunkSize = 128 # Nilai ini dapat diperbesar untuk mempercepat kompresi
            written = 0
            with open(os.path.join(
                    app.config['UPLOAD_FOLDER'], filename), "rb") as fr, gzip.open(os.path.join(
                    app.config['UPLOAD_FOLDER'], filename) + ".gz", "wb") as fw:

                percentages = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
                while True:
                    chunk = fr.read(chunkSize)
                    if not chunk:
                        break
                    
                    fw.write(chunk)
                    written += chunk.__sizeof__()
                    progress = int(written / fileSize * 100.0)
                    channel.queue_bind(exchange='1706040031', queue='law3', routing_key=key)
                    if progress in percentages:
                        message = "Progress kompresi file {}: {}%".format(filename, progress)
                        percentages.remove(progress)
                        channel.basic_publish(
                            exchange='1706040031',
                            routing_key=key,
                            body=message
                        )
            break


@app.route('/', methods=['POST'])
def get_file():
    response = None
    if request.method == 'POST':
        routingKey = request.headers['X-Routing-Key']
        file = request.files['file']
        filename = file.filename
        #print(f'header: {routingKey}')
        
        # [5] Membuat dan mengeksekusi asynchronous background process
        try:
            bg = BackgroundProcess()
            executor.submit(bg.compress, filename, routingKey)
            jsonResponse = jsonify(
                status='berhasil',
                routing_key=routingKey
            )
            return jsonResponse
        except Exception as e:
            print(e)
            jsonResponse = jsonify(
                status='gagal',
                routing_key=routingKey
            )
            return jsonResponse

    return jsonResponse


if __name__ == '__main__':
    app.run(host='localhost', port=5001, threaded=True) # localhost
    #app.run(host='0.0.0.0', port=20155, threaded=True) # infralabs
