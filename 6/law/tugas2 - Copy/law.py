# Pada soal tidak disebutkan bahwa source code untuk service harus dipisah
# Mohon menginstall dan mengimport semua dependency terlebih dahulu sebelum menjalankan
# python law.py

# REFERENSI:
# https://flask.palletsprojects.com/en/1.1.x/api/
# https://blog.miguelgrinberg.com/post/designing-a-restful-api-with-python-and-flask
# https://www.roytuts.com/python-flask-rest-api-file-upload/
# https://stackoverflow.com/questions/30279473/get-an-uploaded-file-from-a-wtforms-field
# https://eddmann.com/posts/using-basic-auth-and-decorators-in-pythons-flask/
# https://stackoverflow.com/questions/1602934/check-if-a-given-key-already-exists-in-a-dictionary

from flask import send_file, send_from_directory, Response, safe_join, abort, Flask, flash, request, redirect, url_for, send_from_directory, jsonify
from copy import copy
from flask_wtf import FlaskForm
from flask_wtf.file import FileField
from werkzeug.utils import secure_filename
from werkzeug.middleware.shared_data import SharedDataMiddleware
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from flask_migrate import Migrate
from werkzeug.datastructures import ImmutableMultiDict
from pprint import pprint
from flask.templating import render_template
from flask.helpers import send_file
from functools import wraps

import os
import requests
import zipfile

UPLOAD_FOLDER = './uploads'
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png',
					  'jpg', 'jpeg', 'gif', 'csv', 'zip', 'm'}


app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.add_url_rule('/uploads/<filename>', 'uploaded_file', build_only=True)
app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
	'/uploads':  app.config['UPLOAD_FOLDER']
})
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DATABASE_URL') or \
	'sqlite:///' + os.path.join(basedir, 'app.db')
app.config['SECRET_KEY'] = os.environ.get(
	'SECRET_KEY') or 'you-will-never-guess'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
migrate = Migrate(app, db)
db.create_all()

# Table pada database untuk menyimpan file-file yang diupload
class File(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	filename = db.Column(db.String(80), nullable=False)
	filesize = db.Column(db.String(80), nullable=False)
	filetype = db.Column(db.String(80), nullable=False)

	formattedDatetime = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
	pub_date = db.Column(db.DateTime, nullable=False,
						 default=datetime.now())

	def as_dict(self):
		return {c.name: getattr(self, c.name) for c in self.__table__.columns}

	def __repr__(self):
		return '<File {} {} {} {} {}>\n'.format(self.id, self.filename, self.filesize, self.filetype, self.pub_date)

# [OAuth] Autentikasi pengguna berdasarkan access token dari oauth infralabs
def is_authorized(access_token):
	URL = 'http://oauth.infralabs.cs.ui.ac.id/oauth/resource'
	value = 'Bearer {}'.format(access_token)
	parameter = {
		'Authorization': value
	}
	response = requests.get(url=URL, headers=parameter)
	
	if 'user_id' in response.json() and 'error' not in response.json():
		return True
	else:
		return False

# [OAuth] Autentikasi pengguna berdasarkan access token dari oauth infralabs
def authenticate(f):
	@wraps(f)
	def wrapper(*args, **kwargs):
		credential = request.headers
		if not is_authorized(credential.get('access_token')):
			print("auth")
			response = {
				'status':'failed',
				'message': 'credentials tidak valid'
			}
			return jsonify(response), 401
		return f(*args, **kwargs)
	return wrapper

# [Service 3] Mendapatkan file dari suatu URL
@app.route('/uploaded_files/<filename>', methods=['GET'])
@authenticate
def uploaded_file(filename):
	try:
		print("masuk method uploaded_file GET")
		print(request.authorization)
		allFiles = File.query.all()
		print(allFiles)
		return send_from_directory(app.config['UPLOAD_FOLDER'],
								filename)
	except Exception as e:
		response = {
			'error': str(e)
		}
		return jsonify(response), 404
		
# [Service 3] Menghandle DELETE request
# Hanya dapat menghapus row berdasarkan filename saja, karena menurut saya
# kurang masuk akal apabila kita hapus file berdasarkan date created dari file 
# atau size dari file itu
@app.route('/uploaded_files/<filename>', methods=['DELETE'])
@authenticate
def delete_file(filename):
	allFiles = File.query.all()
	print("Sebelum update:\n", allFiles)
	try:
		os.remove(os.path.join(app.config['UPLOAD_FOLDER'], filename))
		targetRow = File.query.filter_by(filename=filename).delete()
		db.session.commit()
		allFiles = File.query.all()
		print("Sebelum update:\n", allFiles)
		response = {
			'response' : 'Berkas {} berhasil dihapus.'.format(filename)
		}
		return jsonify(response)
	except Exception as e:
		print(e)
		response = {
			'error': str(e)
		}
		return jsonify(response), 404

# [Service 3] Menghandle update (PUT) request
# Hanya dapat mengupdate atribut filename saja, karena menurut saya
# kurang masuk akal apabila kita mengupdate date created dari file atau 
# size dari file itu
@app.route('/uploaded_files/<filename>/to/<newfilename>', methods=['PUT'])
@authenticate
def update_file(filename, newfilename):
	print("masuk method uploaded_file PUT")
	#allFiles = File.query.all()
	#print("Sebelum update:\n", allFiles)
	try:
		basedir = app.config['UPLOAD_FOLDER'] + '/'
		os.rename(basedir + filename, basedir + newfilename)

		# Mengupdate data pada database
		oldData = File.query.filter_by(filename=filename).first()
		oldData.filename = newfilename
		db.session.commit()

		#allFiles = File.query.all()
		#print("Sesudah update:\n", allFiles)
		response = {
			'response': 'Berkas {} berhasil direname menjadi {}.'.format(filename, newfilename)
		}
		return jsonify(response)
	except Exception as e:
		print(e)
		response = {
			'error': str(e)
		}
		return jsonify(response), 404

# [Service 1] Mengembalikan file yang sudah dicompress (.zip)
@app.route('/upload', methods=['POST'])
@authenticate
def post_file():
	if request.method == 'POST':
		header = request.headers
		dataDict = request.files.to_dict(flat=False)
		file = dataDict['file'][0]
		size = header.get('Content-Length', type=int)
		filename = file.filename
		zip = zipfile.ZipFile(filename + ".zip", mode='w', compression=zipfile.ZIP_DEFLATED)
		zip.writestr(filename, file.read())
		zip.close()
		row = File(filename=filename, filesize=size, filetype=filename.split(".")[1])
		db.session.add(row)
		db.session.commit()
		basedir = os.path.abspath(os.path.dirname(__file__))
		file.save(os.path.join(
				basedir, app.config['UPLOAD_FOLDER'], filename))
		return send_file(filename + '.zip', as_attachment=True)

# [Service 3] Menampilkan metadata dari semua file yang ada pada database
@app.route('/metadata')
@authenticate
def metadata():
	response = dict()
	for row in db.session.query(File).all():
		response[row.id] = "{}|{}|{}|{}|{}".format(row.id, row.filename, row.filesize, row.filetype, row.pub_date)
	
	return jsonify(response)

@app.route('/', methods=['GET'])
@authenticate
def upload_file():
	print(request.headers)
	return render_template('upload.html')


if __name__ == '__main__':
	app.run(host='localhost', port=20153, debug=True)