# https://flask.palletsprojects.com/en/1.1.x/api/
# https://blog.miguelgrinberg.com/post/designing-a-restful-api-with-python-and-flask
# https://www.roytuts.com/python-flask-rest-api-file-upload/
# https://stackoverflow.com/questions/30279473/get-an-uploaded-file-from-a-wtforms-field
#from flask import url_for, redirect, render_template, Flask, Request
from flask import Flask, flash, request, redirect, url_for, send_from_directory

from flask_wtf import FlaskForm
from flask_wtf.file import FileField
from werkzeug.utils import secure_filename
from werkzeug.middleware.shared_data import SharedDataMiddleware

import os	
import tarfile

UPLOAD_FOLDER = './uploads'
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'csv', 'zip', 'm'}

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.add_url_rule('/uploads/<filename>', 'uploaded_file', build_only=True)
app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
	'/uploads':  app.config['UPLOAD_FOLDER']
})

def compress_file(filename):
	tar = tarfile.open(filename + ".tar.gz", mode="w:")
	tar.add(filename)
	tar.close()
	return tar
	
def allowed_file(filename):
	return '.' in filename and \
		   filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/uploaded_files/<filename>')
def uploaded_file(filename):
	#print("masuk method uploaded_file")
	return send_from_directory(app.config['UPLOAD_FOLDER'],
							   filename)

@app.route('/', methods=['GET', 'POST'])
def upload_file():
	#print("masuk method")
	if request.method == 'POST':
		# check if the post request has the file part
		if 'file' not in request.files:
			flash('No file part')
			return redirect(request.url)
		file = request.files['file']
		
		#print("FILE", file)
		if file.filename == '':
			flash('No selected file')
			return redirect(request.url)
		if file and allowed_file(file.filename):
			#print("masuk if")
			basedir = os.path.abspath(os.path.dirname(__file__))
			#basedir = os.path.abspath(os.path.dirname(file))
			filename = secure_filename(file.filename)
			print("Before:\nFILE", file)
			print("Basedir:", basedir)
			print("appconfiguploadfolder:", app.config['UPLOAD_FOLDER'])
			newPath = os.path.join(basedir, filename)
			file = compress_file(newPath)
			print("After:\nFILE", file)
			file.save(os.path.join(basedir, app.config['UPLOAD_FOLDER'], filename))

			return redirect(url_for('uploaded_file',
									filename=filename))
	return '''
	<!doctype html>
	<title>Upload new File</title>
	<h1>Upload new File</h1>
	<form method=post enctype=multipart/form-data>
	  <input type=file name=file>
	  <input type=submit value=Upload>
	</form>
	'''

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=20151, debug=True)