import zipfile

def compress_file_zip(filename):
    zip = zipfile.ZipFile(filename + ".zip", mode='w', compression=zipfile.ZIP_DEFLATED)
    zip.write(filename)
    zip.close()
    return zip

print(compress_file_zip('a.txt'))