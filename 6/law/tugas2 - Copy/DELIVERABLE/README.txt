Pada soal tidak dikatakan bahwa tugas ini harus dideploy pada server 152.118.148.95 seperti pada tugas 1.
Namun saya tetap mendeploynya untuk berjaga-jaga. App saya dapat dilihat pada http://152.118.148.95:20153 

Saya mengerjakan seluruh app ini di Windows 10, dan mendeploynya pada infralabs yang menggunakan ubuntu.
Mohon dimaklumi *apabila* terdapat error yang dikarenakan ketidakcocokan OS pada kode. Tugas ini cukup berat.