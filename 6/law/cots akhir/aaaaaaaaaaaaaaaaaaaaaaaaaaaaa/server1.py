# Mohon menginstall semua dependensi dulu sebelum menjalankan
# Bekerja di Windows, namun tidak diketahui di macOS dan Linux
from flask import Response, Flask, request, redirect
from werkzeug.middleware.shared_data import SharedDataMiddleware
from flask.templating import render_template
from flask_executor import Executor
from clint.textui import progress
from flask.json import jsonify

import random
import string
import os
import requests
import pika
import wget
from flask.helpers import send_from_directory
from werkzeug.exceptions import abort

dir_path = os.path.dirname(os.path.realpath(__file__))
DOWNLOAD_FOLDER = './downloads'  # localhost/windows


app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['DOWNLOAD_FOLDER'] = DOWNLOAD_FOLDER
app.add_url_rule('/uploads/<filename>', 'uploaded_file', build_only=True)
app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
	'/uploads':  app.config['DOWNLOAD_FOLDER']
})
app.config['SECRET_KEY'] = os.environ.get(
	'SECRET_KEY') or 'you-will-never-guess'

credentials = pika.PlainCredentials(
	username='0806444524', password='0806444524')
parameters = pika.ConnectionParameters(
	host='152.118.148.95',
	port=5672,
	virtual_host='/0806444524',
	credentials=credentials
)

@app.route('/', methods=['GET'])
def download_page():
	return render_template('download.html')

@app.route('/download', methods=['POST'])
def post_url():
	url = str(request.form['url'])
	filename = url.split("/")[-1]
	
	server = "http://localhost:5001/" # localhost
	requests.post(url=server, json={'link':url})

	return render_template('result.html', key="gema1706040031", filename=filename)
	#return render_template('result.html', key="gema1706040031")

@app.route("/downloads/<filename>")
def get_file(filename):
    try:
        return send_from_directory(dir_path, filename=filename, as_attachment=True)
    except FileNotFoundError:
        abort(404)


if __name__ == '__main__':
	app.run(host='localhost', port=5000, threaded=True) # localhost
	#app.run(host='0.0.0.0', port=20154, threaded=True) # infralabs
