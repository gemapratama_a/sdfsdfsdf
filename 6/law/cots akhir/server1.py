# Halaman
# Message producer
from flask import Response, Flask, request, redirect
from werkzeug.middleware.shared_data import SharedDataMiddleware
from flask.templating import render_template
from flask_executor import Executor
from clint.textui import progress
from flask.json import jsonify

import random
import string
import os
import requests
import pika
import wget
from flask.helpers import send_from_directory
from werkzeug.exceptions import abort

dir_path = os.path.dirname(os.path.realpath(__file__))
DOWNLOAD_FOLDER = './downloads'  # localhost/windows


app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['DOWNLOAD_FOLDER'] = DOWNLOAD_FOLDER
app.add_url_rule('/uploads/<filename>', 'uploaded_file', build_only=True)
app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
	'/uploads':  app.config['DOWNLOAD_FOLDER']
})
app.config['SECRET_KEY'] = os.environ.get(
	'SECRET_KEY') or 'you-will-never-guess'

credentials = pika.PlainCredentials(
	username='0806444524', password='0806444524')
parameters = pika.ConnectionParameters(
	host='152.118.148.95',
	port=5672,
	virtual_host='/0806444524',
	credentials=credentials
)
connection = pika.BlockingConnection(parameters=parameters) 
channel = connection.channel()
channel.queue_declare(queue='cots_akhir', durable=False)

# [B] Spesifikasi exchange
channel.exchange_declare(exchange='1706040031_DIRECT', exchange_type='direct')
channel.exchange_declare(exchange='1706040031_TOPIC', exchange_type='topic')

@app.route('/form', methods=['GET'])
def form_page():
	return render_template('form.html')

@app.route('/progress', methods=['POST'])
def post_url():
	print("[server1] request.form")
	print(request.form)
	urls = dict()

	for key in request.form:
		#print("masuk for")
		#print(key, request.form[key])
		urls[key] = request.form[key]
	
	print("[server1] urls")
	print(urls)
	server = "http://localhost:5002/" # localhost
	requests.post(url=server, json=urls)

	print("[server 1]1 sblm masuk render_template")
	return render_template('progress.html', key="gema1706040031", urls=urls)#, filename=filename)
	#return render_template('result.html', key="gema1706040031")

@app.route("/downloads/<filename>")
def get_file(filename):
    try:
        return send_from_directory(dir_path, filename=filename, as_attachment=True)
    except FileNotFoundError:
        abort(404)


if __name__ == '__main__':
	app.run(host='localhost', port=5001, threaded=True) # localhost
	#app.run(host='0.0.0.0', port=20154, threaded=True) # infralabs
