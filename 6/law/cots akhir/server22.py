# Progress download
# Message consumer
from flask import Response, Flask, request, redirect
from werkzeug.middleware.shared_data import SharedDataMiddleware
from flask.templating import render_template
from flask_executor import Executor
from clint.textui import progress
from flask.json import jsonify

import random
import string
import os
import requests
import pika
import wget
from flask.helpers import send_from_directory
from werkzeug.exceptions import abort
import threading
#import pika_pool

dir_path = os.path.dirname(os.path.realpath(__file__))
DOWNLOAD_FOLDER = './downloads'  # localhost/windows


app = Flask(__name__)
executor = Executor(app)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['DOWNLOAD_FOLDER'] = DOWNLOAD_FOLDER
app.add_url_rule('/uploads/<filename>', 'uploaded_file', build_only=True)
app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
	'/uploads':  app.config['DOWNLOAD_FOLDER']
})
app.config['SECRET_KEY'] = os.environ.get(
	'SECRET_KEY') or 'you-will-never-guess'

credentials = pika.PlainCredentials(
	username='0806444524', password='0806444524')
parameters = pika.ConnectionParameters(
	host='152.118.148.95',
	port=5672,
	virtual_host='/0806444524',
	credentials=credentials
)

#connection = pika.BlockingConnection(parameters=parameters) 
"""
connection = pika.BlockingConnection(parameters=parameters) 
channel = connection.channel()
channel.queue_declare(queue='cots_akhir')

# [B] Spesifikasi exchange
channel.exchange_declare(exchange='1706040031', exchange_type='direct')
"""

class BackgroundProcess(object):
	def __init__(self, link, interval=1):
		self.interval = interval
		self.link = link

		self.connection = pika.BlockingConnection(parameters=parameters) 
		self.channel = self.connection.channel()
		self.channel.queue_declare(queue='cots_akhir')

		# [B] Spesifikasi exchange
		self.channel.exchange_declare(exchange='1706040031_DIRECT', exchange_type='direct')

		# Kalau ada TypeError: download() takes XX positional arguments but ZZ were given,
		# biarkan saja, download tetap berjalan
		self.thread = threading.Thread(target=self.download, args=(self.link,)) 
		
		#self.thread.daemon = True
		self.thread.start()
	
	def bar_custom(self, current, total, width=80):
		self.channel.queue_bind(exchange='1706040031_DIRECT', queue='cots_akhir', routing_key="gema1706040031")
		message = "Downloading: %d%% [%d / %d] bytes" % (current / total * 100, current, total)
						
		self.channel.basic_publish(
			exchange='1706040031',
			routing_key="gema1706040031",
			body=message
		)
	# https://medium.com/@petehouston/download-files-with-progress-in-python-96f14f6417a2
	# @app.route('/download', methods=['POST'])
	def download(self, url):
		print("[server2][download method] url:", url)
		filename = wget.download(url, bar=self.bar_custom)
		requests.post(url="http://localhost:5001/download", json={'filename' : filename})

@app.route('/', methods=['POST'])
def get_file():
	response = None
	if request.method == 'POST':
		print("[server2] masuk post")
		print("json:", request.json)
		routingKey = 'gema1706040031'

		for i in range(1, 11):
			link = ""
			if i != 10:
				link = request.json.get("url0" + str(i))
			else:
				link = request.json.get("url10")
			try:
				print("[server2] [try] link: ", link)
				bg = BackgroundProcess(link)
				executor.submit(bg.download, link)
				jsonResponse = jsonify(
					status='berhasil',
					routing_key=routingKey
				)
				return jsonResponse
			except Exception as e:
				print(e)
				jsonResponse = jsonify(
					status='gagal',
					routing_key=routingKey
				)
				return jsonResponse

	return jsonResponse


@app.route('/', methods=['GET'])
def handle_download():
	return render_template('progress.html')


if __name__ == '__main__':
	app.run(host='localhost', port=5002, threaded=True) # localhost

