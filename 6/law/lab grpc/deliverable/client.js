const grpc = require("grpc");
const protoLoader = require("@grpc/proto-loader");

//Load the protobuf
const proto = grpc.loadPackageDefinition(
    protoLoader.loadSync("../example.proto", {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    })
);

//Create a new client instance that binds to the IP and port of the grpc server.
const client = new proto.example.ExampleService(
    "localhost:50050",
    grpc.credentials.createInsecure()
);

client.sayHello({ greeting: "Gema" }, (error, response) => {
    if (!error) {
        console.log(response.reply);
    } else {
        console.log("Error:", error.message);
    }
});

client.doAddition({ first_number: 1, second_number: 2 }, (error, response) => {
    if (!error) {
        console.log("Addition: " + response.result);
    } else {
        console.log("Error:", error.message);
    }
});

client.doSubtraction({ first_number: 1, second_number: 2 },
    (error, response) => {
        if (!error) {
            console.log("Substraction: " + response.result);
        } else {
            console.log("Error:", error.message);
        }
    }
);



client.doLabFunction({
    first_number: 10,
    second_number: 32,
    third_number: 1,
    fourth_number: 8,
    fifth_number: 9
}, (error, response) => {
    if (!error) {
        console.log("\n[LAB] [Node] Output:");
        console.log("Minimum number: " + response.minimum);
        console.log("Maximum number: " + response.maximum);
        console.log("Sum of numbers: " + response.sum);
        console.log("------End of lab output------\n");
    } else {
        console.log("Error:", error.message);
    }
})