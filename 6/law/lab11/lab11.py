import logging
from flask import Flask, request
from logstash_async.handler import AsynchronousLogstashHandler
from logstash_async.handler import LogstashFormatter
from flask.json import jsonify
import json

app = Flask(__name__)

# Create the logger and set it's logging level
logger = logging.getLogger("logstash")
logger.setLevel(logging.INFO)        

# Create the handler
handler = AsynchronousLogstashHandler(
    host='58f33aa1-b958-43a3-b6da-bc57d248523c-ls.logit.io', 
    port=24835, 
    ssl_enable=True, 
    ssl_verify=False,
    database_path='')

# Here you can specify additional formatting on your log record/message
formatter = LogstashFormatter()
handler.setFormatter(formatter)

# Assign handler to the logger
logger.addHandler(handler)

# Send log records to Logstash 
# logger.error('python-logstash-async: test error message.')
# logger.info('python-logstash-async: test info message.')
# logger.warning('python-logstash-async: test warning message.')
# logger.debug('python-logstash-async: test debug message.')

def fibonacci(n: int):
	logger.info("Service dipanggil dengan n = {}".format(n + 1))
	answer = [0]
	logger.info("[VARIABLE] Answer: {}".format(answer))
	a, b = 0, 1
	logger.info("[VARIABLE] a, b: {}, {}".format(a, b))
	for i in range(0, n):
		a, b = b, a + b
		logger.info("[VARIABLE] i: {}".format(i))
		logger.info("[VARIABLE] a, b (setelah diupdate): {}, {}".format(a, b))
		answer.append(a)

	logger.info("Hasil (dalam JSON): {}".format(answer))
	logger.info("Service selesai dijalankan")
	logger.info("===============================\n\n")

	return str(answer)

@app.route('/')
def index():
	try:
		n = int(request.args.get('n'))
		return fibonacci(n - 1)
	except TypeError as te:
		error_info = str(te) + ". Apakah anda lupa memberi nilai pada parameter n?"
		logger.info(error_info)
		return error_info

	
if __name__ == "__main__":
	HOST = 'localhost'
	PORT = 8000
	logger.info("App dijalankan pada {}:{}".format(HOST, PORT))
	logger.info("==================================")
	app.run(host=HOST, port=PORT) 	