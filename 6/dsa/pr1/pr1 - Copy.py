import pandas as pd
import plotly.express as px
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn import preprocessing

import json


# Mendeklarasikan dataset yang akan diubah menjadi objek DataFrame untuk dilakukan pengolahan
dfWithNull = pd.read_csv(filepath_or_buffer='bird.csv', header=None, sep=',')
dfWithNull.columns = [
    'id',
    'huml',
    'humw',
    'ulnal',
    'ulnaw',
    'feml',
    'femw',
    'tibl',
    'tibw',
    'tarl',
    'tarw',
    'type'
]
#print(dfWithNull.describe())

# Berdasarkan hasil describe, jumlah row adalah 420, tetapi semua atribut (kecuali id dan type)
# memiliki paling sedikit satu nilai kosong. Maka dari itu, kita akan mengisinya nilai-nilai 
# kosong tersebut dengan rata-rata dari tiap atribut.
# https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.mean.html
# https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.fillna.html
# https://www.w3schools.com/python/python_json.asp
# https://dev.to/chanduthedev/how-to-display-all-rows-from-data-frame-using-pandas-dha
meanJson = dfWithNull.mean(axis = 0, skipna = True).to_json()
meanDict = json.loads(meanJson)

# Variabel ini mengandung nilai rata-rata dari setiap atribut.
valuesToFill = {
    'huml': meanDict['huml'],
    'humw': meanDict['humw'],
    'ulnal': meanDict['ulnal'],
    'ulnaw': meanDict['ulnaw'],
    'feml': meanDict['feml'],
    'femw': meanDict['femw'],
    'tibl': meanDict['tibl'],
    'tibw': meanDict['tibw'],
    'tarl': meanDict['tarl'],
    'tarw': meanDict['tarw'],
}

df = dfWithNull.fillna(value=valuesToFill)

#print(df.describe())

# https://www.edureka.co/blog/principal-component-analysis/

df.drop('type', axis=1)
"""
df.hist(column='tarw', bins=25, grid=False, figsize=(12,8), color='#86bf91', zorder=2, rwidth=0.9)
df.hist(column='femw', bins=25, grid=False, figsize=(12,8), color='#86bf91', zorder=2, rwidth=0.9)
df.hist(column='tibw', bins=25, grid=False, figsize=(12,8), color='#86bf91', zorder=2, rwidth=0.9)
df.hist(column='humw', bins=25, grid=False, figsize=(12,8), color='#86bf91', zorder=2, rwidth=0.9)
df.hist(column='ulnaw', bins=25, grid=False, figsize=(12,8), color='#86bf91', zorder=2, rwidth=0.9)

sns.lmplot(x='huml', y='humw', data=df, fit_reg = False)
plt.show()
sns.lmplot(x='ulnal', y='ulnaw', data=df, fit_reg = False)
plt.show()
sns.lmplot(x='feml', y='femw', data=df, fit_reg = False)
plt.show()
sns.lmplot(x='tibl', y='tibw', data=df, fit_reg = False)
plt.show()
sns.lmplot(x='tarl', y='tarw', data=df, fit_reg = False)
plt.show()
"""

numericData = df.iloc[:, 1:11]
typeData = df.iloc[:, 11]

dimensions = [
    'huml',
    'humw',
    'ulnal',
    'ulnaw',
    'feml',
    'femw',
    'tibl',
    'tibw',
    'tarl',
    'tarw',
]

fig = px.scatter_matrix(df, dimensions=dimensions, color='type')
#fig.show()
print("NUMERIC DATA [{}]:\n{}\n".format(type(numericData), numericData))
print("NUMERIC DATA VALUES [{}]:\n{}\n".format(type(numericData.values), numericData.values))
print("TYPE DATA:\n{}\n".format(typeData))

#print("TYPE DATA VALUES:\n{}\n".format(typeData.values))

#standardizedData = preprocessing.scale(numericData)
standardizedData = StandardScaler().fit_transform(numericData)


print("STANDARDIZED DATA [{}]:\n{}]\n".format(type(standardizedData), standardizedData))


#covarianceMatrix = numericData.cov()
covarianceMatrix = np.cov(standardizedData.T)

#mean_vec = np.mean(standardizedData, axis= 0)
#cov_mat = (standardizedData - mean_vec).T.dot((standardizedData - mean_vec)) / (standardizedData.shape[0] - 1)
#print("COV_MAT{}:\n{}\n".format(type(cov_mat), cov_mat))

print("COVARIANCE MATRIX {}: \n{}\n".format(type(covarianceMatrix), covarianceMatrix))

#print(type(covarianceMatrix.values))

eigenValues, eigenVectors = np.linalg.eig(covarianceMatrix)

print("EIGEN VALUES:\n{}\n".format(eigenValues))
print("EIGEN VECTORS:\n{}\n".format(eigenVectors))

eigenPairs = []

for i in range(len(eigenValues)):
    eigenValue = np.abs(eigenValues[i])
    eigenVector = eigenVectors[:, i]
    pair = (eigenValue, eigenVector)
    eigenPairs.append(pair)

eigenPairs.sort()
eigenPairs.reverse()

print("EIGENPAIRS:\n")
for pair in eigenPairs:
    print(pair[0])

total = sum(np.abs(eigenValues))
percentages = []

for var in np.abs(eigenValues):
    percentage = float(var / total) * 100.0
    percentages.append(percentage)

print("PERCENTAGES:")
for i in percentages:
    print(i)

print(sum(percentages))

W = np.hstack((eigenPairs[0][1].reshape(10, 1), eigenPairs[1][1].reshape(10,1)))

print("W:\n{}\n".format(W))

Y = standardizedData.dot(W)

print("Y:\n{}\n".format(Y))
print(type(Y))

# https://matplotlib.org/2.1.1/api/_as_gen/matplotlib.pyplot.plot.html
colors = ['r', 'b', 'g', 'y', 'k', 'c']
markers = ['|' , '+' , 'p', '*', 'H', 'x']
for l, c, m in zip (np.unique(typeData), colors, markers):
    plt.scatter(Y[typeData==l, 0],
    Y[typeData==l, 1],
    c=c, label=l, marker=m)

plt.title('Y')
plt.xlabel('PC 1')
plt.ylabel('PC 2')
plt.legend(loc='lower left')
plt.tight_layout()
plt.show()