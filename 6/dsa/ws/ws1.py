import math
import numpy as np

S = "sosial"
K = "keilmuan"
O = "olahraga"
T = "tinggi"
R = "rendah"
organisasi = [S,	 S,		K, 	 O,	  O,	O,		K,   S,		S,	  O,  S,	K, 	 K,	   O]
ujianTulis = [85,    80,    83,  70,  68,   65,    64,   72,    69,   75, 75,  72,   81,  71] # 73.571
wawancara  = [85,    90,    86,  96,  80,   70,    65,   95,    70,   80, 70,  90,   75,  91] # 81.642
exchange   = [False, False, True,True,True, False, True, False, True,True,True,True,True, False]
komunikasi = [T,	 R,		T,	 T,   T,	R,		R,	 T,		T,	  T,  R,	R,	 T,		R]
#print(len(ujianTulis) == len(wawancara) == len(exchange))
ujianYa = []
ujianTidak = []
wawancaraYa = []
wawancaraTidak = []

e = math.e
pi = math.pi 
base = 1 / (2*pi)**0.5
#exp = 

for i in range(len(ujianTulis)):
	if exchange[i] == True:
		ujianYa.append(ujianTulis[i])
	else:
		ujianTidak.append(ujianTulis[i])

for i in range(len(wawancara)):
	if exchange[i] == True:
		wawancaraYa.append(wawancara[i])
	else:
		wawancaraTidak.append(wawancara[i])



def gaussian(miu, sigma, x):
	exponent = (x - miu)**2 / (2* (sigma**2))
	base = 1 / (sigma * (2 * pi)**0.5)
	return base * e**(-exponent)
#print(mean(ujianTulis))
#print(mean(wawancara))

# Untuk kelas “Ya” rerata dan standar deviasi dari atribut nilai ujian dan atribut wawancara adalah
print("miu ujian ya: {}".format(np.mean(ujianYa)))
print("std dev ujian ya: {}".format(np.std(ujianYa, ddof=1)))
print("miu wawancara ya: {}".format(np.mean(wawancaraYa)))
print("std dev wawancara ya: {}".format(np.std(wawancaraYa,ddof=1)))


#Untuk kelas “Tidak” rerata dan standar deviasi dari atribut nilai ujian dan atribut wawancara adalah 

print("\n\nmiu ujian tidak: {}".format(np.mean(ujianTidak)))
print("std dev ujian tidak: {}".format(np.std(ujianTidak, ddof=1)))
print("miu wawancara tidak: {}".format(np.mean(wawancaraTidak)))	
print("std dev wawancara tidak: {}".format(np.std(wawancaraTidak, ddof=1)))

#Gunakan hasil di atas untuk menghitung Gaussian density function:
fujiantulis60ya = gaussian(np.mean(ujianYa), np.std(ujianYa), 60)
fujiantulis60tidak = gaussian(np.mean(ujianTidak), np.std(ujianTidak), 60)
fwawancara62ya = gaussian(np.mean(wawancaraYa), np.std(wawancaraYa), 62)
fwawancara62tidak = gaussian(np.mean(wawancaraTidak), np.std(wawancaraTidak), 62)

print("f(ujian tulis = 60 | ya) = {}".format(fujiantulis60ya))
print("f(ujian tulis = 60 | tidak) = {}".format(fujiantulis60tidak))
print("f(wawancara = 62 | ya) = {}".format(fwawancara62ya))
print("f(wawancara = 62 | tidak) = {}".format(fwawancara62tidak))

pya = exchange.count(True) / len(exchange)
ptidak =exchange.count(False) / len(exchange)

#Tahap 2: prior probability dan likelihood untuk atribut nominal
print("P(Ya) =    ", pya)
print("P(Tidak) = ", ptidak)

countKeilmuanYa = 0
countKeilmuanTidak = 0
countTinggiYa = 0
countTinggiTidak = 0
for i in range(len(exchange)):
	if organisasi[i] == K and exchange[i] == True:
		countKeilmuanYa += 1
	if organisasi[i] == K and exchange[i] == False:
		countKeilmuanTidak += 1
	if komunikasi[i] == T and exchange[i] == True:
		countTinggiYa += 1
	if komunikasi[i] == T and exchange[i] == False:
		countTinggiTidak += 1 

print("P(Organisasi = keilmuan | Ya) = ", (countKeilmuanYa / len(exchange)) / pya)
print("P(Organisasi = keilmuan | Tidak) = ", (countKeilmuanTidak / len(exchange)) / ptidak)
print("P(Komunikasi = tinggi | Ya) = ", (countTinggiYa / len(exchange)) / pya)
print("P(Komunikasi = tinggi | Tidak) = ", (countTinggiTidak / len(exchange)) / ptidak)