import pandas as pd
from scipy.spatial import distance_matrix

data = [
	[0.8, 1.06],
	[0.44, 0.76],
	[0.7, 0.64],
	[0.52, 0.38],
	[0.16, 0.82],
	[0.9, 0.6],
	[0.16, 0.1],
	[0.6, 0.2],
	[0.5, 0.2]
]

labels = []
for i in range(1, 10):
	labels.append("P{}".format(i))

print(labels)