import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

#from pandas import display

risuto = np.array([8, 777, 6, 4, 114514, 7, 21, 42, 888])

# Cari dan tampilkan nilai median, minimum, standar deviasi, variance, dan jumlah dari
# array np_risuto!
print(risuto)

print("Median: {}".format(np.median(risuto)))
print("Minimum: {}".format(np.amin(risuto)))
print("Standar deviasi: {}".format(np.std(risuto)))
print("Variance: {}".format(np.var(risuto)))
print("Jumlah: {}".format(np.sum(risuto)))

a = np.array([1, 6, 21])
b = np.array([0.1, 0.2, 0.3])
# Carilah dan print array hasil elementwise substraction dan elementwise division dari
# array a dan b di atas!

print("\n\n2.Elementwise subtraction: {}".format(a + b))
print("Elementwise division: {}".format(a / b))

# https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.dropna.html
#Buat dan tampilkan DataFrame baru yang dihasilkan dari dataset drinks
#dengan mendrop row yang berisi missing value (minimal satu). hint: bisa
#dilakukan dengan satu baris
#- Tampilkan shape, count, dan isnull().sum() dari DataFrame baru tersebut
print("==========================================================")
csv = pd.read_csv('drinks.csv')
newCsv = csv.dropna()
print(csv.dropna())


print("Shape dataset: {}\n\n".format(newCsv.shape))
print("Count: {}\n\n".format(newCsv.count()))
print("Isnull: {}\n\n".format(newCsv.isnull().sum()))

print("==========================================================")
# 4
# Tampilkan baris-baris pada dataset drinks dimana beer_servings nya 0 dan continent
# nya AS.
hasilQuery = csv.query('beer_servings == 0 and continent == "AS"')
print(hasilQuery)

print("==========================================================")
# 5
# Tampilkan barplot yang menampilkan beer_servings dari 5 negara dengan nilai
# beer_servings terbesar, diurutkan dari yang paling besar (kiri = besar).

newCsvSorted = newCsv.sort_values('beer_servings', ascending=False)


sns.barplot(x="country", y="beer_servings", data=newCsvSorted.head())
#plt.show()

# Soal 6
# Tampilkan histogram plot wine servings dari negara di EU, dari dataset drinks.

euServings = csv.query('continent == "EU"')#['wine_servings']

print(euServings)
#histogram = euServings['wine_servings'].hist(bins=20)

euServings.hist(bins=30)

plt.show()
