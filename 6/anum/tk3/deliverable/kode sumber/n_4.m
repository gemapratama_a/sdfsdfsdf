pkg load symbolic;
syms f(x4, y4, z4);
warning("off", "all");

f(x4, y4, z4) = ...
    (-3) ...
    + ((1 / (x4^2 + y4^2 + z4^2)^6) - (2 / (x4^2 + y4^2 + z4^2)^3)) ...
    + ((1 / (x4^2 + y4^2 + (1 - z4)^2)^6) - (2 / (x4^2 + y4^2 + (1 - z4)^2)^3)) ...
    + ((1 / ((0.61237 - x4)^2 + (0.61237 - y4)^2 + (0.5 - z4)^2)^6) - (2 / ((0.61237 - x4)^2 + (0.61237 - y4)^2 + (0.5 - z4)^2)^3));
roots = [-0.5; -0.5; -0.5];
jaco_f = jacobian(f);
hess_f = hessian(f);

jaco_subs = double(subs(jaco_f, [x4, y4, z4], roots.'));
i = 0;
while (norm(jaco_subs) > 1e-4) & (i < 25)
    jaco_subs = double(subs(jaco_f, [x4, y4, z4], roots.'));
    hess_subs = double(subs(hess_f, [x4, y4, z4], roots.'));
    v = double(hess_subs \ -jaco_subs.');
    roots = roots + v;
    disp(jaco_subs);
    i = i + 1;
end

disp("");
determinant = det(hess_subs)
fxx = hess_subs(1, 1)
roots
energy = double(subs(f, [x4, y4, z4], roots.'))
