pkg load symbolic;
syms f(x5, y5, z5);
warning("off", "all");

f(x5, y5, z5) = ...
    (-4.0766) ...
    + ((1 / (x5^2 + y5^2 + z5^2)^6) - (2 / (x5^2 + y5^2 + z5^2)^3)) ...
    + ((1 / (x5^2 + y5^2 + (1 - z5)^2)^6) - (2 / (x5^2 + y5^2 + (1 - z5)^2)^3)) ...
    + ((1 / ((0.61237 - x5)^2 + (0.61237 - y5)^2 + (0.5 - z5)^2)^6) - (2 / ((0.61237 - x5)^2 + (0.61237 - y5)^2 + (0.5 - z5)^2)^3)) ...
    + ((1 / ((-0.35247 - x5)^2 + (-0.35247 - y5)^2 + (-0.86336 - z5)^2)^6) - (2 / ((-0.35247 - x5)^2 + (-0.35247 - y5)^2 + (-0.86336 - z5)^2)^3));
roots = [-0.5; 1; -2];
jaco_f = jacobian(f);
hess_f = hessian(f);

jaco_subs = double(subs(jaco_f, [x5, y5, z5], roots.'));
i = 0;
while (norm(jaco_subs) > 1e-4) & (i < 25)
    jaco_subs = double(subs(jaco_f, [x5, y5, z5], roots.'));
    hess_subs = double(subs(hess_f, [x5, y5, z5], roots.'));
    v = double(hess_subs \ -jaco_subs.');
    roots = roots + v;
    disp(jaco_subs);
    i = i + 1;
end

disp("");
determinant = det(hess_subs)
fxx = hess_subs(1, 1)
roots
energy = double(subs(f, [x5, y5, z5], roots.'))
