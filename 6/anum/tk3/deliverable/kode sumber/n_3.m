pkg load symbolic;
syms f(x3, y3, z3);
warning("off", "all");

f(x3, y3, z3) = ...
    (-1) ...
    + ((1 / (x3^2 + y3^2 + z3^2)^6) - (2 / (x3^2 + y3^2 + z3^2)^3)) ...
    + ((1 / (x3^2 + y3^2 + (1 - z3)^2)^6) - (2 / (x3^2 + y3^2 + (1 - z3)^2)^3));
roots = [0.5; 0.5; 0.5];
jaco_f = jacobian(f);
hess_f = hessian(f);

jaco_subs = double(subs(jaco_f, [x3, y3, z3], roots.'));
while norm(jaco_subs) > 1e-4
    jaco_subs = double(subs(jaco_f, [x3, y3, z3], roots.'));
    hess_subs = double(subs(hess_f, [x3, y3, z3], roots.'));
    v = double(hess_subs \ -jaco_subs.');
    roots = roots + v;
    disp(jaco_subs);
end

disp("");
determinant = det(hess_subs)
fxx = hess_subs(1, 1)
roots
energy = double(subs(f, [x3, y3, z3], roots.'))
