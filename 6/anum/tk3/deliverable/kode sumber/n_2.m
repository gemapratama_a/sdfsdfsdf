pkg load symbolic;
syms f(z2);
warning("off", "all");

f(z2) = ...
    ((1 / z2^12) - (2 / z2^6));
roots = [0.5];
jaco_f = jacobian(f);
hess_f = hessian(f);

jaco_subs = double(subs(jaco_f, [z2], roots.'));
while norm(jaco_subs) > 1e-4
    jaco_subs = double(subs(jaco_f, [z2], roots.'));
    hess_subs = double(subs(hess_f, [z2], roots.'));
    v = double(hess_subs \ -jaco_subs.');
    roots = roots + v;
    disp(jaco_subs);
end

disp("");
determinant = det(hess_subs)
fxx = hess_subs(1, 1)
roots
energy = double(subs(f, [z2], roots.'))
