from pylab import plot,show
from numpy import array,linspace,sqrt,sin
from numpy.linalg import norm

def f2(x):
    a = 2 - 3*x
    b = 4*x*x + 6*x
    return a / b

def f2turunan(x):
    a = 3*x*x - 4*x - 3
    b = x*x*((2*x + 3)**2)
    return a/b 

def f(x):
	return 2 / (4*x*x + 6*x + 3)

def fprime(x):
	return (-2 * (8*x + 6)) / ((4*x*x + 6*x + 3)**2)

#print("aaa")
#print(f(0.3549879733))


def fdoubleprime(x):
    a = 6 * (2*x*x*x -4*x*x - 6*x - 3)
    b = x*x*x * (2*x + 3)**3

    return a/b * (-1)

def asli(x):
    return 8*x*x*x + 12*x*x + 6*x - 4

def asliprime(x):
    return 24*x*x + 24*x + 6


def next(x):
    return asli(x) / asliprime(x)


a = (5**(1/3) - 1) / 2


print("FPRIMEA", fprime(a))    
print(0.3 - next(0.3))
print(0.3588541666666667 - next(0.3588541666666667))
print(0.3550053511685152 - next(0.3550053511685152))
print(0.3549879736915474 - next(0.3549879736915474))
print(0.3549879733383485 - next(0.3549879733383485))

print("============================================")

print(0.3 - next(0.3) - a)
print(0.3588541666666667 - next(0.3588541666666667) - a)
print(0.3550053511685152 - next(0.3550053511685152) - a)
print(0.3549879736915474 - next(0.3549879736915474) - a)
print(0.3549879733383485 - next(0.3549879733383485) - a)

#print(0.3549879733383485) # stop
#print(0.3549879733)

atas = f(0.3549879733) * fdoubleprime(0.3549879733)
bawah =  fprime(0.3549879733) * fprime(0.3549879733)

#print( atas/bawah )

"""
print(f(0.3))
print(f(0.38759689922480617))
print(f(0.33746691948125696))
print(f(0.36494104753075923))
print(f(0.3495052830908155))
print(f(0.35806054905504114))
print(f(0.35806054905504114))

print("WOOOOOOOOO")
print(f(0.3549879733))
"""



n = 0.35806054905504114
n = 0.3549879733

"""
print("x: {}, f(x): {}".format(n, f(n)))
print("x: {}, f(x): {}".format(0.3532824438935706, f(0.3532824438935706)))
print("x: {}, f(x): {}".format( 0.35593974242186915, f( 0.35593974242186915)))
print("x: {}, f(x): {}".format(0.35445841347412527, f(0.35445841347412527)))
"""