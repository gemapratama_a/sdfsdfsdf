import math 

e = math.e
pi = math.pi

def f(x):		# Diubah sesuai keinginan 
	#return x*x*x + 2*x*x + 10*x - 20
	return x**x - 2
	#return 10 * e**(-x) * math.sin(2*pi*x) - 2
	#return x*x*x*x - 6*x*x*x + 12*x*x - 10*x + 3
	#return x*x - math.cos(pi*x)

# a dan b adalah ujung-ujung interval [a, b]
def bisection(a, b, tol): 
	if f(a) * f(b) >= 0: 
		print("Nilai a and b yang dipilih tidak tepat...\n") 
		return
	n = 1
	c = a 
	
	while b -a >= tol: 
		#print("n =", n)
		c = (a + b) / 2
		n += 1
		if f(c) == 0.0: 
			break

		if f(c) * f(a) >= 0: 
			a = c 
		else: 
			b = c 
	print("Toleransi: ", tol)
	print("Akar yang ditemukan: ", "%.15f"%c)
	print("Iterasi {} kali".format(n - 1))

if __name__ == '__main__':
	bisection(1.5, 1.6, 1e-12)