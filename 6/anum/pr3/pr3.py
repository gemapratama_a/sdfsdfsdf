import numpy as np
from scipy import linalg

M = np.array([
	[-2,2,-3],
	[-34,19,-20],
	[-8,4,0]
])

print(np.linalg.eigvals(M))

print(linalg.schur(M))