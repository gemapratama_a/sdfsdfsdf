function Fhasil = F(x,y)
  Fii = zeros(2,1)
  Fii(1) = x*x*x*y*y - 2*x*y + x*x - y*y - 5
  Fii(2) = x*x*exp(-y) - 2*x*sin(y) - 3
  Fhasil = Fii
  
 endfunction