function Jhasil = J(x,y)
  Jii = zeros(2,2);
  Jii(1,1) = 3*x*x*y*y - 2*y + 2*x
  Jii(1,2) = 2*x*x*x*y - 2*x - 2*y
  Jii(2,1)= 2*x*exp(-y) - 2*sin(y)
  Jii(2,2) = -x*x*exp(-y) - 2*x*cos(y)
  Jhasil = Jii
 endfunction