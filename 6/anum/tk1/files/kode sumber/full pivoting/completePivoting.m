function [X, residualError] = completePivoting(A, b)
% https://www.youtube.com/watch?time_continue=725&v=W5f5ezf0YyU&feature=emb_title
% http://www.math.wsu.edu/faculty/genz/448/lessons/l204w.pdf
% https://www.mathworks.com/matlabcentral/fileexchange/25758-gaussian-elimination-using-complete-pivoting?focused=5141288&tab=function

Asave = A;

[n, n] = size(A);
p = 1:n; 
q = 1:n;
for k = 1:n-1
    [maxc, rowindices] = max( abs(A(k:n, k:n)) );
    [maxm, colindex] = max(maxc);
    row = rowindices(colindex)+k-1; col = colindex+k-1;
    A( [k, row], : ) = A( [row, k], : );
    A( :, [k, col] ) = A( :, [col, k] );
    b( [k, row], : ) = b( [row, k], : );
    p( [k, row] ) = p( [row, k] ); q( [k, col] ) = q( [col, k] );
    if A(k,k) == 0
      break
    end
    A(k+1:n,k) = A(k+1:n,k)/A(k,k);
    i = k+1:n;
    A(i,i) = A(i,i) - A(i,k) * A(k,i);
end

L = tril(A,-1) + eye(n);
U = triu(A);
P = eye(n);
P = P(p,:);
Q = eye(n);
Q = Q(:,q);

% Inisialisasi solusi kosong (vektor kolom yang entrinya nol semua)
X = zeros(n, 1);
Y = zeros(n, 1);

% Forward substitution LY = B
Y(1) = b(1)/L(1,1);
for i = 2 : n 
  Y(i) = ( b(i)-L(i,1:i-1)*Y(1:i-1) ) / L(i,i);
end

% Back substitution UX = Y
for i = n : -1 : 1 
  X(i) = ( Y(i) - U(i,i+1:n) * X(i+1:n) ) /U(i,i);
end

solusi = inv(P) * X  % <-- tetep ga ngurut
residualError = b - P*Asave*Q*X
%residualError = b - Asave*X

end
;