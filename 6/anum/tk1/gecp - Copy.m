function [L, U, P, Q] = gecp(A, b)
%GECP calculate Gauss elimination with complete pivoting
%
% (G)aussian (E)limination (C)omplete (P)ivoting
% Input : A nxn matrix
% Output
% L = Lower triangular matrix with ones as diagonals 
% U = Upper triangular matrix
% P and Q permutations matrices so that P*A*Q = L*U 
% 
% See also LU
% https://www.youtube.com/watch?time_continue=725&v=W5f5ezf0YyU&feature=emb_title
% http://www.math.wsu.edu/faculty/genz/448/lessons/l204w.pdf
% https://www.mathworks.com/matlabcentral/fileexchange/25758-gaussian-elimination-using-complete-pivoting?focused=5141288&tab=function
% written by : Cheilakos Nick 
Asave = A;
%A = [A b];
[n, n] = size(A);
p = 1:n; 
q = 1:n;
for k = 1:n-1
    [maxc, rowindices] = max( abs(A(k:n, k:n)) );
    [maxm, colindex] = max(maxc);
    row = rowindices(colindex)+k-1; col = colindex+k-1;
    A( [k, row], : ) = A( [row, k], : );
    A( :, [k, col] ) = A( :, [col, k] );
    b( [k, row], : ) = b( [row, k], : );
    p( [k, row] ) = p( [row, k] ); q( [k, col] ) = q( [col, k] );
    if A(k,k) == 0
      break
    end
    A(k+1:n,k) = A(k+1:n,k)/A(k,k);
    i = k+1:n;
    A(i,i) = A(i,i) - A(i,k) * A(k,i);
end






L = tril(A,-1) + eye(n);
U = triu(A);
P = eye(n);
P = P(p,:);
Q = eye(n);
Q = Q(:,q);

printf("A after\n");
disp(A);

printf("b after\n");
disp(b);

printf("P:\n");
disp(P);

printf("Q:\n");
disp(Q);

printf("L:\n");
disp(L);

printf("U:\n");
disp(U);

printf("PAQ\n");
disp(P*Asave*Q);

printf("LU\n");
disp(L*U);



X = zeros(n,1);
Y = zeros(n,1);
% Back substitution LY = B
for i = n : -1 : 1 
  Y(i) = ( b(i) - L(i,i+1:n)*Y(i+1:n) )/L(i,i);
  %Y(i) = ( b(i, end)-
end

printf("Y (solution part 1)\n");
disp(Y);

% Back substitution UX = Y
for i = n : -1 : 1 
  X(i) = ( Y(i) - U(i,i+1:n)*X(i+1:n) )/U(i,i);
end

printf("X (solution part 2)\n");
disp(X);















end