function [x] = normaleqnsolver(A, b)
  % Solving least square problems through normal equation
  % :param A: matrix of size m x n (with m > n)
  % :param b: vector of size m x 1
  % :return x: vector of size n x 1
  
  % Compute A transposed times A
  ATA = A' * A;
  
  % Compute A transposed times b
  ATb = A' * b;
  
  % Compute x
  x = ATA \ ATb;
endfunction
