import numpy as np


A = np.array([
    [1,-1,4],
    [1,4,-2],
    [1,4,2],
    [1,-1,0]
])

v = np.array([
    [-1],
    [1],
    [1],
    [1],
])

vvt = np.matmul(v, v.T)
stengahvvt = np.dot(0.5, vvt)

stengahvvtA = np.matmul(stengahvvt, A)

AkurangstengahvvtA = np.subtract(A, stengahvvtA)

print(AkurangstengahvvtA)