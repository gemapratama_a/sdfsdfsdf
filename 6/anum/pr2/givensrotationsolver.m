function [x, R, qb] = givensrotationsolver(A, b)
  % Solving least square problems through Givens Rotation transformation
  % :param A: matrix of size m x n (with m > n)
  % :param b: vector of size m x 1
  % :return x: vector which minimizes the norm-2 of b - Ax
  % :return R: reduced matrix of A
  % :return qb: reduced vector of b
  
  [m n] = size(A);
  Ab = [A b];
  for j = 1 : n+1
    for i = j+1 : m
      % Compute the c and s in the rotation matrix
      na = sqrt(Ab(j,j)^2 + Ab(i,j)^2);
      c = Ab(j,j)/na;
      s = Ab(i,j)/na;
      
      % Compute the result from applying the rotation matrix 
      % to the j-th rows and the i-th row
      rj = Ab(j, j:n+1); % matriks baris 1 x n+1
      ri = Ab(i, j:n+1); % matriks baris 1 x n+1
      
      %rj = c * rj - s * ri
      %ri = c * ri + s * rj 
      
      Ab(j, j:n+1) = c*rj + s*ri;
      Ab(i, j:n+1) = c*ri - s*rj;
      
    endfor
  endfor
  
  R = triu(Ab(1:n, 1:n));
  qb = Ab(:, n+1);
  x = R \ qb(1:n);
  printf("R:\n")
  disp(R)
endfunction
