%{
function [x, R, qb] = householdersolver(A, b)
  % Solving least square problems through Householder transformation
  % :param A: matrix of size m x n (with m > n)
  % :param b: vector of size m x 1
  % :return x: vector of size n x 1
  % :return R: reduced matrix of A
  % :return qb: reduced vector of b
  
  [m n] = size(A);
  Ab = [A b];
  for i = 1 : n
    % Le a be the i-th column of A
    a = Ab(i:m, i);
    
    % Create vector v; use sign(a(1)) to avoid cancellation
    ae = zeros(m-i+1, 1);
    ae(1) = norm(a);
    p = sign(a(1));
    if p == 1
      v = a - (p * ae);
    elseif p == -1
      v = a + (p * ae);
    endif
    
    printf("PAS I=")
    disp(i)
    
    printf("Vnya=\n")
    disp(v)
    
    disp(v*v')
    disp(0.5*v*v')
    disp(0.5*v*v'*Ab)
    % Compute H * u where u is every column of A
    % We never compute H, but use vector v instead
    for j = 1 : n+1
      % Let u be the j-th column of A
      u = Ab(i:m, j);
      
      % Update the j-th column of A through
      % TODO : baris 32
    endfor
    
  endfor
 printf("VEKTOR:\n")
    disp(v);
  
  R = triu(Ab(1:n, 1:n));
  qb = Ab(:, n+1);
  x = R \ qb(1:n);
endfunction
%}



function [x, R, qb] = householdersolver(A, b)
  % Solving least square problems through Householder transformation
  % :param A: matrix of size m x n (with m > n)
  % :param b: vector of size m x 1
  % :return x: vector of size n x 1
  % :return R: reduced matrix of A
  % :return qb: reduced vector of b
  
  [m n] = size(A);
  Ab = [A b];
  for i = 1 : n
    % Le a be the i-th column of A
    a = Ab(i:m, i);   % yg mau dibikin v
    
    % Create vector v; use sign(a(1)) to avoid cancellation
    ae = zeros(m-i+1, 1);   % init vektor nol yg dimensi sama kek a
    ae(1) = norm(a);        % paling atas dibikin jd norm
    p = sign(a(1));
    if p == 1               % biar ga catastrophic cancellation
      v = a - (p * ae);
    elseif p == -1
      v = a + (p * ae);
    endif
    
    
    % vektor v dah jadi
    
    % Compute H * u where u is every column of A
    % We never compute H, but use vector v instead
    for j = 1 : n+1   % +1 karena dah diaugment sama b
      % Let u be the j-th column of A
      u = Ab(i:m, j);   % dari A, iris dari row ke-i kolom ke-j
      
      % Update the j-th column of A
      disp(v)
      
      %Ab(i:m, j) = (Ab(i:m, j) - (0.5*v*v'*Ab(i:m, j)))
      %Ab(i, j) = (Ab(i, j) - (0.5*v*v'*Ab(i, j)))
    endfor
  endfor
  
  R = triu(Ab(1:n, 1:n));
  qb = Ab(:, n+1);
  x = R \ qb(1:n);
  disp(x)
endfunction
