import numpy as np
import math


def root(n):
    return math.sqrt(n)

A = np.array([[4, -2, 0], 
             [-2, 2, -3],
            [0, -3, 10]])


L = np.zeros_like(A)

# kolom 1
L[1-1,1-1] = root(A[1-1,1-1])
L[2-1,1-1] = (A[2-1,1-1]) / L[1-1,1-1]
L[3-1,1-1] = (A[3-1,1-1]) / L[1-1,1-1]

# kolom 2
L[2-1,2-1] = root(A[2-1,2-1] - (L[2-1,1-1])**2 )

print(A[2-1,2-1])
print((L[2-1,1-1])**2) 

print(np.linalg.eigvals(np.dot(-1, A)))

#L[3-1,2-1] = (A[3-1,2-1] - (L[3-1,1-1] * L[2-1,1-1])) / L[2-1,2-1]

# kolom 3
#L[3-1,3-1] = root(A[3-1,3-1] - ( (L[3-1, 1-1])**2 + (L[3-1, 2-1])**2) )

#print(L[1-1,1-1])

#print(L[2-1,1-1])
#print(L[3-1,1-1])
#print(L[2-1,2-1])
#print(L[3-1,2-1])
#print(L[3-1,3-1])
















#print("L")
#print(L)
#print("\nLT")
#print(L.T)

def cholesky(A):
    L = np.zeros_like(A)
    n, m = np.shape(A)
    for j in range(n):
        for i in range(j, n):
            if i == j:
                sum = 0
                for k in range(j):
                    sum += L[i, k] ** 2
                L[i, j] = (A[i, j] - sum)**0.5
                #print("i == j")
                #print("L[{}, {}] = {}".format(i + 1, j + 1, L[i, j]))
            else:
                sum = 0
                for k in range(j):
                    sum += L[i, k] * L[j, k]
                L[i, j] = (A[i, j] - sum) / L[j, j]
                #print("i != j")
                #print("L[{}, {}] = {}".format(i + 1, j + 1, L[i, j]))
    
    return L
#G = cholesky(A)
#print(G)
#GT = G.T
A = np.array([[4, -2, 0], 
             [-2, 2, -3],
            [0, -3, 10]])

#print(np.linalg.cholesky(A))
#print(np.matmul(G, GT))



A2 = np.array([
    [3,-1,2],
    [4,1,0],
    [-3,2,1],
    [1,1,5],
    [-2,0,3]
])


b2 = np.array([
    [4],
    [5],
    [0],
    [7],
    [1]
])
"""
LSSA = np.array([
    [3,-1,2],
    [4,1,0],
    [-3,2,1],
    [1,1,5],
    [-2,0,3]
])

LSSB = np.array([[4],[5],[0],[7],[1]])
"""



"""

L = np.linalg.cholesky(A)

p,l,u = lu(A)

print("P:\n{}\nL:\n{}\nU:\n{}\n".format(p,l,u))

LT = L.T
print("L:\n{}\n".format(L))

LT = L.T
print("LT:\n{}\n".format(LT))

LLT = np.matmul(L, LT)
print("LLT:\n{}\n".format(LLT))












A
3  -1   2
4   1   0
-3  2   1
1   1   5
-2  0   5

b

4
5
0
7
1

(1)
A topi                      z
3  -1   2   4               3
4   1   0   5               4
-3  2   1   0               -3
1   1   5   7               1
-2  0   5   1               -2

(2)
v = sign(z1) ||z|| e + z
  = sign(3) akar39 1 +  3
                   0    4
                   0    -3
                   0    1
                   0    2
  = 3 + akar39
  = 4
  = -3
  = 1
  = 2

||v|| = 














"""