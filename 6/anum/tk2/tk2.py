import math

pi = math.pi
def f1(x, y):
	return x*x + 3*x*y*y + 8*y - 100

def f2(x, y):
	return math.cos(pi*x) - x*y*y + 35


a = -9.34
b = -0.15

print(f1(a,b))
print(f2(a,b))