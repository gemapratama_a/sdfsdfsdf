function x = fixed_point(guess, tol)
    i = 1;
    max_iter = 100;
    x = guess;
    while i <= max_iter
        next_x = g(x);
        if abs(next_x - x) < tol
            x = next_x;
            break;
        end
        i = i + 1;
        x = next_x;
    end
end
