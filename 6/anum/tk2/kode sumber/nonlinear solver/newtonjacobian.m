function approx = newtonjacobian(awal)
  x = awal  % tebakan awal (x0, y0)
  F(1) = x(1)^2 + 3*x(1)*x(2)^2 + 8*x(2) - 100
  F(2) = cos(pi*x(1)) - x(1)*x(2)^2 + 35 
  J(1,1) = 2*x(1) + 3*x(2)^2
  J(1,2) = 6*x(1)*x(2) + 8
  J(2,1) = -pi*sin(pi*x(1)) - x(2)^2
  J(2,2) = -2*x(1)*x(2)

  d = J\-F’
  x=x+d;
  approx = x;
  end
