import math
pi = math.pi
def f1(x, y):
	return x*x + 3*x*y*y + 8*y - 100

def f2(x, y):
	return math.cos(pi*x) - x*y*y + 35

a = 4	# Solusi yang didapat dari octave
b = -3	

print(f1(a,b))
print(f2(a,b))
