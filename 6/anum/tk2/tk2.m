
%F(1) = x(1)^3 * x(2)^2 - 2*x(1)*x(2) + x(1)^2-x(2)^2 - 5;
%F(2) = x(1)^2 * exp(-x(2)) - 2*x(1)*sin(x(2)) - 3

%J(1,1) = 3*x(1)^2*x(2)^2 - 2*x(2)+2*x(1)
%J(1,2) = 2*x(1)^3*x(2)-2*x(1)-2*x(2)
%J(2,1) = 2*x(1)*exp(-x(2))-2*sin(x(2))
%J(2,2) = -x(1)^2*exp(-x(2))-2*x(1)*cos(x(2))
function approx = tk2(awal)
  x = awal  % tebakan awal (x0, y0)
  F(1) = x(1)^2 + 3*x(1)*x(2)^2 + 8*x(2) - 100
  F(2) = cos(pi*x(1)) - x(1)*x(2)^2 + 35 
  J(1,1) = 2*x(1) + 3*x(2)^2
  J(1,2) = 6*x(1)*x(2) + 8
  J(2,1) = -pi*sin(pi*x(1)) - x(2)^2
  J(2,2) = -2*x(1)*x(2)

  d = J\-F'
  %display(d);
  x=x+d;
  display(x);
  approx = x;
  %while abs(d) > 0.01:
end