"""


def gprime(t):
	return 6400*t*t*t + 480000*t*t + 7998408*t - 40004


print(gprime(0.00499))
print(gprime(-25.000051))
print(gprime(-50.00494))



def find_gcd(x, y): 
    while(y): 
        x, y = y, x % y 
  
    return x 
      
      
l = [1600,
    120000,
   1999602,
     10001] 
        
num1=l[0] 
num2=l[1] 
gcd=find_gcd(num1,num2) 
  
for i in range(2,len(l)): 
    gcd=find_gcd(gcd,l[i]) 
      
print(gcd)


  
# Function to find the root 
def newtonRaphson( x ): 
    h = func(x) / derivFunc(x) 
    for _ in range(3):
        h = func(x)/derivFunc(x) 
          
        # x(i+1) = x(i) - f(x) / f'(x) 
        x = x - h 
      
    print("The value of the root is : ", 
                             "%.15f"% x) 
  
# Driver program to test above 
x0 = 0 # Initial values assumed 
newtonRaphson(x0) 

x1 = 0.005001495297564
fx1 = func(0.005001495297564)
fprimex1 = derivFunc(0.005001495297564)

print(x1 - (fx1/fprimex1))


def func(t): 
    return 6400*t*t*t + 480000*t*t + 7998408*t - 40004
  
# Derivative of the above function  
# which is 3*x^x - 2*x 
def derivFunc(t): 
    return 19200*t*t + 960000*t + 7998408

x2 = 0.0049999949021796285

fx2 = func(x2)
fprimex2 = derivFunc(x2)

print(x2 - (fx2/fprimex2))
"""

x1 = 0.0099999898
y1 = 0.0000010196

def f(x, y):
	return (100*(y-x*x) )**2 + ((x - 1)**2)

print(f(x1,  y1))