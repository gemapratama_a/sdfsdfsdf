function r = interpolate(x, y)
    [_ n] = size(x);
    n = n - 1;
    r = zeros(4, n);

    h = zeros(1, n);
    for i = 1:n
        h(i) = x(i+1) - x(i);
    end

    miu = zeros(1, n-1);
    for i = 1:n-1
        miu(i) = h(i) / (h(i) + h(i+1));
    end

    lambda = zeros(1, n-2);
    for i = 1:n-2
        lambda(i) = 1 - miu(i);
    end

    gamma = zeros(n-1, 1);
    for i = 1:n-1
        gamma(i) = 6 * ((y(i+2) - y(i+1)) / h(i+1) - (y(i+1) - y(i)) / h(i)) / (h(i) + h(i+1));
    end

    a = zeros(n-1, n-1);
    for i = 1:n-1
        a(i, i) = 2;
    end
    for i = 1:n-2
        a(i, i+1) = lambda(i);
    end
    for i = 2:n-1
        a(i, i-1) = miu(i);
    end
    sigma_to_n = a \ gamma;
    sigma = zeros(1, n+2);
    for i = 2:n
        sigma(i) = sigma_to_n(i-1);
    end

    for i = 1:n
        r(1, i) = (sigma(i+1) - sigma(i)) / (6 * h(i));
        r(2, i) = sigma(i) / 2;
        r(3, i) = (y(i+1) - y(i)) / h(i) - h(i) * (2 * sigma(i) + sigma(i+1)) / 6;
        r(4, i) = y(i);
    end
end
