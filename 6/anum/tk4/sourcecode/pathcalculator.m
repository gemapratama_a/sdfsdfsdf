function pathcalculator(n)
    plot([-1 1], [0 0], 'k', [0 0], [-1 1], 'k');
    hold on;
    xs = zeros(1, n);
    ys = zeros(1, n);
    for i = 1:n
        [x y] = ginput(1);
        plot(x, y, 'bo');
        xs(i) = x;
        ys(i) = y;
    end
    t = 0:n-1;
    x_ip = interpolate(t, xs);
    y_ip = interpolate(t, ys);
    last_integration = -1;
    section = (n - 1) * 10;
    while true
        integration = 0;
        h = (n - 1) / section;
        for i = 0:section-1
            l = h * i;
            r = h * (i + 1);
            m = (l + r) / 2;
            integration = integration + h * (sqrt(df(l, t, x_ip)^2 + df(l, t, y_ip)^2) + 4 * sqrt(df(m, t, x_ip)^2 + df(m, t, y_ip)^2) + sqrt(df(r, t, x_ip)^2 + df(r, t, y_ip)^2)) / 6;
        end
        if round(integration * 1000) == round(last_integration * 1000)
            break;
        end
        last_integration = integration;
    end
    t_plot = 0:h:n-1;
    [_ t_size] = size(t_plot);
    x_plot = zeros(1, t_size);
    y_plot = zeros(1, t_size);
    for i = 1:t_size
        x_plot(i) = f(t_plot(i), t, x_ip);
        y_plot(i) = f(t_plot(i), t, y_ip);
    end
    plot(x_plot, y_plot, 'r');
    text(-0.9, 0.9, ['Total length = ' num2str(integration)], 'FontSize', 16);
end
