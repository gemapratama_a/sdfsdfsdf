function r = f(p, t, ip)
    r = 0;
    [_ n] = size(t);
    n = n - 1;
    for i = 1:n
        if (t(i) <= p) && (p <= t(i+1))
            r = ip(1, i) * (p - t(i))^3 + ip(2, i) * (p - t(i))^2 + ip(3, i) * (p - t(i)) + ip(4, i);
            break;
        end
    end
end
