function r = df(p, t, ip)
    r = 0;
    [_ n] = size(t);
    n = n - 1;
    for i = 1:n
        if (t(i) <= p) && (p <= t(i+1))
            r = 3 * ip(1, i) * (p - t(i))^2 + 2 * ip(2, i) * (p - t(i)) + ip(3, i);
            break;
        end
    end
end
