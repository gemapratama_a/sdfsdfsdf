
def backward(x0, h):
	atas = 1/(x0) - 1/(x0 - h)
	bawah = h
	return atas/bawah

def central(x0, h):
	atas = 1/(x0 + h) - 1/(x0 - h)
	bawah = 2*h
	return atas/bawah

def f(x):
	return 1/x

def fprime(x):
	return -1 / (x*x)

x0 = 0.1
h = 0.05

print(f(0.15)-f(0.05))
print("CD")

"""
backwards = []
centrals = []
error_backwards = []
error_centrals = []

for h_vals in h:
	print("[BACKWARD] f'({}) w/ h={} = {}".format(x, h_vals, backward(x, h_vals)))
	backwards.append(backward(x, h_vals))

for h_vals in h:
	print("[CENTRAL] f'({}) w/ h={} = {}".format(x, h_vals, central(x, h_vals)))
	centrals.append(central(x, h_vals))

print("[ASLI] f'(0.1) = {}".format(fprime(x)))

for x in backwards:
	error_backwards.append(abs(x + 100))
	
for x in centrals:
	error_centrals.append(abs(x + 100))

print("back < central")
for i in range(len(error_backwards)):
	print(error_backwards[i] < error_centrals[i])

print("back > central")
for i in range(len(error_backwards)):
	print(error_backwards[i] > error_centrals[i])



a = 0
b = 4
def f(x):
	return 3*x*x + 1

def h(j):
	pangkat = j + 1
	return (b-a)/(2**pangkat)



def R(j, k, a, b):
	if j == 0 and k == 0:
		return (b-a)*(f(a)+f(b))/2
	elif j == 0 and k != 0:
		kiri = 0.5 * R(j - 1, k, a, b)
		kanan = 0
		for n in range(1, 2**(j-1) + 1):
			kanan += f(a + (2*k-1)*h(j))
		return kiri + (h(j) * kanan)
	elif j != 0 and k != 0:
		atas = 4**k * R(j, k-1, a, b) - R(j-1, k-1, a, b)
		bawah = 4**k - 1
		return atas/bawah


for i in range(4):
	for j in range(i):
		print("R{},{} = {}".format(i,j, R(i,j,0,4)))


"""