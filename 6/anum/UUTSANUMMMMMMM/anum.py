import numpy as np
from scipy.linalg import lu

A = np.matrix([
     [1, 2, -4, 3],
     [4, 10, -12, 20],
     [-2, -7, 3, -21],
	 [2,8,-5, 38]
    ])


B = np.matrix([
    [-28],
    [24],
    [4]
    ])

P,L,U = lu(A)

print(P)
print(L)
print(U)