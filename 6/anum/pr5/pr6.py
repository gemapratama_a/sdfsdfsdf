import math

def root(n):
	return n**0.5

def f(x):
	x2 = 27 - 18*root(3)
	x1 = 12*root(3) - (33/2)
	x0 = (5-3*root(3))/2
	return x*x*x2 + x*x1 + x0

pi = math.pi
#print(f(1/6))
#print(f(1/3))
#print(f(1/2))

#print(f(1/5))
#print(math.sin(math.pi / 5))




#1/2 + (pi*sqrt(3)/2)*(x-1/6) + (18sqrt(3)-1-3*pi*sqrt(3))*(x-1/6)^2 + (18*pi-216*sqrt(3)+216+18*pi*sqrt(3))*(x-1/6)^2 *(x-1/3) + (-486 -108*pi + 648*sqrt(3) - 54*pi*sqrt(3)) * (x-1/6)^2 * (x-1/3)^2 + (-2916 + 648*pi + 162*pi*sqrt(3))*(x-1/6)^2 * (x-1/3)^2 * (x-1/2)

z = [1/6, 1/6, 1/3, 1/3, 1/2, 1/2]

fz0 = fz1 = 1/2
fz2 = fz3 = 3**0.5 / 2
fz4 = fz5 = 1

fz1z2 = (fz2 - fz1) / (z[2] - z[1])

#print(fz1z2)
#print( 3 * (root(3) - 1) )

fz3z4 = (fz4 - fz3) / (z[4] - z[3])

#print(fz3z4)
#print(6 - 3*root(3))

fz0z1 = pi*root(3)/2
fz2z3 = pi/2
fz4z5 = 0

fz0z1z2 = (fz1z2 - fz0z1 ) / (z[2] - z[0])

#print(fz0z1z2)
#print(18*(root(3)-1) - 3*pi*root(3))

fz1z2z3 = ( fz2z3 - fz1z2 ) / (z[3] - z[1])
fz2z3z4 = ( fz3z4 - fz2z3 ) / (z[4] - z[2])
fz3z4z5 = ( fz4z5 - fz3z4 ) / (z[5] - z[3])

#print(fz1z2z3)
#print(3*pi - 18*(root(3) - 1))

#print(fz2z3z4)
#print(36 - 18*root(3) - 3*pi)

#print(fz3z4z5)
#print(18*root(3) - 36)



fz0123 = (fz1z2z3 - fz0z1z2 ) / (z[3] - z[0])
#print(fz0123)
#print(18*pi - 216*root(3) + 216 + 18*pi*root(3))

fz1234 = ( fz2z3z4 - fz1z2z3 ) / (z[4] - z[1])
#print(fz1234)
#print(54 - 18*pi)

fz2345 = ( fz3z4z5 - fz2z3z4 ) / (z[5] - z[2])
#print(fz2345)
#print(216*root(3) - 432 + 18*pi)

fz01234 = (fz1234 - fz0123) / (z[4] - z[0])
#print(fz01234)
#print(-486 - 108*pi + 648*root(3) - 54*pi*root(3))

fz12345 = (fz2345 - fz1234) / (z[5] - z[1])
#print(fz12345)
#print(648*root(3) - 1458 + 108*pi)

fz012345 = (fz12345 - fz01234 ) / (z[5] - z[0])
#print(fz012345)
#print(-2916+648*pi + 162*pi*root(3))


print(fz0)
print(fz0z1)
print(fz0z1z2)
print(fz0123)
print(fz01234)
print(fz012345)


# HASIL DESMOS DAH BENER
"""
y=\frac{1}{2}+2.7206990463513265\left(x-\frac{1}{6}\right)+\left(-3.1472797418681697\right)\cdot\left(x-\frac{1}{6}\right)^{2}+\left(-3.6291410016134424\right)\cdot\left(x-\frac{1}{6}\right)^{2}\cdot\left(x-\frac{1}{3}\right)+\left(3.2414197109915\right)\cdot\left(x-\frac{1}{6}\right)^{2}\cdot\left(x-\frac{1}{3}\right)^{2}+\left(1.2585305440157784\right)\cdot\left(x-\frac{1}{6}\right)^{2}\cdot\left(x-\frac{1}{3}\right)^{2}\cdot\left(x-\frac{1}{6}\right)


A = [
	1/(6^3) 1/(6^2) 1/6 1 0 0 0 0;
	1/(3^3) 1/(3^2) 1/3 1 0 0 0 0;
	0 0 0 0 1/(3^3) 1/(3^2) 1/3 1;
	0 0 0 0 1/(2^3) 1/(2^2) 1/2 1;
	1/3 2/3 1 0 -1/3 -2/3 -1 0;
	1 1 0 0 -1 -1 0 0;
	1 2 0 0 0 0 0 0; 
	0 0 0 0 3 2 0 0

]

b=[1/2; sqrt(3)/2; sqrt(3)/2;1;0;0;0;0]



  -12.53074
    6.26537
    1.50000
    0.13397

	   12.53074
  -18.79612
    9.85383
   -0.79423

"""