import math
import numpy

# dy/dt = f(t, y)
def f(t, y):
	return y * math.cos(t)

def getk1(tn, yn):
	return f(tn, yn)

def getk2(tn, yn, h):
	k1 = getk1(tn, yn)
	tn = tn + (h/2)
	yn = yn + (h * k1 /2)

	return f(tn, yn)

def getk3(tn, yn ,h):
	k1 = getk1(tn, yn)
	k2 = getk2(tn, yn, h)
	tn = tn + h
	yn = yn - h*k1 + 2*h*k2
	return f(tn, yn)

def ynext(ycurrent, k1, k2, k3, h):
	return ycurrent + h*k1/6 + 2/3*h*k2 + h*k3/6


t0 = 0
y0 = 1
h = 0.1