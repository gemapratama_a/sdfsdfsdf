import math
import numpy

# dy/dt = f(t, y)
def f(t, y):
	return y * math.cos(t)

def getk1(tn, yn):
	return f(tn, yn)

def getk2(tn, yn, h):
	k1 = getk1(tn, yn)
	tn = tn + h
	yn = yn + h * k1

	return f(tn, yn)

def ynext(ycurrent, k1, k2, h):
	return ycurrent + h*(k1 + k2)/2

############
# f(t, y) = y cos t
# t0 = 0
# y0 = 1

t0 = 0
y0 = 1
h = 0.1