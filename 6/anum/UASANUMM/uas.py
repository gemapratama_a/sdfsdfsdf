def f(x):
	return x**5

def forward(x0, h):
	return (f(x0 + h) - f(x0)) / h


def central(x0, h):
	return ( f(x0 + h) - f(x0 - h) )/ (2*h)

H = [1/2, 1/4, 1/8, 1/16]

for h in H:
	print("[forward] [h={}] [x0=1] f'(1) = {}".format(h, forward(1, h)))
	print("[central] [h={}] [x0=1] f'(1) = {}".format(h, central(1, h)))
	print("error forward = {}".format(forward(1, h) - 5))
	print("error central = {}".format(central(1, h) - 5))
	

# Contoh: composite_trapezoidal(lambda x:x**3, 0, 1, 2) 
def composite_trapezoidal(f, a, b, n):
    h = float(b - a) / n
    s = 0.0
    s += f(a)/2.0
    for i in range(1, n):
        s += f(a + i*h)
    s += f(b)/2.0
    return s * h

# Rumus
# R(1,1) = R(1,0) + (R(1,0)-R(0,0))/3
# R(2,1) = R(2,0) + (R(2,0)-R(1,0))/3
# R(n,m) = 4^m * R(n, m-1) - R(n-1, m-1)