import numpy as np

A = np.array([[-2,1,-5], [4,1,3], [6,-3,-1]])
b = np.array([-28, 24, 4])

x = np.linalg.solve(A, b)

print(x)
