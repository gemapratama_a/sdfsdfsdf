import numpy as np
#from scipy.linalg import lu

L = np.matrix([
    [1,0,0],
    [-1/3, 1, 0],
    [2/3, -11/16, 1]
])

U = np.matrix([
    [6,-1,-3],
    [0,-16/3,0],
    [0,0,3]
])

A = np.matrix([
     [-2, 1, -5],
     [4, 1, 3],
     [6, -3, -1]
    ])

P = np.matrix([
    [0,0,1],
    [1,0,0],
    [0,1,0]
])


Q = np.matrix([
    [1,0,0],
    [0,0,1],
    [0,1,0]
])

LU = np.matmul(L,U)
PA = np.matmul(P,A)
PAQ= np.matmul(PA, Q)

Astar = PAQ
print(PAQ)
print(LU)

B = np.matrix([
    [4],
    [-28],
    [24]
    ])

Y = np.linalg.solve(L, B)
print("Y:\n{}\n".format(Y))

X = np.linalg.solve(U, Y)
print("X:\n{}\n".format(X))

"""


P,L,U = lu(A)

print("P:\n{}\n".format(P))
print("L:\n{}\n".format(L))
print("U:\n{}\n".format(U))

PA = np.matmul(P, A)
LU = np.matmul(L, U)

print("PA:\n{}\n".format(PA))
print("LU:\n{}\n".format(LU))

Astar = np.matmul(P, A)
Bstar = np.matmul(P, B)

Y = np.linalg.solve(L, Bstar)
print("A*:\n{}\n".format(Astar))
print("B*:\n{}\n".format(Bstar))
print("Y:\n{}\n".format(Y))

# SOLVE FOR UX = Y

X = np.linalg.solve(U, Y)
print("X:\n{}\n".format(X))


"""
