import numpy as np
from scipy.linalg import lu
"""
Computer Arithmetic [3.5 poin]
Misal Anda sedang dihadapkan pada sebuah mesin yang menggunakan sistem IEEE double precision dengan aturan round to nearesti. 
Anda kemudian diminta untuk menghitung hasil dari (9.3 - 9.1) - 0.2 menggunakan mesin tersebut dengan melengkapi tabel di bawah ini.

x       bit sign                                    mantissa                             exponen
9.3         0       0010 1001 1001 1001 1001 1001 1001 1001 1001 1001 1001 1001 1010        3 (000000 00011)
9.1         0       0010 0011 0011 0011 0011 0011 0011 0011 0011 0011 0011 0011 0011        3 (000000 00011)
0.2         0       1001 1001 1001 1001 1001 1001 1001 1001 1001 1001 1001 1001 1010        -3



9.3 mantissa = 0010 (1001)... --> bit ke53 = 1 --> +1 ke bit 52 --> 0010 (1001)11 1010
9.1 mantissa = 0010 (0011)... --> bit ke53 = 0 --> truncate     --> 0010 (0011)...
0.2 mantissa = 1001 (1001)... --> bit ke53 = 1 --> +1 ke bit 52 --> 1001 (1001)11 1010

Berikan detail operasi aritmatika yang terjadi di dalam mesin tersebut saat menghitung (9.3 - 9.1) - 0.2 hingga diperoleh hasil akhirnya.

1.0010 1001 1001 1001 1001 1001 1001 1001 1001 1001 1001 1001 1010 x 2^3 <----------- 9.3

0 | 1000 0000 010 |0010 1001 1001 1001 1001 1001 1001 1001 1001 1001 1001 1001 1010


1.0010 0011 0011 0011 0011 0011 0011 0011 0011 0011 0011 0011 0011 x 2^3 <----------- 9.1

------------------------------------------------------------- -
1.1001 1001 1001 1001 1001 1001 1001 1001 1001 1001 1001 1100 0000 x 2^-3<----------- 2.0000000000000107E-1

1.1001 1001 1001 1001 1001 1001 1001 1001 1001 1001 1001 1001 1010 x 2^-3<----------- 0.2
------------------------------------------------------------- -
1.0011 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 x 2^-50<---------- 1.0547118733938987E-15












0 0111 1111 100 | 1001 1001 1001 1001 1001 1001 1001 1001 1001 1001 1001 1100 0000
                                                                        11100 0000

0 0111 1111 100 | 1001 1001 1001 1001 1001 1001 1001 1001 1001 1001 1001 1100 0000
0 0111 1111 100 | 1001 1001 1001 1001 1001 1001 1001 1001 1001 1001 1001 1001 1010



0 0111 1001 101 | 0011 000000000000000000000000000000000000000000000000

0010 1001 1001
0010 0011 0011 0011 0011 0011 0011 0011 0011 0011 0011 0011 0011
---------------------------------------------------- -
xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx            0110

Condition Number [3 poin]
Misal Anda ingin melakukan komputasi sebuah fungsi,

f(x) = -1
      -------
       (x-3)^2

dengan x=2. Namun, saat Anda memasukkan nilai x tersebut ke mesin, mesin yang Anda gunakan justru menyimpan xt=1.9998 
sebagai masukan fungsi tersebut. Berapakah condition number dari f untuk x=2 yang berlaku pada mesin tersebut? Untuk 
nilai x yang seperti apa fungsi f akan semakin sensitif terhadap error dari masukan? Uraikan jawaban Anda.
"""
# f(x) asli
def f(x):
    return (-(x-3)**-2)

# f'(x)
def fturunan(x):
    return (2/(x-3)**3)

# Given 
x  = 2
xt = 1.9998

# Ngitung condition number
atas = abs(f(xt) - f(x)) / abs(f(x)) 
bawah = abs(xt - x) / abs(x)
k = atas / bawah

# Ngitung absolute condition number
kest = abs(x * fturunan(x) / f(x))
print("CONDITION NUMBER k: ", k)
print("ABSOLUTE CONDITION NUMBER kabs: ", kest)

# =================================================================================================================================================
print(f'''
========================================================================================
3. LU Factorization Menggunakan Partial Pivoting [3.5 poin]

Carilah solusi x pada sistem persamaan Ax=b apabila diberikan

    -2  1   5
A = 4   1   3
    6   -3  -1

    -28
b =  24
     1

menggunakan LU Factorization dengan Partial Pivoting, dengan cara melakukan dekomposisi PA=LU 
terlebih dahulu, yang dilanjutkan dengan backward substitution dan forward elimination.

=================ANS=========================

# Pada kolom pertama, |-2| < |4| < |6|, maka kita perlu membuat angka 6 menjadi paling atas,
# dengan cara menukar baris 3 dengan baris 1. Sehingga didapat P
# 0     0    1
# 0     1    0
# 1     0    0




''')


A = np.matrix([
     [-2, 1, -5],
     [4, 1, 3],
     [6, -3, -1]
    ])
B = np.matrix([
    [-28],
    [24],
    [4]
    ])

P,L,U = lu(A)

print("P:\n{}\n".format(P))
print("L:\n{}\n".format(L))
print("U:\n{}\n".format(U))

PA = np.matmul(P, A)
LU = np.matmul(L, U)

print("PA:\n{}\n".format(PA))
print("LU:\n{}\n".format(LU))



"""
PA = LU

AX = B 

PAx = PB
let PA = A*, PB = B*

A*x = B*
LUX = B*

misal UX = Y
LY = B*

1       0       0   |  y1 = 4
2/3     1       0   |  y2 = 24
-1/3    0       1   |  y3 = -28

y1 = 4

8/3 + y2 = 24
y2 = 21.3333333

-4/3 + y3 = -28
y3 = -28 + 4/3
y3 = -26.66666666

     4
Y =  21.333333
    -26.666666

SOLVE UX = Y

6       -3      -1    | x1 = 4
0       3       11/3  | x2 = 64/3
0       0       -16/3  | x3 = -80/3

-16/3 x3 = -80/3
-16 x3 = -80
x3 = 5

3x2 + 55/3 = 64/3
3x2 = 9/3 = 3
x2 = 1

6x1 - 3 - 5 = 4
6x1 = 12
x1 = 2

    2
X = 1
    5
"""
Astar = np.matmul(P, A)
Bstar = np.matmul(P, B)

Y = np.linalg.solve(L, Bstar)
print("A*:\n{}\n".format(Astar))
print("B*:\n{}\n".format(Bstar))
print("Y:\n{}\n".format(Y))

# SOLVE FOR UX = Y

X = np.linalg.solve(U, Y)
print("X:\n{}\n".format(X))


1.0010100110011001100110011001100110011001100110011010
  0010100110011001100110011001100110011001100110011010
1.001010011001100110011001100110011001100110011001101 


0010001100110011001100110011001100110011001100110011
0010001100110011001100110011001100110011001100110011