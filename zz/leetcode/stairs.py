def climbStairs(n: int) -> int:
    answer = 0
    maxTwos = n // 2
    for two in range(maxTwos + 1):
        answer += C(n - two, two)
    
    return answer

def factorial(n):
    if n == 0 or n == 1:
        return 1
    return n * factorial(n - 1)

def C(n, k):
    top = factorial(n)
    bottom = factorial(k) * factorial(n - k)
    return top // bottom
