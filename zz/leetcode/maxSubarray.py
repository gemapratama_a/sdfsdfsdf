def maxSubArray(nums: list) -> int:
    
    start = 0
    end = 0
    maxSum = nums[start]
    currentSum = nums[start]
    
    for i in range(len(nums)):
        print("start = {}, end = {}, sum = {}, max = {}".format(start, end, currentSum, maxSum))
        if currentSum >= maxSum:
            maxSum = currentSum
            currentSum += nums[end]
            end += 1
        else:
            currentSum -= nums[start]
            start += 1
            
    return maxSum


if __name__ == "__main__":
    arr1 = [-2,1,-3,4,-1,2,1,-5,4]
    arr2 = [5,4,-1,7,8]
    arr3 = [1]
    print(maxSubArray(arr1))