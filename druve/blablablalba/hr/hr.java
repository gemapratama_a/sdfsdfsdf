import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

class Solution {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = Integer.parseInt(scanner.nextLine());
        
        if (n == 2) { System.out.println("prime"); }
        if (n <= 1) { System.out.println("not prime"); }
        for (int i = 2; i < n; i++) {
            if (n == i + 1) {
                System.out.println("prime");
                break;
            }
            if (n % i == 0) {
                System.out.println("not prime");
                break;
            }
            
        }

        scanner.close();
    }

}