from decimal import *

n = int(input())

dictionary = dict()

for i in range(0, n):
	command = input().split(" ")
	score1 = float(command[1])
	score2 = float(command[2])
	score3 = float(command[3])
	name = command[0]

	avg = (score1 + score2 + score3) / 3
	dictionary[name] = avg

TWOPLACES = Decimal(10) ** -2       # same as Decimal('0.01')
m = input()
rounded = Decimal(dictionary[m]).quantize(TWOPLACES)
print(rounded)