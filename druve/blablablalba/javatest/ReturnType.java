
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;

public class ReturnType {

	public static void main(String[] args) throws Exception {
		
		Class<?> testClass;
		Test t = new Test(new Test2("test"));

		testClass = Class.forName(Test.class.getName());

		Method toString = testClass.getDeclaredMethod("toString1");
		//Method rVoid = testClass.getDeclaredMethod("returnvoid");

        Type retType = toString.getGenericReturnType();
        
        System.out.println(retType.getTypeName());

	}
}

class Test {

	public Test2 testVar;

	public Test(Test2 testVar) {
		this.testVar = testVar;
	}

	public Test2 toString1() {
		return this.testVar;
	}

	public void returnvoid() {
		System.out.println("AAA");
	}
}

class Test2 {
	public String testVar;

	public Test2(String testVar) {
		this.testVar = testVar;
	}

	public String toString() {
		return this.testVar;
	}

	public void returnvoid() {
		System.out.println("AAA");
	}
}