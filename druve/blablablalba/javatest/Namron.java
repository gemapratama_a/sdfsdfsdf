import java.io.*;
import java.util.*;

class Namron {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());
        Kid[] kids = new Kid[n];
        for (int i = 0; i < n; i++) {
            String[] line = reader.readLine().split(",");
            String name = line[0];
            int age = Integer.parseInt(line[1]);
            String rating = line[2];
            kids[i] = new Kid(name, age, rating);
            System.out.println("INSERTED: " + kids[i].getName());
        }
        System.out.print("Manusia yang berada di simulasi yaitu:\n");
        for (int i = 0; i < n; i++) {
            System.out.print((i + 1) + ". " + kids[i].getName() + " berumur " + kids[i].getAge() + " tahun dan ratingnya " + kids[i].getRating() + "\n");
        }
        ArrayList<Kid> barisan = new ArrayList(n);
        String[] command = reader.readLine().split(" ");
        while (!command[0].equals("udahan")) {
            if (command[0].equals("join")) {
                int len1 = command[1].length();
                String name = command[1].substring(1, len1 - 1);
                System.out.println("NAME: " + name);
                int len2 = command[2].length();
                int index = Integer.parseInt(command[2].substring(0, len2 - 1));
                Kid kid = getKid(name, kids, n);
                System.out.println("KID'S NAME: " + kid.getName());
                if (!(cekKidinList(kids, kid, n))) {
                    if (cekPosisi(index, n)) {
                        if (!(kid.isBosan())) {
                            if (!(cekKidinLine(barisan, kid, n))) {
                                barisan.add(kid.getIndex(), kid);
                            }
                            else {
                                System.out.println("Gak bisa goblok, udah dibilangin juga");
                                command = reader.readLine().split(" ");
                            }
                        }
                        else {
                            System.out.println("Bosan kaka");
                            command = reader.readLine().split(" ");
                        }
                    }
                    else {
                        System.out.println("Gak bisa kaka");
                        command = reader.readLine().split(" ");
                    }
                }
                else {
                    System.out.println("Gak bisa juga kaka");
                    command = reader.readLine().split(" ");
                }
            }
        }
    }

    public static Kid getKid(String name, Kid[] kids, int n) {
        for (int i = 0; i < n; i++) {
            if (kids[i].getName().equals(name)) {
                System.out.println("FOUND: " + name);
                return kids[i];
            }
        }
        return null;
    }

    public static boolean cekPosisi(int index, int n) {
        if (index > 0 & index <= n) {
            return true;
        }
        else {
            return false;
        }
    }

    public static boolean cekKidinList(Kid[] kids, Kid kid, int n) {
        boolean isIn = false;

        for (int i = 0; i < n; i++) {
            System.out.println(kids[i].getName());
            if (kid.getName().equals(kids[i].getName())) {
                isIn = true;
            }
            else {
                isIn = false;
            }
        }
        return isIn;
    }

    public static boolean cekKidinLine(ArrayList<Kid> barisan, Kid kid, int n) {
        boolean isIn = false;
        for (int i = 0; i < n; i++) {
            if (kid.getName().equals(barisan.get(i))) {
                isIn = true;
            }
            else {
                isIn = false;
            }
        }
        return isIn;
    }
}

class Kid {
    public String name;
    public int age;
    public String rating;
    public int index;
    public boolean bosan;

    public Kid(String name, int age, String rating) {
        this.name = name;
        this.age = age;
        this.rating = rating;
        this.bosan = false;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getRating() {
        return rating;
    }

    public int getIndex() {
        return index;
    }

    public boolean isBosan() {
        return bosan;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
