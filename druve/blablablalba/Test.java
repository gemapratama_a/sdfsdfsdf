import java.util.ArrayList;
import java.math.*;

public class Test {

    public static boolean isPrime(int n) {
        if (n == 2 || n == 1) {
            return false;
        }
        if (n > 2 && n % 2 == 0) {
            return false;
        }
        int sqrt = (int) Math.floor(Math.sqrt((double) n));

        for (int i = 3; i < 1 + sqrt; i += 2) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static boolean isPrime2(int n) {
        if (n == 1) {
            return false;
        }
        for (int i = 2; i < n; i ++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {

        long startTime = System.nanoTime();
        ArrayList<Integer> primes = new ArrayList<Integer>();
        for (int i = 1; i < 300000; i ++) {
            if (isPrime(i)) {
                primes.add(i);
            }
        }
        for (Integer i : primes) {
            System.out.println(i);
        }
        double endTime   = System.nanoTime();
        double totalTime = (endTime - startTime) / 1000000000;
        System.out.println("Time: " + totalTime + "s");
    }
    
}